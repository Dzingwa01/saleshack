import { SalesHackTemplatePage } from './app.po';

describe('SalesHack App', function() {
  let page: SalesHackTemplatePage;

  beforeEach(() => {
    page = new SalesHackTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
