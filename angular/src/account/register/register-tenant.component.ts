import { Component, Injector, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import {
    TenantRegistrationServiceProxy,
    RegisterTenantInput,
    RegisterTenantOutput,
    EditionSelectDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from 'account/login/login.service';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterTenantModel } from './register-tenant.model';

@Component({
    templateUrl: './register-tenant.component.html',
    animations: [accountModuleAnimation()],
    providers: [TenantRegistrationServiceProxy]
})
export class RegisterTenantComponent extends AppComponentBase implements OnInit, AfterViewInit {

    @ViewChild('cardBody',{static:false}) cardBody: ElementRef;
    model: RegisterTenantModel = new RegisterTenantModel();
    saving = false;
    // tenant: CreateTenantDto = new CreateTenantDto();
    registerTenantForm: FormGroup;
    constructor(
        injector: Injector,
        private _router: Router,
        public _loginService: LoginService,
        private fb: FormBuilder,
        private _activatedRoute: ActivatedRoute,
        private _tenantRegistrationService: TenantRegistrationServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.model.editionId = this._activatedRoute.snapshot.queryParams['editionId'];
        if (this.model.editionId) {
            this.model.subscriptionStartType = this._activatedRoute.snapshot.queryParams['subscriptionStartType'];
            this.model.gateway = this._activatedRoute.snapshot.queryParams['gateway'];
            this.model.paymentId = this._activatedRoute.snapshot.queryParams['paymentId'];
        }
        if (this.model.editionId) {
            this._tenantRegistrationService.getEdition(this.model.editionId)
                .subscribe((result: EditionSelectDto) => {
                    this.model.edition = result;
                    console.log(this.model.edition);
                });
        } this.model.init({ isActive: true });
        this.initializeForm();
    }
    ngAfterViewInit(): void {

        $(this.cardBody.nativeElement).find('input:first').focus();

    }
    initializeForm() {
        this.model.editionId = 3;
        this.registerTenantForm = this.fb.group({
            name: ['', [Validators.required, Validators.maxLength(32)]],
            lastName: ['', [Validators.required, Validators.maxLength(32)]],
            companyName: ['', Validators.required],
            tenancyName: ['', [Validators.required]],
            adminEmailAddress: ['', [Validators.required, Validators.email]],
            cellphone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(8)]],
            confirmPassword: ['', Validators.required],
            terms: [''],
            edition: [this.model.edition.displayName]
        }, { validator: this.passWordMatchValidator });

    }
    passWordMatchValidator(g: FormGroup) {
        return g.get('password').value === g.get('confirmPassword').value ? null : { 'mismatch': true };
    }
    save(): void {
        this.saving = true;
        Object.assign(this.model, this.registerTenantForm.value)
        console.log(this.model);
        this._tenantRegistrationService.registerTenant(this.model)
            .finally(() => { this.saving = false; })
            .subscribe((result: RegisterTenantOutput) => {
                //this.notify.info(this.l('SavedSuccessfully'));
                //Autheticate
                console.log(result);
                this.saving = true;
                abp.multiTenancy.setTenantIdCookie(result.tenantId);

                this._router.navigate(['account/success']);


                //this._loginService.authenticateModel.userNameOrEmailAddress = this.tenant.adminEmailAddress;
                //this._loginService.authenticateModel.password = this.tenant.password;
                //this._loginService.authenticate(() => { this.saving = false; });
            });
    }
}
