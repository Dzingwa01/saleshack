import { EditionSelectDto, RegisterTenantInput } from '@shared/service-proxies/service-proxies';

export class RegisterTenantModel extends RegisterTenantInput {
    public confirmPassword: string;
    public edition: EditionSelectDto;
}
