import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './success-message.component.html',
    animations: [accountModuleAnimation()]
})
export class SuccessMessageComponent extends AppComponentBase {
    constructor(
        injector: Injector
    ) {
        super(injector);
    }
}
