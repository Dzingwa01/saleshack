import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css'],
    providers: [AccountServiceProxy],
    animations: [accountModuleAnimation()]
})
export class ForgotPasswordComponent extends AppComponentBase implements OnInit {

    emailAddress: string;
    passwordRevoveryForm: FormGroup;
    constructor(injector: Injector,
        private _accountService: AccountServiceProxy) {
        super(injector)
    }

    ngOnInit() {
        this.passwordRevoveryForm = new FormGroup({
            'emailAddress': new FormControl('', [Validators.required, Validators.email])
        })
    }
    changePassword() {
        this._accountService.forgotPassword(this.emailAddress).subscribe((result) => {

        });
        window.location.href = '/account/success-message';
    }
    buttonToolTip() {
        return this.passwordRevoveryForm.valid ? null : 'Enter Email Address to Reset Password';
    }

}
