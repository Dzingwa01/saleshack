import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { ActivatedRoute, Params } from '@angular/router';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import {
    AccountServiceProxy,
    UserDto, RegisterInput,
    DataTableServiceProxy,
    DataTableListDto
} from 'shared/service-proxies/service-proxies';
import { DataTableTypes } from '@shared/models/data-table';
import { Observable } from 'rxjs/Observable';
import { TagModel } from 'ngx-chips/core/accessor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    templateUrl: './confirmation.component.html',
    styleUrls: ['./confirmation.component.css'],
    animations: [accountModuleAnimation()],
    providers: [AccountServiceProxy, DataTableServiceProxy]
})

export class ConfirmationComponent extends AppComponentBase implements OnInit {

    industries: DataTableListDto[] = [];
    cities: DataTableListDto[] = [];
    beeStatus: DataTableListDto[] = [];
    companySize: DataTableListDto[] = [];
    estBracket: DataTableListDto[] = [];
    provinces: DataTableListDto[] = [];
    serviceOffering: DataTableListDto[] = [];
    selectedIndustries: DataTableListDto[] = [];
    selectedServices: DataTableListDto[] = [];
    departments: DataTableListDto[] = [];
    jobTitles: DataTableListDto[] = [];
    experience: DataTableListDto[] = [];
    services: number[] = [];
    targetedIndustries: number[] = []
    user: UserDto = new UserDto();
    register: RegisterInput;
    userId: number;
    confirmationLink: string;
    confirmatonCode: string;
    confirmationForm: FormGroup;

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute

    ) {
        super(injector);
    }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.user.emailConfirmationCode = params['code'];
            this.user.id = params['id'];
            this.userId = params['id'];
            this.confirmatonCode = params['code'];
        });
        this.getIndustries();
        this.getProvinces();
        this.getCities();
        this.getCompanySize();
        this.getEstBracket();
        this.getBeeStatus();
        this.getServiceOffering();
        this.getDepartment();
        this.getJobTitle();
        this.getExperience();
        this.initializeForm();
    }
    initializeForm() {
        this.confirmationForm = this.fb.group({
            targetedMonthlyTurnoverSD: ['', Validators.required],
            averageDealSizeSD: [''],
            targetedMonthlyTurnoverMD: ['', Validators.required],
            averageDealSizeMD: [''],
            targetedMonthlyTurnoverBD: ['', Validators.required],
            averageDealSizeBD: [''],
            province: ['undefined', Validators.required],
            city: ['undefined', Validators.required],
            industry: ['undefined', Validators.required],
            size: ['undefined', Validators.required],
            turnover: ['undefined', Validators.required],
            beeStatus: ['undefined', Validators.required],
            services: this.fb.array([this.createService]),
            department: ['undefined', Validators.required],
            jobTitle: ['undefined', Validators.required],
            yearsOfExperience: ['undefined', Validators.required]
        })
    }
    createService(): FormGroup {
        return this.fb.group({
            id: ''
        })
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        });
    }
    onTargetIndustryAdd(tag: any): Observable<TagModel> {
        // this.selectedIndustries.push(tag);
        return Observable
            .of(tag)
    }
    onTargetIndustryRemove(tag: any): Observable<TagModel> {
        this.selectedIndustries.splice(this.selectedIndustries.indexOf(tag));
        return Observable
            .of(tag)
    }

    getProvinces() {
        this._dataTableService.getAll(DataTableTypes.PROVINCE).subscribe((result) => {
            this.provinces = result.items;
        });
    }

    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        });
    }

    getCompanySize() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySize = result.items;
        });
    }

    getEstBracket() {
        this._dataTableService.getAll(DataTableTypes.EST_BRACKET).subscribe((result) => {
            this.estBracket = result.items;
        });
    }

    getBeeStatus() {
        this._dataTableService.getAll(DataTableTypes.BEE_STATUS).subscribe((result) => {
            this.beeStatus = result.items;
        });
    }

    getServiceOffering() {
        this._dataTableService.getAll(DataTableTypes.SERVICE_OFFERING).subscribe((result) => {
            this.serviceOffering = result.items;
        });
    }
    onServiceOfferingAdd(tag: any): Observable<TagModel> {
       // this.selectedServices.push(tag)
        console.log(this.selectedServices);
        return Observable
            .of(tag)

    }
    onServiceOfferingRemove(tag: any): Observable<TagModel> {
        this.selectedServices.splice(this.selectedServices.indexOf(tag.id, 1));
        return Observable
            .of(tag)
    }

    getDepartment() {
        this._dataTableService.getAll(DataTableTypes.DEPARTMENT).subscribe((result) => {
            this.departments = result.items;
        });
    }

    getJobTitle() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        });
    }
    getExperience() {
        this._dataTableService.getAll(DataTableTypes.EXPERIENCE).subscribe((result) => {
            this.experience = result.items;
        });
    }
    save(): void {
        this.confirmationForm.get('averageDealSizeSD').setValue(
            this.confirmationForm.get('targetedMonthlyTurnoverSD').value / 4
        );
        this.confirmationForm.get('averageDealSizeMD').setValue(
            this.confirmationForm.get('targetedMonthlyTurnoverMD').value / 4
        );
        this.confirmationForm.get('averageDealSizeBD').setValue(
            this.confirmationForm.get('targetedMonthlyTurnoverBD').value / 4
        );
        this.user = Object.assign({}, this.confirmationForm.value);
        this.user.id = this.userId;
        this.user.emailConfirmationCode = this.confirmatonCode;
        this.selectedIndustries.forEach((value) => {
            this.services.push(value.id);
            this.user.services = this.services;
        });
        this.selectedServices.forEach((value) => {
            this.targetedIndustries.push(value.id)
            this.user.targetIndustries = this.targetedIndustries;
        })
        this._accountService.confirmation(this.user)
            .finally(() => { })
            .subscribe((result) => {
            });
        window.location.href = '/account/confirmed';
    }
    submitToolTip() {
        return this.confirmationForm.valid ? null : 'Please Enter All Required Fields to Submit';
    }
}

