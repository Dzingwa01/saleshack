import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './confirm-success.component.html',
    animations: [accountModuleAnimation()]
})

export class ConfirmSuccessComponent extends AppComponentBase {
    constructor(
        injector: Injector
    ) {
        super(injector);
    }
}
