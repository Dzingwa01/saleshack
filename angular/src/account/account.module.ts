import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


import { AbpModule } from '@abp/abp.module';
import { AccountRoutingModule } from './account-routing.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { ArchwizardModule } from 'angular-archwizard';
import { TagInputModule } from 'ngx-chips';

import { AccountComponent } from './account.component';
import { TenantChangeComponent } from './tenant/tenant-change.component';
import { TenantChangeModalComponent } from './tenant/tenant-change-modal.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountLanguagesComponent } from './layout/account-languages.component';

import { LoginService } from './login/login.service';
import { RegisterTenantComponent } from 'account/register/register-tenant.component';
import { ConfirmationComponent } from 'account/confirmation/confirmation.component';
import { RegisterSucess } from 'account/register/registerSucess.component';
import { ConfirmSuccessComponent } from 'account/confirmation/confirm-success.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SuccessMessageComponent } from './forgot-password/success-message/success-message.component';
import { ResetSuccessComponent } from './reset-password/reset-success/reset-success.component';
import { SelectEditionComponent } from './register/select-edition.component';
import { TenantRegistrationServiceProxy, SubscriptionServiceProxy, PaymentServiceProxy } from '@shared/service-proxies/service-proxies';
import { UpgradeOrExtendComponent } from './payment/upgrade-or-extend.component';
import { PaymentGatewaysComponent } from './payment/payment-gateways.component';
import { PayPalComponent } from './payment/paypal/paypal.component';
import { BuyComponent } from './payment/buy.component';
//import { PricingComponent } from './pricing/pricing.component';

TagInputModule.withDefaults({
    tagInput: {
        maxItems: 3
    }
});
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        JsonpModule,
        AbpModule,
        SharedModule,
        ServiceProxyModule,
        AccountRoutingModule,
        ModalModule.forRoot(),
        TooltipModule.forRoot(),
        ArchwizardModule,
        TagInputModule
    ],
    declarations: [
        AccountComponent,
        RegisterTenantComponent,
        TenantChangeComponent,
        TenantChangeModalComponent,
        LoginComponent,
        RegisterComponent,
        AccountLanguagesComponent,
        ConfirmationComponent,
        RegisterSucess,
        ConfirmSuccessComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        SuccessMessageComponent,
        ResetSuccessComponent,
        SelectEditionComponent,
        BuyComponent,
        UpgradeOrExtendComponent,
        PaymentGatewaysComponent,
        PayPalComponent,
        ResetSuccessComponent
        //PricingComponent
    ],
    providers: [
        LoginService,
        TenantRegistrationServiceProxy,
        SubscriptionServiceProxy,
        PaymentServiceProxy

    ]
})
export class AccountModule {

}
