import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation, accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './reset-success.component.html',
    animations: [accountModuleAnimation()]
})
export class ResetSuccessComponent extends AppComponentBase {
    constructor(
        injector: Injector
    ) {
        super(injector);
    }
}
