import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { AccountServiceProxy, UserDto } from '@shared/service-proxies/service-proxies';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css'],
    animations: [accountModuleAnimation()],
    providers: [AccountServiceProxy]
})
export class ResetPasswordComponent extends AppComponentBase implements OnInit {
    newPassword: string;
    confirmPassword: string;
    user: UserDto = new UserDto();
    passwordResetForm: FormGroup;
    constructor(injector: Injector,
        private _accountService: AccountServiceProxy,
        private activatedRoute: ActivatedRoute) {
        super(injector)
    }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.user.passwordResetToken = params['code'];
            this.user.id = params['id'];
        });
        this.passwordResetForm = new FormGroup({
            'newPassword': new FormControl('', [Validators.required, Validators.minLength(6)]),
            'confirmPassword': new FormControl('', Validators.required)
        }, { validators: this.passWordMatchValidator })
    }
    passWordMatchValidator(g: FormGroup) {
        return g.get('newPassword').value === g.get('confirmPassword').value ? null : { 'mismatch': true };
    }
    resetPassword() {
        this._accountService.passwordReset(this.user, this.newPassword).subscribe(() => { });
        window.location.href = '/account/reset-success';
    }

}
