import { Component, Injector, ElementRef, ViewChild, Input, OnInit, AfterViewInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from './login.service';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-child',
    templateUrl: './login.component.html',
    styleUrls: [
        './login.component.less'
    ],
    animations: [accountModuleAnimation()]
})
export class LoginComponent extends AppComponentBase implements OnInit, AfterViewInit {
    @ViewChild('cardBody',{static:false}) cardBody: ElementRef;
    submitting = false;
    tenancyName: string;
    loginForm: FormGroup;

    constructor(
        injector: Injector,
        public loginService: LoginService,
        private fb: FormBuilder,
        private _sessionService: AbpSessionService,

    ) {
        super(injector);

        this.tenancyName = this.appSession.tenant.tenancyName;
    }
    ngOnInit() {
        this.initializeLoginForm();
    }

    ngAfterViewInit(): void {
        $(this.cardBody.nativeElement).find('input:first').focus();
    }

    get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }

    get isSelfRegistrationAllowed(): boolean {
        if (!this._sessionService.tenantId) {

            return false;
        }

        return true;

    }
    initializeLoginForm() {
        this.loginForm = this.fb.group({
            rememberMe: [''],
            userNameOrEmailAddress: ['', Validators.required],
            password: ['', [Validators.required]]
        });
    }

    login(): void {
        this.loginService.authenticateModel = Object.assign({}, this.loginForm.value);
        this.submitting = true;
        this.loginService.authenticate(
            () => this.submitting = false

        );
        window.location.href = '/'
    }
}
