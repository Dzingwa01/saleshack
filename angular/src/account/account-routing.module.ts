import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account.component';
import { RegisterTenantComponent } from 'account/register/register-tenant.component';
import { TenantChangeModalComponent } from 'account/tenant/tenant-change-modal.component';
import { ConfirmationComponent } from 'account/confirmation/confirmation.component';
import { RegisterSucess } from 'account/register/registerSucess.component';
import { ConfirmSuccessComponent } from 'account/confirmation/confirm-success.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SuccessMessageComponent } from './forgot-password/success-message/success-message.component';
import { ResetSuccessComponent } from './reset-password/reset-success/reset-success.component';
import { SelectEditionComponent } from 'account/register/select-edition.component';
import { BuyComponent } from './payment/buy.component';
import { UpgradeOrExtendComponent } from './payment/upgrade-or-extend.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
                children: [
                    { path: 'login', component: LoginComponent },
                    { path: 'tenant', component: TenantChangeModalComponent },
                    { path: 'register2', component: RegisterComponent },
                    { path: 'register', component: RegisterTenantComponent },
                    { path: 'confirmation', component: ConfirmationComponent },
                    { path: 'success', component: RegisterSucess },
                    { path: 'confirmed', component: ConfirmSuccessComponent },
                    { path: 'forgot-password', component: ForgotPasswordComponent },
                    { path: 'reset-password', component: ResetPasswordComponent },
                    { path: 'success-message', component: SuccessMessageComponent },
                    { path: 'reset-success', component: ResetSuccessComponent },
                    { path: 'buy', component: BuyComponent },
                    { path: 'extend', component: UpgradeOrExtendComponent },
                    { path: 'upgrade', component: UpgradeOrExtendComponent },
                    { path: 'select-edition', component: SelectEditionComponent }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
