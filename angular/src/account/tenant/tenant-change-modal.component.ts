import { Component, OnInit, ViewChild, Injector, ElementRef } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { IsTenantAvailableInput } from '@shared/service-proxies/service-proxies';
import { AppTenantAvailabilityState } from '@shared/AppEnums';
import { ModalDirective } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'tenantChangeModal',
    templateUrl: './domain-login.html'
})
export class TenantChangeModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('tenantChangeModal',{static:false}) modal: ModalDirective;
    @ViewChild('tenancyNameInput',{static:false}) tenancyNameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    tenancyName: string;
    changeTenantForm: FormGroup;
    active = false;
    saving = false;

    constructor(
        private _accountService: AccountServiceProxy,
        private fb: FormBuilder,
        injector: Injector,
        private _router: Router,
    ) {
        super(injector);
    }
    ngOnInit() {
        this.initializeForm();
    }
    initializeForm() {
        this.changeTenantForm = this.fb.group({
            tenancyName: ['', Validators.required]
        });
    }
    show(tenancyName: string): void {
        this.tenancyName = tenancyName;
        this.active = false;
        this.modal.show();
    }

    onShown(): void {
        $(this.tenancyNameInput.nativeElement).focus().select();
    }
    onKey(event: any) { // without type info
        this.tenancyName = event.target.value;
        console.log('KEYUP', event.target.value)
        this.appSession.tenant.tenancyName = event.target.value;
    }
    save(): void {
        this.tenancyName = this.changeTenantForm.get('tenancyName').value;
        if (!this.tenancyName) {
            abp.multiTenancy.setTenantIdCookie(undefined); ;
            this.close();
            location.reload();
            // return;
        }

        const input = new IsTenantAvailableInput();
        input.tenancyName = this.tenancyName;
        console.log('tenancyName: ', this.tenancyName);

        this.saving = true;
        this._accountService.isTenantAvailable(input)
            .finally(() => { this.saving = false; })
            .subscribe((result) => {
                switch (result.state) {
                    case AppTenantAvailabilityState.Available:
                        abp.multiTenancy.setTenantIdCookie(result.tenantId);
                        this._router.navigate(['account/login']);
                        return;
                    case AppTenantAvailabilityState.InActive:
                        this.message.warn(this.l('TenantIsNotActive', this.tenancyName));
                        break;
                    case AppTenantAvailabilityState.NotFound: // NotFound
                        this.message.warn(this.l('ThereIsNoTenantDefinedWithName{0}', this.tenancyName));
                        break;
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
