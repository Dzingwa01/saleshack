export interface CompanyProspectInput {
    country: string
    industry: string
    city: string
    companysize: string
    beeStatus: string
}
