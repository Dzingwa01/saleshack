import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DomainSearchServiceProxy, ContactProspectServiceProxy } from 'shared/service-proxies/service-proxies';
import { CreateContactInfoModalComponent } from 'app/domain/create-info/create-info.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-domain-search',
    templateUrl: './domain.component.html',
    animations: [appModuleAnimation()],
    providers: [DomainSearchServiceProxy, ContactProspectServiceProxy]
})

export class DomainComponent extends AppComponentBase implements OnInit {
    @ViewChild('createContactInfoModal',{static:false}) createContactInfoModal: CreateContactInfoModalComponent;
    email: string;
    domain = '';
    emails: string[];
    visible = false;
    searchForm: FormGroup;

    constructor(injector: Injector,
        private _domainService: DomainSearchServiceProxy,
        private fb: FormBuilder) {
        super(injector);
    }
    ngOnInit(): void {
        this.initializeForm();
    }
    initializeForm() {
        const urlReg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
         this.searchForm = this.fb.group({
            domain: ['', [Validators.required, Validators.pattern(urlReg)]]
        })
    }
    getEmails(): void {
        this.visible = true;
        this.domain = this.searchForm.get('domain').value;
        this._domainService.getWebEmails(this.domain).subscribe((results) => {
            this.emails = results;
        });
    }
    createContact(): void {
        this.createContactInfoModal.show();

    }
    getEmail(i: number) {
        this.email = this.emails[i];
        this.createContactInfoModal.email = this.emails[i];
    }
}
