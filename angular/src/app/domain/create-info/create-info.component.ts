import { Component, ViewChild, Injector, ElementRef, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    ContactServiceProxy,
    CreateContactInput,
    ProspectListServiceProxy,
    ContactProspect, CompanyServiceProxy,
    CompanyListDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
    selector: 'app-create-contact-info',
    templateUrl: './create-info.component.html',
    providers: [ContactServiceProxy, ProspectListServiceProxy, CompanyServiceProxy]
})
export class CreateContactInfoModalComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createContactInfoModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    filter = '';
    companies: CompanyListDto[] = [];
    contact: CreateContactInput;
    contactProspect: ContactProspect;
    active = false;
    saving = false;
    @Input() email: string;

    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy,
        private _companyService: CompanyServiceProxy,
    ) {
        super(injector);
        this.contact = new CreateContactInput();
    }
    ngOnInit(): void {

    }
    show(): void {
        this.active = true;
        this.contact.email = this.email;
        this.getCompanies();
        this.modal.show();
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
        this.contact.email = this.email;
    }

    save(): void {
        this.saving = true;
        this.contact.status = 'New';
        this._contactService.create(this.contact)
            .finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(this.contact);
            });
        this.notify.info('Contact Also  Added on Prospect List');
    }
    getCompanies() {
        this._companyService.getAllCompanies(this.filter).subscribe((result) => {
            this.companies = result.items;
        });
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}
