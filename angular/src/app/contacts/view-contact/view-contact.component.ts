import { Component, OnInit, ViewChild, ElementRef, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    ContactServiceProxy,
    ContactDetailOutput,
    CompanyServiceProxy,
    ContactProspectServiceProxy,
    ContactProspectDetailOutput
} from 'shared/service-proxies/service-proxies';
import { AppComponentBase } from 'shared/app-component-base';

@Component({
    selector: 'app-view-contact',
    templateUrl: './view-contact.component.html',
    styleUrls: ['./view-contact.component.css'],
    providers: [ContactServiceProxy, ContactProspectServiceProxy]
})
export class ViewContactComponent extends AppComponentBase implements OnInit {
    active = false;
    @ViewChild('viewContactModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    contact: any;
    companyName: string;
    constructor(injector: Injector,
        private _contactService: ContactServiceProxy,
        private _companyService: CompanyServiceProxy,
        private _contactProspectService: ContactProspectServiceProxy) {
        super(injector)
    }

    ngOnInit() {

    }
    show(id: string): void {
        this._contactService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: ContactDetailOutput) => {
                this.contact = result;
                this.getCompanyName(this.contact);
            });
    }
    showProspectProfile(id: string) {
        this._contactProspectService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: ContactProspectDetailOutput) => {
                this.contact = result;
                this.getCompanyName(this.contact)
            });
    }
    close() {
        this.active = false;
        this.modal.hide();
    }
    getCompanyName(contact: any) {
        this._companyService.getById(contact.companyId).subscribe((result) => {
            this.companyName = result.items[0].name;
        })
    }
}
