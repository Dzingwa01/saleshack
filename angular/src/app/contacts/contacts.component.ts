import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    ContactListDto, ContactServiceProxy, ContactDetailOutput,
    ContactProspectServiceProxy, ContactProspect,
    ProspectListServiceProxy, TasksServiceProxy, NoteServiceProxy
} from 'shared/service-proxies/service-proxies';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { EditContactComponent } from 'app/contacts/edit-contact/edit-contact.component';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { CreateContactModalComponent } from 'app/contacts/create-contact/create-contact.component';
import { ContactImportComponent } from 'app/contacts/contact-import/contact-import.component';
import { CreateTaskModalComponent } from '@app/general/tasks/create-task/create-task.component';
import { CreateNoteModalComponent } from '@app/general/notes/create-note/create-note.component';
import { ViewContactComponent } from 'app/contacts/view-contact/view-contact.component';
import { FormControl } from '@angular/forms';


@Component({
    templateUrl: './contacts.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./contacts.component.css'],
    providers: [ContactServiceProxy, ContactProspectServiceProxy,
        ProspectListServiceProxy, TasksServiceProxy, NoteServiceProxy]
})

export class ContactsComponent extends PagedListingComponentBase<ContactListDto> {
    @ViewChild('createContactModal',{static:false}) createContactModal: CreateContactModalComponent;
    @ViewChild('editContactModal',{static:false}) editContactModal: EditContactComponent;
    @ViewChild('viewContactModal',{static:false}) viewContactModal: ViewContactComponent;
    @ViewChild('importContactModal',{static:false}) importContactModal: ContactImportComponent;
    @ViewChild('createTaskModal',{static:false}) createTaskModal: CreateTaskModalComponent;
    @ViewChild('createNoteModal',{static:false}) createNoteModal: CreateNoteModalComponent;

    key = 'name';
    reverse = false;
    filter = '';
    filterTerm: FormControl = new FormControl();
    contacts: ContactListDto[] = [];
    prospect: ContactProspect = new ContactProspect();

    saving = false;
    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy,
    ) {
        super(injector);
    }

    list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._contactService.getAll(request.sorting, request.skipCount, request.maxResultCount)
            .finally(() => {
                finishedCallback();
            }).subscribe((result) => {
                this.contacts = result.items;
                this.showPaging(result, pageNumber);
            });
    }
    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    createContact(): void {
        this.createContactModal.show();
    }

    delete(contact: ContactListDto): void {
        abp.message.confirm(
            'Delete Contact \'' + contact.name + '\'?',
            (result: boolean) => {
                if (result) {
                    this._contactService.delete(contact.id)
                        .finally(() => {
                            abp.notify.info('Deleted Contact: ' + contact.name);
                            this.refresh();
                        })
                        .subscribe(() => { });
                }
            }
        );
    }

    editContact(contact: ContactDetailOutput): void {
        this.editContactModal.show(contact.id);
    }

    createTask(target: ContactDetailOutput, targetType: string): void {
        this.createTaskModal.show(target.id, targetType);
    }

    createNote(target: ContactDetailOutput, targetType: string): void {
        this.createNoteModal.show(target.id, targetType);
    }

    qualify(contact: ContactDetailOutput): void {
        this._contactService.editStatus(contact.id, 'Qualify').subscribe(() => {
            this.notify.success(contact.name + ' Added to Qualification Successfully');
        });
        this.refresh();
    }

    showContactImport() {
        this.importContactModal.show();
    }
    viewContact(contact: ContactDetailOutput): void {
        this.viewContactModal.show(contact.id);
    }

    filterContacts() {
        this.filterTerm.valueChanges
            .pipe(debounceTime(400))
            .subscribe(data => {
                this._contactService.contactFilter(data).subscribe(response => {
                    this.contacts = response.items
                })
            })
    }
    getInitial(name: string) {
        return name.substring(0, 1)
    }
    getBadgeColor(status: string) {
        switch (status) {
            case 'New':
                return 'success';
            case 'Qualify':
                return 'info';
            case 'Qualified':
                return 'primary';
            case 'Recycle':
                return 'danger';
            default:
                return 'default';
        }
    }
    isQualified(status: string) {
        if (status === 'Qualify' || status === 'Qualified') {
            return true;
        }
        return false;
    }
    trackByName(index, contact) {
        return contact.name;
    }
}
