import { Component, ViewChild, Injector, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    ContactServiceProxy,
    CreateContactInput,
    CompanyListDto,
    CompanyServiceProxy,
    CompanyProspectServiceProxy,
    CompanyProspectListDto,
    DataTableListDto,
    DataTableServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTableTypes } from '@shared/models/data-table';
@Component({
    selector: 'app-create-contact',
    templateUrl: './create-contact.component.html',
    providers: [ContactServiceProxy, CompanyProspectServiceProxy]
})
export class CreateContactModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createContactModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    country = ''; industry = ''; city = ''; companySize = ''; bee = '';
    contact: CreateContactInput;
    companies: CompanyListDto[] = [];
    provinces: DataTableListDto[] = [];
    companyProspects: CompanyProspectListDto[] = [];
    industries: DataTableListDto[] = [];
    departments: DataTableListDto[] = [];
    cities: DataTableListDto[] = [];
    sources: DataTableListDto[] = [];
    jobTitles: DataTableListDto[] = [];
    active = false;
    saving = false;
    filter = '';
    createContactForm: FormGroup;

    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy,
        private _companyService: CompanyServiceProxy,
        private _companyProspect: CompanyProspectServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder
    ) {

        super(injector);
    }
    ngOnInit(): void {
        this.initializeForm();
    }
    initializeForm() {
        this.createContactForm = this.fb.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            industry: ['undefined', Validators.required],
            email: ['', [Validators.email, Validators.required]],
            cellphone: ['', Validators.required],
            city: ['undefined', Validators.required],
            source: ['undefined', Validators.required],
            jobTitle: ['undefined', Validators.required],
            department: ['undefined', Validators.required],
            province: ['undefined', Validators.required],
            specialEvents: [''],
            facebook: [''],
            linkedIn: [''],
            googlePlus: [''],
            instagram: [''],
            twitter: [''],
            companyId: ['undefined', Validators.required]
        })
    }

    show(): void {
        this.active = true;
        this.contact = new CreateContactInput();
        this.modal.show();
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
        this.getCompanies();
        this.getIndustries();
        this.getProspects();
        this.getCities();
        this.getDepartments();
        this.getSources();
        this.getJobTitles();
        this.getProvinces();
    }

    save(): void {
        this.saving = true;
        this.contact.status = 'New';
        this.contact = Object.assign({}, this.createContactForm.value);
        this._contactService.create(this.contact)
            .finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(this.contact);
            });
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    getCompanies() {
        this._companyService.getAllCompanies(this.filter).subscribe((result) => {
            this.companies = result.items;
        });
    }
    getProspects(): void {
        this._companyProspect.getAll(this.country, this.industry, this.city, this.companySize, this.bee).subscribe((result) => {
            this.companyProspects.sort((a, b) => {
                if (a > b) {
                    return 1;
                }
                if (a < b) {
                    return -1;
                }
                return 0;
            })
        })
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        })
    }
    getDepartments() {
        this._dataTableService.getAll(DataTableTypes.DEPARTMENT).subscribe((result) => {
            this.departments = result.items;
        })
    }
    getSources() {
        this._dataTableService.getAll(DataTableTypes.SOURCE).subscribe((result) => {
            this.sources = result.items;
        })
    }
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        })
    }
    getProvinces() {
        this._dataTableService.getAll(DataTableTypes.PROVINCE).subscribe((result) => {
            this.provinces = result.items;
        })
    }
}
