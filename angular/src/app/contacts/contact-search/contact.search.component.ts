import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    ContactProspectListDto,
    ContactProspectServiceProxy,
    ProspectListServiceProxy,
    ContactProspect,
    DataTableListDto,
    DataTableServiceProxy,
    ContactDetailOutput
} from 'shared/service-proxies/service-proxies';
import { DataTableTypes } from '@shared/models/data-table';
import { ViewContactComponent } from '../view-contact/view-contact.component';

@Component({
    templateUrl: './contact.search.component.html',
    animations: [appModuleAnimation()],
    providers: [ContactProspectServiceProxy, ProspectListServiceProxy]
})

export class ContactSearchComponent extends AppComponentBase implements OnInit {

    @ViewChild('viewContactModal',{static:false}) viewContactModal: ViewContactComponent;

    cities: DataTableListDto[] = [];
    jobTitles: DataTableListDto[] = [];
    industries: DataTableListDto[] = [];
    companySizes: DataTableListDto[] = [];
    city = '';
    jobTitle = '';
    industry = '';
    companySize = '';
    contactSearch = false;
    firstName = '';
    lastName = '';
    filter = '';
    saving = false;
    totalItems: number;
    page = 1;
    noOfItems = 10;
    prospect: ContactProspect;
    contactProspects: ContactProspectListDto[] = [];

    constructor(
        injector: Injector,
        private _contactProspectService: ContactProspectServiceProxy,
        private _prospectListService: ProspectListServiceProxy,
        private _dataTableService: DataTableServiceProxy

    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getCities();
        this.getIndustries();
        this.getCompanySizes();
        this.getBeeStatuses();
    }
    search(): void {
        this.contactSearch = true;
        this._contactProspectService.getAll(this.industry, this.jobTitle, this.city, this.companySize,
             this.firstName, this.lastName).subscribe((results) => {
            this.contactProspects = results.items;
            this.totalItems = this.contactProspects.length;
        });
    }
    viewContact(contact: ContactDetailOutput): void {
        this.viewContactModal.showProspectProfile(contact.id);
    }
    add(contact: ContactProspect) {
        this._prospectListService.create(contact.id).finally(() => this.saving = false)
            .subscribe(() => {
                abp.notify.success(this.l('Added Successfully'));
            });
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        });
    }

    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        });
    }

    getBeeStatuses() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        });
    }
    getCompanySizes() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySizes = result.items;
        });
    }
    getInitial(name: string) {
        return name !== '' ? name.substring(0, 1) : '-';
    }
}
