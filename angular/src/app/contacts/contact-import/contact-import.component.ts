import { Component, OnInit, ViewChild, EventEmitter, Output, ElementRef, Injector } from '@angular/core';
import * as XLSX from 'xlsx';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from 'shared/app-component-base';
import { ContactImport, ContactServiceProxy } from 'shared/service-proxies/service-proxies';

type AOA = any[][];

@Component({
    selector: 'app-contact-import',
    templateUrl: './contact-import.component.html',
    providers: [ContactServiceProxy]
})
export class ContactImportComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('importContactModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    active = false;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    headerRow: any[] = [];
    data: AOA = [[1, 2], [3, 4]];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    fileName = 'SheetJS.xlsx';
    saving = false;
    import: ContactImport;
    importList: ContactImport[] = [];

    constructor(injector: Injector, private _contactService: ContactServiceProxy) {
        super(injector)
    }
    ngOnInit(): void {
        this.import = new ContactImport();
    }
    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbook */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            this.headerRow = this.getHeaderRow(ws);
            console.log(this.headerRow);
            /* save data */
            this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
        };
        reader.readAsBinaryString(target.files[0]);
    }

    getHeaderRow(sheet) {
        const headers = [];
        const range = XLSX.utils.decode_range(sheet['!ref']);
        let C = range.s.r; /* start in the first row */
        const R = range.s.r;
        /* walk every column in the range */
        for (C = range.s.c; C <= range.e.c; ++C) {
            const cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })] /* find the cell in the first row */

            let hdr = 'UNKNOWN ' + C; // <-- replace with your desired default
            if (cell && cell.t) { hdr = XLSX.utils.format_cell(cell); }

            headers.push(hdr);
        }
        return headers;
    }
    show() {
        this.active = true;
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    importButton() {
        for (let i = 1; i < this.data.length; i++) {
            const tempImport: ContactImport = new ContactImport();
            tempImport.name = this.data[i][this.headerRow.indexOf(this.import.name)];
            tempImport.surname = this.data[i][this.headerRow.indexOf(this.import.surname)];
            tempImport.email = this.data[i][this.headerRow.indexOf(this.import.email)];
            tempImport.industry = this.data[i][this.headerRow.indexOf(this.import.industry)];
            tempImport.companyName = this.data[i][this.headerRow.indexOf(this.import.companyName)];
            tempImport.address = this.data[i][this.headerRow.indexOf(this.import.address)];
            tempImport.province = this.data[i][this.headerRow.indexOf(this.import.province)];
            tempImport.source = this.data[i][this.headerRow.indexOf(this.import.source)];
            tempImport.cellphone = this.data[i][this.headerRow.indexOf(this.import.cellphone)];
            tempImport.city = this.data[i][this.headerRow.indexOf(this.import.city)];
            tempImport.jobTitle = this.data[i][this.headerRow.indexOf(this.import.jobTitle)];
            tempImport.department = this.data[i][this.headerRow.indexOf(this.import.department)];
            tempImport.beeStatus = this.data[i][this.headerRow.indexOf(this.import.beeStatus)];
            tempImport.companySize = this.data[i][this.headerRow.indexOf(this.import.companySize)];
            tempImport.phone = this.data[i][this.headerRow.indexOf(this.import.phone)];
            tempImport.website = this.data[i][this.headerRow.indexOf(this.import.website)];
            this.importList.push(tempImport);
        }
        this._contactService.importContact(this.importList)
            .finally(() => { this.saving = false; })
            .subscribe((result) => {
                this.notify.success(this.l('Contacts Imported Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
}
