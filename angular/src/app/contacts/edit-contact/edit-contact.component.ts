import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/app-component-base';
import {
    ContactServiceProxy, ContactDetailOutput,
    CompanyServiceProxy, CompanyListDto, CompanyProspectServiceProxy,
    CompanyProspectListDto,
    DataTableListDto,
    DataTableServiceProxy
} from 'shared/service-proxies/service-proxies';

import * as _ from 'lodash';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTableTypes } from '@shared/models/data-table';

@Component({
    selector: 'app-edit-contact',
    templateUrl: './edit-contact.component.html',
    providers: [CompanyProspectServiceProxy, CompanyServiceProxy]
})
export class EditContactComponent extends AppComponentBase implements OnInit {

    @ViewChild('editContactModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    country = ''; industry = ''; city = ''; companySize = ''; bee = '';
    active = false;
    saving = false;
    filter = '';
    contactId: string;
    contact: ContactDetailOutput = null;
    company: CompanyProspectListDto = null;
    companyProspects: CompanyProspectListDto[] = [];
    companies: CompanyListDto[] = [];
    industries: DataTableListDto[] = [];
    departments: DataTableListDto[] = [];
    cities: DataTableListDto[] = [];
    sources: DataTableListDto[] = [];
    jobTitles: DataTableListDto[] = [];
    provinces: DataTableListDto[] = [];
    editContactForm: FormGroup;

    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy,
        private _companyProspectService: CompanyProspectServiceProxy,
        private _companyService: CompanyServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder
    ) {
        super(injector);
    }
    ngOnInit() {
        this.initializeForm();
    }
    initializeForm() {
        this.editContactForm = this.fb.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            industry: ['undefined', Validators.required],
            email: ['', [Validators.email, Validators.required]],
            cellphone: ['', Validators.required],
            city: ['undefined', Validators.required],
            source: ['undefined', Validators.required],
            jobTitle: ['undefined', Validators.required],
            department: ['undefined', Validators.required],
            province: ['undefined', Validators.required],
            specialEvents: [''],
            facebook: [''],
            linkedIn: [''],
            googlePlus: [''],
            instagram: [''],
            twitter: [''],
            companyId: ['undefined', Validators.required]
        })
    }
    show(id: string): void {
        this.contactId = id;
        this._contactService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: ContactDetailOutput) => {
                this.contact = result;
                this.editContactForm.patchValue(result);
                this.getCompany();
            });
    }

    onShown(): void {
        $(this.modalContent.nativeElement).focus();
        this.getCompanies();
        this.getIndustries();
        this.getCities();
        this.getDepartments();
        this.getSources();
        this.getJobTitles();
        this.getProvinces();
    }

    save(): void {
        this.saving = true;
        this.contact = Object.assign({}, this.editContactForm.value)
        this.contact.id = this.contactId;
        this._contactService.editContact(this.contact)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
    getCompany() {
        this.company = new CompanyProspectListDto();
        this._companyProspectService.getById(this.contact.companyId).subscribe((result) => {
            this.company = result.items[0];
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    getCompanies() {
        this._companyService.getAllCompanies(this.filter).subscribe((result) => {
            this.companies = result.items;
        });
    }
    getProspects(): void {
        this._companyProspectService.getAll(this.country, this.industry, this.city, this.companySize, this.bee).subscribe((result) => {
            this.companyProspects = result.items;
            this.companyProspects.sort((a, b) => {
                if (a > b) {
                    return 1;
                }
                if (a < b) {
                    return -1;
                }
                return 0;
            })
        })
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        })
    }
    getDepartments() {
        this._dataTableService.getAll(DataTableTypes.DEPARTMENT).subscribe((result) => {
            this.departments = result.items;
        })
    }
    getSources() {
        this._dataTableService.getAll(DataTableTypes.SOURCE).subscribe((result) => {
            this.sources = result.items;
        })
    }
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        })
    }
    getProvinces() {
        this._dataTableService.getAll(DataTableTypes.PROVINCE).subscribe((result) => {
            this.provinces = result.items;
        })
    }
}
