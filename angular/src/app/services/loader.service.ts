import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class LoaderService {
    isLoading = new Subject<boolean>();
    show() {
        this.isLoading.next(true);
        console.log('Spinner Shown');
    }
    hide() {
        this.isLoading.next(false);
        console.log('Spinner Hidden');
    }
}
