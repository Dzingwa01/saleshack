import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { CompanyInfoModalComponent } from '../company-info/company-info.component';
import {
    CompanyProspectDetailOutput,
    CompanyProspectServiceProxy,
    ContactProspectServiceProxy,
    CompanyProspect,
    CompanyServiceProxy,
    CompanyDetailOutput
} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-company-profile',
    templateUrl: './company-profile.component.html',
    styleUrls: ['./company-profile.component.css'],
    providers: [CompanyProspectServiceProxy, ContactProspectServiceProxy]
})
export class CompanyProfileComponent extends AppComponentBase implements OnInit {

    @ViewChild('companyProfileModal',{static:false}) modal: ModalDirective;
    @ViewChild('companyInfoModal',{static:false}) companyContacts: CompanyInfoModalComponent;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    totalContacts = 0;
    city?: string;
    beeStatus?: string;
    companySize?: string;
    colorBackground: string;
    company: any = null;
    constructor(injector: Injector,
        private _idealClientService: CompanyProspectServiceProxy,
        private _companyService: CompanyServiceProxy,
        private _contactService: ContactProspectServiceProxy) {
        super(injector)
    }

    ngOnInit() {

    }
    show(id: string, backgroundClass: string): void {
        this._idealClientService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.colorBackground = backgroundClass;
                this.modal.show();
                this.getContacts();
            })
            .subscribe((result: CompanyProspectDetailOutput) => {
                this.company = result;
                this.city = this.company.city;
                this.companySize = this.company.size;
                this.beeStatus = this.company.beeStatus;
            });

    }
    fullCompanyProfile(id: string, backgroundClass: string) {
        this._companyService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.colorBackground = backgroundClass;
                this.modal.show();
            })
            .subscribe((result: CompanyDetailOutput) => {
                this.company = result;
                this.city = this.company.city;
                this.companySize = this.company.size;
                this.beeStatus = this.company.beeStatus;
                this.getContacts();
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    getInitial(name: string) {
        return name.substring(0, 1);
    }
    getContacts() {
        this._contactService.getCompanyId(this.company.id).subscribe((result) => {
            this.totalContacts = result.items.length;
        })
    }
    viewContacts(company: CompanyProspect) {
        this.close();
        this.companyContacts.show(company);
    }
    getContactEnding() {
        return this.totalContacts === 1 ? 'Contact' : 'Contacts';
    }
}
