import {
    Component,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    Injector
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    CompanyListDto,
    Company,
    ContactListDto,
    ContactProspectListDto,
    ContactServiceProxy
} from 'shared/service-proxies/service-proxies';
import { AppComponentBase } from 'shared/app-component-base';

@Component({
    selector: 'app-company-contacts',
    templateUrl: './company-contacts.component.html',
    providers: [ContactServiceProxy]
})
export class CompanyContactsComponent extends AppComponentBase {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('companyContacts',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    active = false;
    saving = false;
    company: CompanyListDto[] = [];
    contacts: ContactListDto[] = [];
    prospects: ContactProspectListDto[] = [];
    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy

    ) {
        super(injector);
    }
    show(company: Company): void {
        this._contactService.getCompanyId(company.id).finally(() => {
            this.active = true;
            this.modal.show();
        }).subscribe((result) => {
            this.contacts = result.items;
        });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    getContacts() {
        this._contactService.getCompanyId(this.company[0].id).subscribe((result) => {
            this.contacts = result.items;
        })
    }
    getCompanyContacts(id: string) {
        this._contactService.getCompanyId(id).subscribe((result) => {
            this.prospects = result.items;
        });
    }
    qualify(contact: any): void {
        this._contactService.editStatus(contact.id, 'Qualify').subscribe(() => {
            abp.notify.success(this.l('Added Successfully'))
            this.close();
        });
    }
}
