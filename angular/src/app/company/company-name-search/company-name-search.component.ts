import { AppComponentBase } from 'shared/app-component-base';
import { Component, Injector, ViewChild } from '@angular/core';
import {
    CompanyProspectServiceProxy,
    CompanyProspectListDto,
    CompanyProspect,
    ContactProspectServiceProxy,
    ContactProspectListDto,
    CompanyProspectDetailOutput
} from 'shared/service-proxies/service-proxies';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

import { FormControl } from '@angular/forms';
import { appModuleAnimation } from 'shared/animations/routerTransition';
import { CompanyInfoModalComponent } from 'app/company/company-info/company-info.component';
import { CompanyProfileComponent } from '../company-profile/company-profile.component';


@Component({
    templateUrl: './company-name-search.component.html',
    providers: [CompanyProspectServiceProxy, ContactProspectServiceProxy],
    animations: [appModuleAnimation()]
})
export class CompanyNameSearchComponent extends AppComponentBase {

    @ViewChild('companyProfileModal',{static:false}) companyProfileModal: CompanyProfileComponent;
    @ViewChild('companyInfoModal',{static:false}) companyinfoModal: CompanyInfoModalComponent;
    searchTerm: FormControl = new FormControl();
    list: ContactProspectListDto[] = [];
    totalItems: number;
    page = 1;
    noOfItems = 10;
    search = false;
    searchResult: CompanyProspectListDto[] = [];
    Filter: string;
    constructor(injector: Injector,
        private _companyProspectService: CompanyProspectServiceProxy,
        private _contactProspectService: ContactProspectServiceProxy) {
        super(injector);
        this.populate();
    }
    display(): void {
        this._companyProspectService.nameSearch(this.Filter).subscribe((response) => {
            this.searchResult = response.items;
        })
    }
    populate() {
        this.searchTerm.valueChanges
            .pipe(debounceTime(1000))
            .subscribe(data => {
                this._companyProspectService.nameSearch(data).subscribe(response => {
                    this.searchResult = response.items;
                    this.search = this.searchResult.length > 0 ? true : false;
                    this.totalItems = this.searchResult.length;
                })
            })
    }
    showContacts(company: CompanyProspect) {
        this.companyinfoModal.show(company);
        this._contactProspectService.getCompanyId(company.id).subscribe((result) => {
            this.list = result.items;
        })
    }
    getInitial(name: string) {
        return name.substring(0, 1);
    }
    viewFullProfile(company: CompanyProspectDetailOutput, colorClass: number) {
        const color = colorClass % 2 === 0 ? 'even-class' : 'odd-class';
        this.companyProfileModal.show(company.id, color);
    }

}
