import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    CompanyListDto,
    CompanyServiceProxy,
    CompanyDetailOutput,
    ContactServiceProxy,
    ContactProspectListDto,
    Company,
    DataTableServiceProxy,
    ContactProspectServiceProxy
} from 'shared/service-proxies/service-proxies';
import { debounceTime, map } from 'rxjs/operators';
import { EditCompanyComponent, } from 'app/company/edit-company/edit-company.component';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { CreateCompanyModalComponent } from 'app/company/create-company/create-company.component';
import { CompanyContactsComponent } from 'app/company/company-contacts/company-contacts.component';
import { CreateDealComponent } from '@app/deals/create-deal/create-deal.component';
import { FormControl } from '@angular/forms';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { LoaderService } from '@app/services/loader.service';


@Component({
    selector: 'app-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.css'],
    animations: [appModuleAnimation()],
    providers: [ContactServiceProxy, DataTableServiceProxy, ContactProspectServiceProxy]
})

export class CompaniesComponent extends PagedListingComponentBase<CompanyListDto> {
    @ViewChild('createCompanyModal',{static:false}) createCompanyModal: CreateCompanyModalComponent;
    @ViewChild('editCompanyModal',{static:false}) editCompanyModal: EditCompanyComponent;
    @ViewChild('companyContacts',{static:false}) companyContacts: CompanyContactsComponent;
    @ViewChild('companyProfileModal',{static:false}) companyProfileModal: CompanyProfileComponent;
    @ViewChild('createDealComponent',{static:false}) createDealComponent: CreateDealComponent;
    companies: CompanyListDto[] = [];
    key = 'name';
    reverse = false;
    isLoading = false;
    prospects: ContactProspectListDto[] = [];
    filter = '';
    filterTerm: FormControl = new FormControl();
    constructor(
        injector: Injector,
        private _companyService: CompanyServiceProxy,
        private _contactService: ContactServiceProxy,
        private _contactProspectService: ContactProspectServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private _loaderService: LoaderService
    ) {
        super(injector);
    }
    list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this.isLoading = true;
        this._loaderService.show();
        this._companyService.getAll(request.sorting, request.skipCount, request.maxResultCount)
            .finally(() => {
                finishedCallback();
                this.isLoading = false;
            }).subscribe((result) => {
                this.companies = result.items;
                this.showPaging(result, pageNumber);
                this.isLoading = false;
                this._loaderService.hide();
            });
    }
    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }
    getRelatedContacts(id: string) {
        this._contactProspectService.getCompanyId(id).subscribe((result) => {
            const totalContacts = result.items.length;
            console.log(totalContacts)
            return totalContacts;
        })
    }
    createCompany(): void {
        this.createCompanyModal.show();
    }
    showCompanyInfo(company: Company): void {
        this.companyContacts.show(company);
        this.companyContacts.getCompanyContacts(company.id);
    }
    getCompanyContacts(id: string): number {
        this._contactService.getCompanyId(id).subscribe((result) => {
            this.prospects = result.items;
        });
        return this.prospects.length;
    }

    createDeal() {
        this.createDealComponent.show();
    }
    delete(company: CompanyListDto): void {
        abp.message.confirm(
            'Delete Company \'' + company.name + '\'?',
            (result: boolean) => {
                if (result) {
                    this._companyService.delete(company.id)
                        .finally(() => {
                            abp.notify.info('Deleted Company: ' + company.name);
                            this.refresh();
                        })
                        .subscribe(() => { });                                               
                }                
            }
        );
    }

    editCompany(company: CompanyDetailOutput): void {
        this.editCompanyModal.show(company.id);
    }

    filterCompanies() {
        this.filterTerm.valueChanges
            .pipe(debounceTime(1000))
            .subscribe(data => {
                this._companyService.companyFilter(data).subscribe(response => {
                    this.companies = response.items
                })
            })
        this._companyService.getAllCompanies(this.filter);
    }
    getIndustries(type: string) {
        this._dataTableService.getAll(type).subscribe((result) => {
        })
    }
    viewFullProfile(company: CompanyDetailOutput, colorClass: number) {
        const color = colorClass % 2 === 0 ? 'even-class' : 'odd-class';
        this.companyProfileModal.fullCompanyProfile(company.id, color);
    }
    getInitial(name: string) {
        return name.substring(0, 1)
    }
}

