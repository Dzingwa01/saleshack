import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppComponentBase } from '@shared/app-component-base';
import { DataTableTypes } from '@shared/models/data-table';
import { CompanyServiceProxy, CreateCompanyInput, DataTableListDto, DataTableServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'app-create-company',
    templateUrl: './create-company.component.html',
    providers: [DataTableServiceProxy]
})
export class CreateCompanyModalComponent extends AppComponentBase implements OnInit {
    companySize = '';
    phase = '';
    industry = '';
    source = '';
    businessType = '';
    cities: DataTableListDto[] = [];
    provinces: DataTableListDto[] = [];
    companySizes: DataTableListDto[] = [];
    beeStatuses: DataTableListDto[] = [];
    createCompanyForm: FormGroup;
    industries: DataTableListDto[] = [];

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createCompanyModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    company: CreateCompanyInput;
    active = false;
    saving = false;

    constructor(
        injector: Injector,
        private _companyService: CompanyServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder
    ) {
        super(injector);
    }

    ngOnInit() {
        this.initializeModalForm();
    }
    initializeModalForm() {
        const urlReg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
        this.createCompanyForm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(2),
            Validators.maxLength(64)]],
            email: ['', [Validators.required, Validators.email,
            Validators.minLength(5),
            Validators.maxLength(32)]],
            size: ['undefined', Validators.required],
            phase: ['undefined', Validators.required],
            turnover: ['', [Validators.required, Validators.minLength(4)]],
            yearInBusiness: ['', Validators.required],
            webUrl: ['', [Validators.required, Validators.pattern(urlReg)]],
            telephone: ['', [Validators.required,
            Validators.minLength(10), Validators.maxLength(10)]],
            industry: ['undefined', Validators.required],
            businessType: ['undefined', Validators.required],
            stage: ['undefined', Validators.required],
            beeStatus: ['undefined', Validators.required],
            address: ['', [Validators.required, Validators.minLength(5)]],
            province: ['undefined', Validators.required],
            source: ['undefined', Validators.required],
            city: ['undefined', Validators.required],
            postalCode: ['', [Validators.required, Validators.minLength(2)]],
            country: ['South Africa', Validators.required],
            specialEvents: [''],
            facebook: [''],
            linkedIn: [''],
            googlePlus: [''],
            instagram: [''],
            twitter: [''],
        })
    }
    onShown() {
        $(this.nameInput.nativeElement).focus();
        this.getIndustries();
        this.getCities();
        this.getProvinces();
        this.getCompanySizes();
        this.getBeeStatuses();
    }
    show(): void {
        this.active = true;
        this.company = new CreateCompanyInput();
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this.company = Object.assign({}, this.createCompanyForm.value)
        this._companyService.create(this.company)
            .finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(this.company);
            });
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        })
    }
    getCompanySizes() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySizes = result.items;
        })
    }
    getProvinces() {
        this._dataTableService.getAll(DataTableTypes.PROVINCE).subscribe((result) => {
            this.provinces = result.items;
        })
    }
    getBeeStatuses() {
        this._dataTableService.getAll(DataTableTypes.BEE_STATUS).subscribe((result) => {
            this.beeStatuses = result.items;
        })
    }
}
