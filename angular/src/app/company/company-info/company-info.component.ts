import { Component, ViewChild, Injector, ElementRef, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    CompanyListDto,
    ContactListDto,
    ContactServiceProxy,
    Company,
    ProspectListServiceProxy,
    ContactProspect,
    ContactProspectServiceProxy,
    ContactProspectListDto,
    CompanyProspect
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
    selector: 'app-company-info',
    templateUrl: './company-info.component.html',
    providers: [ContactServiceProxy, ProspectListServiceProxy, ContactProspectServiceProxy]
})
export class CompanyInfoModalComponent extends AppComponentBase {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('companyInfoModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    active = false;
    saving = false;
    company: CompanyListDto[] = [];
    company2: Company[] = [];
    contacts: ContactListDto[] = [];
    prospects: ContactProspectListDto[] = [];
    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy,
        private prospectList: ProspectListServiceProxy,
        private _contactProspectService: ContactProspectServiceProxy

    ) {
        super(injector);
    }
    show(company: CompanyProspect): void {
        this._contactProspectService.getCompanyId(company.id).finally(() => {
            this.active = true;
            this.modal.show();
        }).subscribe((result) => {
            this.prospects = result.items;
        })
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    getContacts() {
        this._contactService.getCompanyId(this.company[0].id).subscribe((result) => {
            this.contacts = result.items;
        })
    }
    getCompanyContacts(id: string) {
        this._contactService.getCompanyId(id).subscribe((result) => {
            this.prospects = result.items;
        });
    }
    getContactProspects() {

    }
    add(contact: ContactProspect) {
        console.log('clicked')
        this.prospectList.create(contact.id).finally(() => this.saving = false)
            .subscribe(() => {
                abp.notify.success(this.l('Added Successfully'));
            });
    }
}
