import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/app-component-base';
import { CompanyServiceProxy, CompanyDetailOutput, DataTableServiceProxy, DataTableListDto } from 'shared/service-proxies/service-proxies';

import * as _ from 'lodash';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTableTypes } from '@shared/models/data-table';

@Component({
    selector: 'app-edit-company',
    templateUrl: './edit-company.component.html'
})
export class EditCompanyComponent extends AppComponentBase implements OnInit {

    @ViewChild('editCompanyModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    industries: DataTableListDto[];
    cities: DataTableListDto[] = [];
    provinces: DataTableListDto[] = [];
    companySizes: DataTableListDto[] = [];
    beeStatuses: DataTableListDto[] = [];
    active = false;
    saving = false;

    company: CompanyDetailOutput = null;
    editCompanyForm: FormGroup;

    constructor(
        injector: Injector,
        private _companyService: CompanyServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.initializeModalForm();
    }
    initializeModalForm() {
        const urlReg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
        this.editCompanyForm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(2),
            Validators.maxLength(64)]],
            email: ['', [Validators.required, Validators.email,
            Validators.minLength(5),
            Validators.maxLength(32)]],
            size: ['undefined', Validators.required],
            phase: ['undefined', Validators.required],
            turnover: ['', [Validators.required, Validators.minLength(4)]],
            yearInBusiness: ['', Validators.required],
            webUrl: ['', [Validators.required, Validators.pattern(urlReg)]],
            telephone: ['', [Validators.required,
            Validators.minLength(10), Validators.maxLength(10)]],
            industry: ['undefined', Validators.required],
            businessType: ['undefined', Validators.required],
            stage: ['undefined', Validators.required],
            beeStatus: ['undefined', Validators.required],
            address: ['', [Validators.required, Validators.minLength(5)]],
            province: ['undefined', Validators.required],
            source: ['undefined', Validators.required],
            city: ['undefined', Validators.required],
            postalCode: ['', [Validators.required, Validators.minLength(2)]],
            country: ['South Africa', Validators.required],
            specialEvents: [''],
            facebook: [''],
            linkedIn: [''],
            googlePlus: [''],
            instagram: [''],
            twitter: [''],
        })
    }

    show(id: string): void {
        this._companyService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: CompanyDetailOutput) => {
                this.company = result;
                this.editCompanyForm.patchValue(result)
            });
    }
    onShown(): void {
        $(this.nameInput.nativeElement).focus();
        this.getIndustries();
        this.getCities();
        this.getProvinces();
        this.getCompanySizes();
        this.getBeeStatuses();
    }
    save(): void {
        this.saving = true;
        this.company = Object.assign({}, this.editCompanyForm.value);
        this._companyService.editCompany(this.company)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        })
    }
    getCompanySizes() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySizes = result.items;
        })
    }
    getProvinces() {
        this._dataTableService.getAll(DataTableTypes.PROVINCE).subscribe((result) => {
            this.provinces = result.items;
        })
    }
    getBeeStatuses() {
        this._dataTableService.getAll(DataTableTypes.BEE_STATUS).subscribe((result) => {
            this.beeStatuses = result.items;
        })
    }
}
