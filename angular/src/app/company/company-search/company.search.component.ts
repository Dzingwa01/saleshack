import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    CompanyProspectServiceProxy,
    CompanyProspectListDto,
    CompanyProspect,
    ContactProspectServiceProxy,
    ContactProspectListDto,
    DataTableListDto,
    DataTableServiceProxy,
    CompanyProspectDetailOutput
} from 'shared/service-proxies/service-proxies';
import { CompanyInfoModalComponent } from 'app/company/company-info/company-info.component';
import { DataTableTypes } from '@shared/models/data-table';
import { CompanyProfileComponent } from '../company-profile/company-profile.component';

@Component({
    templateUrl: './company.search.component.html',
    animations: [appModuleAnimation()],
    providers: [CompanyProspectServiceProxy, ContactProspectServiceProxy]
})

export class CompanySearchComponent extends AppComponentBase implements OnInit {

    @ViewChild('companyInfoModal',{static:false}) companyinfoModal: CompanyInfoModalComponent;
    @ViewChild('companyProfileModal',{static:false}) companyProfileModal: CompanyProfileComponent;

    cities: DataTableListDto[] = [];
    beeStatus: DataTableListDto[] = [];
    industries: DataTableListDto[] = [];
    companySizes: DataTableListDto[] = [];
    companyProspects: CompanyProspectListDto[] = [];
    list: ContactProspectListDto[] = [];
    city = '';
    beeStatusString = '';
    country = 'South Africa';
    industry = '';
    companySize = '';
    showSpinner = false;
    companySearch = false;
    isVisible: boolean;
    totalItems: number;
    page = 1;
    noOfItems = 10;

    constructor(
        injector: Injector,
        private companyProspectService: CompanyProspectServiceProxy,
        private _contactProspectService: ContactProspectServiceProxy,
        private _dataTableService: DataTableServiceProxy
    ) {
        super(injector);
    }
    ngOnInit() {
        this.getCities();
        this.getIndustries();
        this.getCompanySizes();
        this.getJobTitles();
    }

    ShowTableResults() {
        this.companySearch = true;
    }
    showCompanyInfo(company: CompanyProspect): void {
        this.companyinfoModal.show(company);
    }
    search(): void {
        this.companySearch = true;
        this.companyProspectService.getAll(this.country, this.industry, this.city,
            this.companySize, this.beeStatusString).subscribe((results) => {
                this.showSpinner = false;
                this.companyProspects = results.items;
                this.totalItems = this.companyProspects.length;
            })
    }
    showContacts(company: CompanyProspect) {

        this.companyinfoModal.show(company);
        this._contactProspectService.getCompanyId(company.id).subscribe((result) => {
            this.list = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        });
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        });
    }

    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.BEE_STATUS).subscribe((result) => {
            this.beeStatus = result.items;
        });
    }
    getCompanySizes() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySizes = result.items;
        });
    }
    getInitial(name: string) {
        return name.substring(0, 1);
    }
    viewFullProfile(company: CompanyProspectDetailOutput, colorClass: number) {
        const color = colorClass % 2 === 0 ? 'even-class' : 'odd-class';
        this.companyProfileModal.show(company.id, color);
    }
}
