import { AppComponentBase } from 'shared/app-component-base';
import { appModuleAnimation } from 'shared/animations/routerTransition';
import { Injector, Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { EmailServiceProxy, CreateEmailInput } from 'shared/service-proxies/service-proxies';

declare var $: any;
@Component({
    selector: 'app-new-email',
    templateUrl: './new-email.component.html',
    animations: [appModuleAnimation()],
    styleUrls: ['./new-email.component.css']

})
export class NewEmailComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('newEmailModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active = false;
    saving = false;
    email: CreateEmailInput;

    constructor(injector: Injector, private _emailService: EmailServiceProxy) {
        super(injector)
    }
    ngOnInit() {
        this.email = new CreateEmailInput();
        $('#summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            placeholder: 'Write Your Message Here...'
        });
    }
    show(): void {
        this.active = true;
        this.email = new CreateEmailInput();
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    sendEmail(): void {
        const message = $('#summernote').summernote('code');
        this.email.message = message;
        this._emailService.createEmail(this.email)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.success(this.l('Email Sent Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
}
