import { DataTableListDto, DataTableServiceProxy } from '@shared/service-proxies/service-proxies';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DataTableTypes } from '@shared/models/data-table';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';

@Injectable()
export class IndustriesResolver implements Resolve<DataTableListDto> {
    constructor(private _router: Router, private _dataTableService: DataTableServiceProxy) { }
    resolve(_route: ActivatedRouteSnapshot): Observable<DataTableListDto> {
        return this._dataTableService.getAll(DataTableTypes.INDUSTRY).pipe(
            catchError(error => {
                console.log(error)
                this._router.navigate(['/app/user/customer-profile']);
                return of(null);
            }))
    }
}
