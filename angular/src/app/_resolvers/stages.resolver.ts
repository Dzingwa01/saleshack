import { Stage, DealServiceProxy } from '@shared/service-proxies/service-proxies';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';

@Injectable()
export class StagesResolver implements Resolve<Stage[]> {
    constructor(private _router: Router, private _dealService: DealServiceProxy) { }
    resolve(_route: ActivatedRouteSnapshot): Observable<Stage[]> {
        return this._dealService.getStages().pipe(
            catchError(error => {
                this._router.navigate(['/app/user/customer-profile']);
                return of(null);
            }))
    }
}
