import { Component, Injector, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    IdealClientServiceProxy,
    CompanyProspectListDto,
    CompanyProspectDetailOutput,
    CompanyProspect,
    DealServiceProxy,
    Stage,
    NoteServiceProxy,
    TasksServiceProxy,
    NoteListDto,
    TasksListDto,
    ActivityListDto,
    ActivityServiceProxy,
    CompanyServiceProxy
} from '@shared/service-proxies/service-proxies';
import { DataTableTypes } from '@shared/models/data-table';
import { ViewClientComponent } from '@app/prospects/ideal-client/view-client/view-client.component';
import { CompanyInfoModalComponent } from '@app/company/company-info/company-info.component';
import { ActivatedRoute } from '@angular/router';


@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [ActivityServiceProxy, CompanyServiceProxy],
    animations: [appModuleAnimation()]
})
export class HomeComponent extends AppComponentBase implements AfterViewInit, OnInit {
    @ViewChild('viewIdealClientModal',{static:false}) viewIdealClientModal: ViewClientComponent;
    @ViewChild('companyInfoModal',{static:false}) companyContacts: CompanyInfoModalComponent;
    industries: string[] = [];
    cities: string[] = [];
    beeStatus: string[] = [];
    companySize: string[] = [];
    estBracket: string[] = [];
    totalItems: number;
    page = 1;
    noOfItems = 8;
    stages: Stage[];
    totalDeals = 0;
    userId: number;
    totalAmount = 0;
    notes: NoteListDto[] = [];
    tasks: TasksListDto[] = [];
    activities: ActivityListDto[] = [];
    idealClients: CompanyProspectListDto[] = [];

    constructor(
        injector: Injector,
        private _route: ActivatedRoute,
        private _idealClientService: IdealClientServiceProxy,
        private _dealService: DealServiceProxy,
        private _noteService: NoteServiceProxy,
        private _tasksService: TasksServiceProxy,
        private _activityService: ActivityServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.userId = abp.session.userId;
        this.getIndustries();
        this.getCities();
        this.getBeeStatus();
        this.getCompanySize();
        this.getEstBracket();
        this.getStages();
        this.getAll();
        this.getNotes();
        this.getTasks();
        this.getActivities();
    }
    ngAfterViewInit(): void {

    }
    getAll() {
        if (this.industries.length > 0 &&
            this.beeStatus.length > 0 &&
            this.companySize.length > 0 &&
            this.estBracket.length > 0) {
            this._idealClientService.getAllCompanies(this.industries, this.cities,
                this.beeStatus, this.companySize, this.estBracket).subscribe((result) => {
                    this.idealClients = result.items.slice(1, 5);
                    this.totalItems = result.items.length;
                });
        } else {
            this._idealClientService.getDashBoardClients(abp.session.userId).subscribe((results) => {
                this.idealClients = results.items.slice(1, 5);
            })
        }
    }
    getIndustries() {
        this._idealClientService.getDefaultValues(DataTableTypes.INDUSTRY).subscribe((result) => {
            result.items.forEach((value) => {
                this.industries.push(value.value);
            })
        })
    }

    getCities() {
        this._idealClientService.getDefaultValues(DataTableTypes.CITY).subscribe((result) => {
            result.items.forEach((value) => {
                this.cities.push(value.value);
            })
        })
    }

    getBeeStatus() {
        this._idealClientService.getDefaultValues(DataTableTypes.BEE_STATUS).subscribe((result) => {
            result.items.forEach((value) => {
                this.beeStatus.push(value.value);
            })
        })
    }
    getCompanySize() {
        this._idealClientService.getDefaultValues(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            result.items.forEach((value) => {
                this.companySize.push(value.value);
            })
        })
    }
    getEstBracket() {
        this._idealClientService.getDefaultValues(DataTableTypes.EST_BRACKET).subscribe((result) => {
            result.items.forEach((value) => {
                this.estBracket.push(value.value);
            })
        })
    }
    getInitial(name: string) {
        return name !== null ? name.substring(0, 1) : 'U';
    }

    viewFullProfile(client: CompanyProspectDetailOutput, colorClass: number) {
        const color = colorClass % 2 === 0 ? 'even-class' : 'odd-class';
        this.viewIdealClientModal.show(client.id, color);
    }
    viewContacts(company: CompanyProspect) {
        this.companyContacts.show(company);
    }

    getStages() {
        // this._route.data.subscribe(data => {
        //     this.stages = data['stages'].result;
        // })
        this._dealService.getStages().subscribe((result) => {
            this.stages = result.items;
        });
    }
    getStageInfo(id: string) {
        this._dealService.getByStage(id).subscribe((result) => {
            this.totalDeals = result.items.length;
            result.items.forEach((value) => {
                this.totalAmount += value.expectedAmount;
            })
        })
    }
    getNumDeals(id: string) {
        this._dealService.getByStage(id).subscribe(result => result.items.length);
    }
    onMouseleave() {
        this.totalDeals = 0;
        this.totalAmount = 0;
    }
    getNotes() {
        this._noteService.getAllUserNotes(this.userId).subscribe((result) => {
            this.notes = result.items;
        });
    }
    getTasks() {
        this._tasksService.getAllUserTasks(this.userId).subscribe((result) => {
            this.tasks = result.items;
        });
    }
    getActivities() {
        this._activityService.getAllActivities().subscribe((result) => {
            this.activities = result.items;
        });
    }
}
