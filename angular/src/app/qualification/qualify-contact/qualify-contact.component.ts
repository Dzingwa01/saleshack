import { Component, ViewChild, Injector, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from 'shared/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { ContactServiceProxy, ContactDetailOutput } from 'shared/service-proxies/service-proxies';


@Component({
    selector: 'app-create-qualification',
    templateUrl: './qualify-contact.component.html',

})

export class QualifyComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createQualificationModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active = false;
    saving = false;
    value = 0;
    pos: number;
    radios: any;
    optradio = 0; optradio1 = 0; optradio2 = 0; optradio3 = 0; optradio4 = 0;
    optradio5 = 0; optradio6 = 0; optradio7 = 0; optradio8 = 0; optradio9 = 0;


    contact: ContactDetailOutput = null;
    constructor(injector: Injector, private _contactService: ContactServiceProxy) {

        super(injector);

    }
    show(id: string): void {
        this._contactService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: ContactDetailOutput) => {
                this.contact = result;
                console.log(this.contact)
            });

    }
    close(): void {
        this.modal.hide();
        this.active = false;
        this.clearForm();
    }


    isQualified() {
        this.value = 0;
        this.value = parseInt(this.optradio.toString(), 10) + parseInt(this.optradio1.toString(), 10) +
            parseInt(this.optradio2.toString(), 10) + parseInt(this.optradio3.toString(), 10) +
            parseInt(this.optradio4.toString(), 10) + parseInt(this.optradio5.toString(), 10) +
            parseInt(this.optradio6.toString(), 10) + parseInt(this.optradio7.toString(), 10) +
            parseInt(this.optradio8.toString(), 10) + parseInt(this.optradio9.toString(), 10);
    }

    save(): void {
        this.saving = true;
        if (this.value >= 7) {
            this.contact.status = 'Qualified';
        } else {
            this.contact.status = 'Recycle';
        }
        this._contactService.editContact(this.contact)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
        location.reload();
    }
    clearForm() {
        this.optradio = 0; this.optradio1 = 0; this.optradio2 = 0; this.optradio3 = 0;
        this.optradio4 = 0; this.optradio5 = 0; this.optradio6 = 0; this.optradio7 = 0;
        this.optradio8 = 0; this.optradio9 = 0;
        this.value = 0;
    }

}
