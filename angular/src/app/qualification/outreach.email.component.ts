import { AppComponentBase } from 'shared/app-component-base';
import { Injector, Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { appModuleAnimation } from 'shared/animations/routerTransition';

import { ModalDirective } from 'ngx-bootstrap';
import { OutreachServiceProxy, EmailDto } from 'shared/service-proxies/service-proxies';

declare var $: any;

@Component({
    selector: 'app-outreach-email',
    templateUrl: './outreach.email.component.html',
    animations: [appModuleAnimation()],
    providers: [OutreachServiceProxy]
})

export class OutreachEmailComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createOutreachModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active = false;
    saving = false;
    postText = '';
    errorMessage: string;
    postSaved = false;
    email: EmailDto;
    emails: any[] = [];

    constructor(injector: Injector,  private _outreachService: OutreachServiceProxy) {
        super(injector)
    }
    ngOnInit(): void {
        this.email = new EmailDto();
        $('#summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            placeholder: 'Write Your Message Here...',
            height: 250
        });
    }

    show(): void {
        this.active = true;
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    save(): void {
        if (this.emails.length > 1) {
            this.email.isBulk = true;
            this.email.recipients = this.emails;
        } else {
            this.email.recipient = this.emails[0];
        }
        const message = $('#summernote').summernote('code');

        this.email.message = message;
        this._outreachService.sendEmail(this.email)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.success(this.l('Email Sent Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
}
