import { AppComponentBase } from 'shared/app-component-base';
import { OnInit, Component, Injector, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { OutreachServiceProxy, SmsDto } from 'shared/service-proxies/service-proxies';

@Component({
    selector: 'app-outreach-sms',
    templateUrl: './outreach.sms.component.html',
    providers: [OutreachServiceProxy]
})


export class OutreachSMSComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createSMSOutreachModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active = false;
    saving = false;
    contacts: any[] = [];
    sms: SmsDto;

    constructor(injector: Injector,
        private _outreachAppService: OutreachServiceProxy) {
        super(injector)
    }

    ngOnInit(): void {

    }

    show(): void {
        this.active = true;
        this.sms = new SmsDto();
        console.log('Contacts: ' + this.contacts)
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    save(): void {
        if (this.contacts.length > 1) {
            this.sms.isBulk = true;
            this.sms.recipients = this.contacts;
        } else {
            this.sms.recipient = this.contacts[0];
        }
        this._outreachAppService.sendSms(this.sms)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('Sent Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

}
