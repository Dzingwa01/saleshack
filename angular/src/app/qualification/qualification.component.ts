import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ContactListDto, ContactServiceProxy, ContactDetailOutput } from 'shared/service-proxies/service-proxies';
import { QualifyComponent } from 'app/qualification/qualify-contact/qualify-contact.component';
import { OutreachEmailComponent } from 'app/qualification/outreach.email.component';
import { OutreachSMSComponent } from 'app/qualification/outreach.sms.component';
import { CreateDealComponent } from '@app/deals/create-deal/create-deal.component';
import * as moment from 'moment';

@Component({
    templateUrl: './qualification.component.html',
    animations: [appModuleAnimation()],
    providers: [ContactServiceProxy]
})

export class QualificationComponent extends AppComponentBase implements OnInit {

    @ViewChild('createQualificationModal',{static:false}) createQualificationModal: QualifyComponent;
    @ViewChild('createOutreachModal',{static:false}) createOutreachModal: OutreachEmailComponent;
    @ViewChild('createSMSOutreachModal',{static:false}) createSMSOutreachModal: OutreachSMSComponent;
    @ViewChild('createDealComponent',{static:false}) createDealComponent: CreateDealComponent;
    list: ContactListDto[] = []
    isQualified = false;
    input = 'New';
    recycle = false;
    disabled = true;
    ids = [];
    status: string;
    selectedList = [];
    emails = [];
    btnQuote = true;

    constructor(
        injector: Injector,
        private _contactService: ContactServiceProxy
    ) {
        super(injector);
    }
    ngOnInit() {
        this.getAll(0, 'Qualify');
    }
    getAll(input: number, status: string) {
        this._contactService.getQualificationContacts(this.input).subscribe((result) => {
            this.list = result.items.filter(x => x.status === status
                && x.lastModificationTime.subtract(input) <= (moment(new Date())))
            this.isQualified = status !== 'Qualify' ? true : false;
            this.status = this.isQualified ? null : 'Qualify';
        })
    }
    getLast90Days(input: number, status: string) {
        this._contactService.getQualificationContacts(this.input).subscribe((result) => {
            this.list = result.items.filter(x => x.status === status
                && x.lastModificationTime.subtract(input) >= (moment(new Date())))
            this.isQualified = status !== 'Qualify' ? true : false;
            this.status = this.isQualified ? null : 'Qualify';
        })
    }
    createModal(contact: ContactDetailOutput): void {
        this.createQualificationModal.show(contact.id);
    }
    showEmailOutreach(): void {
        this.createOutreachModal.emails = this.emails;
        this.createOutreachModal.show();

    }
    showSMSOutreach(): void {
        this.createSMSOutreachModal.contacts = this.ids;
        this.createSMSOutreachModal.show();
    }

    showDealModal(): void {
        this.createDealComponent.show();
    }
    getStatus(i: number) {
        if (this.list[i].status !== 'Qualify' && this.list[i].status !== 'Recycle') {
            this.recycle = false;
            return false;
        } else if (this.list[i].status === 'Qualify') {
            this.recycle = false;
            return false;
        } else {
            this.recycle = true;
            return true;
        }
    }
    showActions() {
        const list = this.list.find((e => e.status === 'Qualify'));
        return list.status === 'Qualified' ? true : false;
    }
    toggleSelection(id: string) {
        const idx = this.ids.indexOf(id);
        if (this.ids.length > 1) {
            this.btnQuote = false;
        } else {
            this.btnQuote = true;
        }
        // is currently selected
        if (idx > -1) {
            if (id.length >= 1) {
                this.ids.splice(idx, 1);
            }
        } else {

            this.ids.push(id);
            this.disabled = false;
        }
        if (this.ids.length > 1) {
            this.disabled = false;
        }

    }
    emailSelection(id: string) {
        const idx = this.emails.indexOf(id);
        if (this.emails.length > 1) {
            this.btnQuote = false;
        } else {
            this.btnQuote = true;
        }
        // is currently selected
        if (idx > -1) {
            if (id.length >= 1) {
                this.emails.splice(idx, 1);
            }
        } else {
            this.emails.push(id);
            this.disabled = false;
        }
        if (this.emails.length > 1) {
            this.disabled = false;
        }
        this.createOutreachModal.emails = this.emails;
    }
    selectAll() {
        this.list.forEach((el) => {
            if (this.selectedList.indexOf(el.id) === -1) {
                this.selectedList.push(el.id);
            }
        });
    }
    selectOne(id: string) {
        this.list.forEach((value) => {
        })
    }
}
