import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injector } from '@angular/core';
import { ModalDirective, BsDatepickerConfig } from 'ngx-bootstrap';
import { AppComponentBase } from 'shared/app-component-base';
import { DealServiceProxy, CompanyListDto, ContactListDto, UserDto, CreateDealInput, Stage } from 'shared/service-proxies/service-proxies';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-create-deal',
    templateUrl: './create-deal.component.html',
    providers: [DealServiceProxy]
})
export class CreateDealComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createDealComponent',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    active = false;
    saving = false;
    companies: CompanyListDto[] = [];
    contacts: ContactListDto[] = [];
    users: UserDto[] = [];
    stages: Stage[] = [];
    selectedCompany = 0;
    deal: CreateDealInput;
    createDealForm: FormGroup;
    datePickerConfig: Partial<BsDatepickerConfig>;
    constructor(injector: Injector, private _dealService: DealServiceProxy,
        private fb: FormBuilder) {
        super(injector);
        this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
    }
    ngOnInit() {
        this.initializeForm();
        this.getCompanies();
        this.getUsers();
        this.getStages();
    }
    initializeForm() {
        this.createDealForm = this.fb.group({
            name: ['', Validators.required],
            ownerId: ['undefined', Validators.required],
            companyId: ['undefined', Validators.required],
            expectedAmount: ['', Validators.required],
            pipelineStageId: ['undefined', Validators.required],
            expectedCloseDate: ['', Validators.required]
        })
    }
    show() {
        this.active = true;
        this.deal = new CreateDealInput();
        this.modal.show();
    }
    close() {
        this.modal.hide();
        this.active = false;
    }
    getCompanies() {
        this._dealService.getCompanies().subscribe((result) => {
            this.companies = result.items;
        });
    }
    getUsers() {
        this._dealService.getUsers().subscribe((result) => {
            this.users = result.items;
        });
    }

    getStages() {
        this._dealService.getStages().subscribe((result) => {
            this.stages = result.items;
        })
    }
    save() {
        this.deal = Object.assign({}, this.createDealForm.value);
        this._dealService.create(this.deal).finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.info(this.l('Created Successfully'));
                this.close();
                this.modalSave.emit(this.deal);
            });

    }
    saveAndCreate() {
        // TODO: implement Method
    }
}
