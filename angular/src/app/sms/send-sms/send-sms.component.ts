import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injector } from '@angular/core';
import { AppComponentBase } from 'shared/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { SmsDto, OutreachServiceProxy } from 'shared/service-proxies/service-proxies';

@Component({
    selector: 'app-send-sms',
    templateUrl: './send-sms.component.html',
    styleUrls: ['./send-sms.component.css'],
    providers: [OutreachServiceProxy]
})
export class SendSmsComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('sendSmSModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active = false;
    saving = false;

    sms: SmsDto = new SmsDto();
    constructor(injector: Injector,
        private _outreachAppService: OutreachServiceProxy) {

        super(injector)
    }

    ngOnInit(): void {
    }

    show(): void {
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    save(): void {
        this._outreachAppService.sendSms(this.sms)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('Sent Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

}
