import { Component, OnInit } from '@angular/core';
import { CardStore } from 'app/kanban/cardStore';
import { ListSchema } from 'app/kanban/listSchema';
import { DealServiceProxy, Stage, DealListDto } from 'shared/service-proxies/service-proxies';
import { ListComponent } from 'app/kanban/list/list.component';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.css'],
    providers: [DealServiceProxy]
})
export class BoardComponent implements OnInit {

    list: ListComponent;
    cardStore: CardStore;
    lists: ListSchema[];
    stages: Stage[] = [];
    card: Array<string> = [];
    dealStages: ListSchema[] = [];
    deals: DealListDto[] = [];
    showSpinner = true;
    constructor(private _dealService: DealServiceProxy) { }
    ngOnInit() {
        this.getStages();
        this.initializeStages();
    }
    initializeStages(): void {
        this.lists = this.dealStages;
    }
    getStages() {
        this.cardStore = new CardStore()
        this._dealService.getStages().subscribe((result) => {
            this.stages = result.items;
        });
        this.populateList();
    }
    populateDeals(id: string) {
        this._dealService.getDeals(id).subscribe((result) => {
            this.deals = result.items;
        })
    }
    populateList() {
        this.card = [];
        for (let i = 0; i < this.stages.length; i++) {
            this.populateDeals(this.stages[i].id);
            for (let r = 0; r < this.deals.length; r++) {
                this.card.push(this.cardStore.newCard(this.deals[r]));
            }
            const stage = new ListSchema(this.stages[i].name, this.stages[i].id, this.card);
            this.dealStages.push(stage);
            this.deals = [];
            this.card = [];
        }
    }
}
