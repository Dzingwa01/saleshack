import { Component, OnInit, Input, Injector, ViewChild } from '@angular/core';
import { ListSchema } from 'app/kanban/listSchema';
import { CardStore } from 'app/kanban/cardStore';
import { DealServiceProxy, Stage } from 'shared/service-proxies/service-proxies';
import { AppComponentBase } from 'shared/app-component-base';
import { CreateDealComponent } from '@app/deals/create-deal/create-deal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    providers: [DealServiceProxy]
})
export class ListComponent extends AppComponentBase implements OnInit {
    @ViewChild('createDealComponent',{static:false}) createDealComponent: CreateDealComponent;
    @Input() list: ListSchema;
    @Input() cardStore: CardStore;
    displayAddCard = false;
    stages: Stage[];
    totalDeals = 0;
    totalAmount = 0;

    constructor(private injetor: Injector, private _dealService: DealServiceProxy) {
        super(injetor)
    }
    ngOnInit() {
        this.getStages();
    }
    toggleDisplayAddCard() {
        this.displayAddCard = !this.displayAddCard;
    }
    allowDrop($event) {
        $event.preventDefault();
    }

    drop($event, stageId) {
        $event.preventDefault();
        const data = $event.dataTransfer.getData('text');
        const dealId = $event.dataTransfer.getData('dealId');
        this._dealService.updateStage(dealId, stageId).subscribe(() => { })

        let target = $event.target;
        const targetClassName = target.className;

        while (target.className !== 'list') {
            target = target.parentNode;
        }
        target = target.querySelector('.cards');

        if (targetClassName === 'card') {
            $event.target.parentNode.insertBefore(document.getElementById(data), $event.target);

        } else if (targetClassName === 'list__title') {
            if (target.children.length) {
                target.insertBefore(document.getElementById(data), target.children[0]);
            } else {
                target.appendChild(document.getElementById(data));
            }
        } else {
            target.appendChild(document.getElementById(data));
        }

    }
    createDeal() {
        this.createDealComponent.show();
    }
    getStages() {
        this._dealService.getStages().subscribe((result) => {
            this.stages = result.items;
        });
    }
    getStageInfo(id: string) {
        this._dealService.getByStage(id).subscribe((result) => {
            this.totalDeals = result.items.length;
            result.items.forEach((value) => {
                this.totalAmount += value.expectedAmount;
            })
        })
    }
    onMouseleave() {
        this.totalDeals = 0;
        this.totalAmount = 0;
    }
}
