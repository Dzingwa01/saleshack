import { Component, OnInit, Input, Output } from '@angular/core';
import { CardSchema } from 'app/kanban/cardSchema';
import { Router } from '@angular/router';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
    @Input() card: CardSchema;
    dealId: string;
    constructor(private _router: Router) { }

    ngOnInit() {
    }
    viewDeal(dealId: string) {
        this._router.navigate(['/app/pipeline/deals/' + dealId]);
    }
    dragStart(ev) {
        ev.dataTransfer.setData('text', ev.target.id);
        ev.dataTransfer.setData('dealId', ev.target.children[0].value);
    }
}
