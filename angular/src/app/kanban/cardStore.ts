import { CardSchema } from "app/kanban/cardSchema";
import { DealListDto } from "shared/service-proxies/service-proxies";

export class CardStore {
    cards: Object = {};
    lastid = -1;
    _addCard(card: CardSchema) {
        card.id = String(++this.lastid);
        this.cards[card.id] = card;
        return (card.id);
    }

    getCard(cardId: string) {
        return this.cards[cardId];
    }

    newCard(deal: DealListDto): string {
        const card = new CardSchema();
        card.dealId = deal.id;
        card.description = deal.name;
        return (this._addCard(card));
    }
}
