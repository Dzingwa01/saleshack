export class ListSchema {
    name: string;
    stageId: string;
    cards: string[];
    constructor(name: string, id: string, cards: string[]) {
        this.stageId = id;
        this.name = name;
        this.cards = cards;
    }
}
