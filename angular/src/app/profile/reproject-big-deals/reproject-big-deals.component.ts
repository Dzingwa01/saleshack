import { AppComponentBase } from "shared/app-component-base";
import { Injector, Component, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { appModuleAnimation } from "shared/animations/routerTransition";
import { ModalDirective } from "ngx-bootstrap";
import { UserDto, AccountServiceProxy } from "shared/service-proxies/service-proxies";
import { AbpSessionService } from "abp-ng2-module/src/session/abp-session.service";

@Component({
    selector: 'bigDealsModal',
    templateUrl: './reproject-big-deals.component.html',
    animations: [appModuleAnimation()],
    providers: [AccountServiceProxy, AbpSessionService]
})

export class BigDealsComponent extends AppComponentBase {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('bigDealsModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    active: boolean = false;
    saving: boolean = false;
    user: UserDto = new UserDto();

    constructor(injector: Injector,
        private _accountService: AccountServiceProxy,
        private _sessionService: AbpSessionService) {
        super(injector)
    }
    ngOnInit(): void {
        this.user.id = this._sessionService.userId;
    }
    save(): void {
        this.saving = true;
        this._accountService.reprojectBigDeals(this.user)
            .finally(() => { })
            .subscribe((result: string) => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
                location.reload();
            });
        location.reload();
        console.log("AverageDealSize = " + this.user.averageDealSizeMD);
    }

    show(): void {
        this.active = true;
        this.modal.show();
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
