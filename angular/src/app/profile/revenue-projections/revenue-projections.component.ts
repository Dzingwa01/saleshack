import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { MediumDealsComponent } from 'app/profile/reproject-medium-deals/reproject-medium-deals.component';
import { BigDealsComponent } from 'app/profile/reproject-big-deals/reproject-big-deals.component';
import { SmallDealsComponent } from 'app/profile/reproject-small-deals/reproject-small-deals.component';
import { UserDto, UserServiceProxy } from 'shared/service-proxies/service-proxies';
import { appModuleAnimation } from 'shared/animations/routerTransition';
import { AppComponentBase } from 'shared/app-component-base';

@Component({
    selector: 'app-revenue-projections',
    templateUrl: './revenue-projections.component.html',
    styleUrls: ['./revenue-projections.component.css'],
    animations: [appModuleAnimation()]
})
export class RevenueProjectionsComponent extends AppComponentBase  implements OnInit {
    @ViewChild('mediumDealsModal',{static:false}) mediumDealsModal: MediumDealsComponent;
    @ViewChild('bigDealsModal',{static:false}) bigDealsModal: BigDealsComponent;
    @ViewChild('smallDealsModal',{static:false}) smallDealsModal: SmallDealsComponent;

    user: UserDto = new UserDto();
    constructor(injector: Injector,
        private _userService: UserServiceProxy) {
    super(injector)}

    ngOnInit() {
        this.getUser();
    }
    getUser(): void {
        var id = abp.session.userId;
        this._userService.get(id)
            .subscribe(
            (result) => {
                this.user = result;
            });
    }
    reprojectMediumDeals(): void {
        this.mediumDealsModal.show();
    }
    reprojectSmallDeals(): void {
        this.smallDealsModal.show();
    }
    reprojectBigDeals(): void {
        this.bigDealsModal.show();
    }
}
