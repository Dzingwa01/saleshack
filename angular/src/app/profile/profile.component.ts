import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    UserDto,
    UserServiceProxy,
    DataTableServiceProxy,
    DataTableListDto,
    ProfilePhotoDto
} from 'shared/service-proxies/service-proxies';
import { MediumDealsComponent } from 'app/profile/reproject-medium-deals/reproject-medium-deals.component';
import { BigDealsComponent } from 'app/profile/reproject-big-deals/reproject-big-deals.component';
import { SmallDealsComponent } from 'app/profile/reproject-small-deals/reproject-small-deals.component';
import { DataTableTypes } from '@shared/models/data-table';

@Component({
    templateUrl: './profile.component.html',
    animations: [appModuleAnimation()]
})

export class ProfileComponent extends AppComponentBase implements OnInit {

    @ViewChild('mediumDealsModal',{static:false}) mediumDealsModal: MediumDealsComponent;
    @ViewChild('bigDealsModal',{static:false}) bigDealsModal: BigDealsComponent;
    @ViewChild('smallDealsModal',{static:false}) smallDealsModal: SmallDealsComponent;
    user: UserDto = null;
    jobTitles: DataTableListDto[] = [];
    departments: DataTableListDto[] = [];
    profilePhoto: ProfilePhotoDto = new ProfilePhotoDto();

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _dataTableService: DataTableServiceProxy

    ) {
        super(injector);
        this.show()
    }
    ngOnInit(): void {
        this.profilePhoto.userId = abp.session.userId;
        this.getJobTitles();
        this.getDepartments();
    }

    show(): void {
        const id = abp.session.userId;
        this._userService.get(id)
            .subscribe(
                (result) => {
                    this.user = result;
                });
    }
    reprojectMediumDeals(): void {
        this.mediumDealsModal.show();
    }
    reprojectSmallDeals(): void {
        this.smallDealsModal.show();
    }
    reprojectBigDeals(): void {
        this.bigDealsModal.show();
    }
    updateProfile() {

    }
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        })
    }
    getDepartments() {
        this._dataTableService.getAll(DataTableTypes.DEPARTMENT).subscribe((result) => {
            this.departments = result.items;
        })
    }
    uploadProfilePhoto() {
        this._userService.addProfilePhoto(this.profilePhoto).subscribe(() => {
            this.notify.success('Profile Pic Updated Successfully');
        })
    }
}
