import { Component, OnInit, Injector } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';

@Component({
    selector: 'app-total-available-market',
    templateUrl: './total-available-market.component.html',
    styleUrls: ['./total-available-market.component.css'],
    animations: [appModuleAnimation()],
})
export class TotalAvailableMarketComponent extends AppComponent implements OnInit {

    constructor(injector: Injector, private _router: Router) {
        super(injector)
    }

    ngOnInit() {
    }
    next() {
        this._router.navigate(['/app/feeds/ideal-client']);
    }
    redo() {
        this._router.navigate(['/app/user/customer-profile']);
    }

}
