import { Component, OnInit, Injector, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { appModuleAnimation } from 'shared/animations/routerTransition';
import { AppComponentBase } from 'shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import {
    DataTableServiceProxy,
    DataTableListDto,
    CreateIdealClientInput,
    IdealClientServiceProxy
} from '@shared/service-proxies/service-proxies';
import { DataTableTypes } from '@shared/models/data-table';
import { TagModel } from 'ngx-chips/core/accessor';
import { Observable } from 'rxjs/';
import { filter, map, } from 'rxjs/operators';

@Component({
    selector: 'app-customer-profile',
    templateUrl: './customer-profile.component.html',
    styleUrls: ['./customer-profile.component.css'],
    animations: [appModuleAnimation()],
    providers: [DataTableServiceProxy, IdealClientServiceProxy]
})
export class CustomerProfileComponent extends AppComponentBase implements OnInit {

    cities: DataTableListDto[] = []; cityList: string[] = [];
    beeStatus: DataTableListDto[] = []; beeStatusList: string[] = [];
    industries: DataTableListDto[] = []; industryList: string[] = [];

    companySizes: DataTableListDto[] = []; companySizeList: string[] = [];
    estBracket: DataTableListDto[] = []; estBracketList: string[] = [];
    ages: DataTableListDto[] = []; ageList: string[] = [];

    levels: DataTableListDto[] = []; levelList: string[] = [];
    jobTitles: DataTableListDto[] = []; jobTitleList: string[] = [];
    services = ['Web Development', 'Mobile Development', 'Prototyping'];

    defaultStatus = []; defaultCity = []; defaultIndustry = [];
    defaultCompanySize = []; defaultEst = []; defaultAge = [];
    defaultService = [];
    defaultJob = [];
    defaultLevel = [];

    delete = false;
    @ViewChild('myIndustries',{static:false}) selectedIndustries: ElementRef;
    idealClientInput: CreateIdealClientInput = new CreateIdealClientInput();
    myList: string[] = [];
    saving = false;
    constructor(injector: Injector, private _router: Router,
        private _dataTableService: DataTableServiceProxy,
        private _idealClientService: IdealClientServiceProxy) {
        super(injector)
    }
    ngOnInit(): void {
        this.getIndustries();
        this.getCities();
        this.getCompanySizes();
        this.getBeeStatuses();
        this.getEstBracket();
        this.getAges();
        this.getJobTitles();
        this.getCompanyLevel();
    }
    save() {
        this.initializeList();
        this._idealClientService.createIdealClient(this.idealClientInput).finally(() => {
            this.saving = false;
        }).subscribe(() => {
            this.notify.success(this.l('SavedSuccessfully'))
        })
        // this._router.navigate(['/app/user/customer-profile/total-available-market']);
        this._router.navigate(['/app/feeds/ideal-client']);
    }

    next() {
        this._router.navigate(['/app/user/customer-profile/total-available-market']);
    }
    /**
     *BEGIN Cities Related Code
     * */
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
            for (let i = 0; i < this.cities.length; i++) {
                this.cityList.push(this.cities[i].value)
            }
            this._idealClientService.getDefaultValues(DataTableTypes.CITY).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultCity.push(value.value);
                })
            })
        });
    }
    onCityAdd(tag: any): Observable<TagModel> {
        this.defaultCity.push(tag.value);
        return Observable
            .of(tag)
    }
    onCityRemoving(tag): Observable<TagModel> {
        // const confirm = window.confirm('Do you really want to remove this tag?');
        this._idealClientService.remove(tag).subscribe(() => {
            return Observable
                .of(tag)
        });
        return Observable
            .of(tag)

        // .filter(() => confirm);
    }
    onCityRemove(tag: any): Observable<TagModel> {
        this.defaultCity.splice(this.defaultCity.indexOf(tag.value));
        return Observable.
            of(tag)
    }
    public deleteTag(tag): void {
        abp.message.confirm(
            'Remove City \'' + tag + '\'?',
            (result: boolean) => {
                this.delete = result
                if (result) {
                    this._idealClientService.remove(tag)
                        .subscribe(() => {
                            abp.notify.info('Removed City: ' + tag);
                        });
                }
            }
        );
    }
    /**
     *END Cities Related Code
     * */


    /**
     * BEGIN Industry related Code
     * */
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
            for (let i = 0; i < this.industries.length; i++) {
                this.industryList.push(this.industries[i].value)
            }
            this._idealClientService.getDefaultValues(DataTableTypes.INDUSTRY).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultIndustry.push(value.value);
                })
            })
        });
    }
    onIndustryAdd(tag: any): Observable<TagModel> {
        this.defaultIndustry.push(tag.value);
        return Observable
            .of(tag)

    }
    onIndustryRemove(tag: any): Observable<TagModel> {
        const tagValue = tag.value;
        this.defaultIndustry.splice(this.defaultIndustry.indexOf(tag.value));
        this._idealClientService.remove(tagValue).subscribe(() => {

        });
        return Observable
            .of(tag);
    }
    /**
     *END Industry Related Code
     * */


    /**
     *BEGIN BeeStatus Related Code
     * */
    getBeeStatuses() {
        this._dataTableService.getAll(DataTableTypes.BEE_STATUS).subscribe((result) => {
            this.beeStatus = result.items;
            for (let i = 0; i < this.beeStatus.length; i++) {
                this.beeStatusList.push(this.beeStatus[i].value)
            }
            this._idealClientService.getDefaultValues(DataTableTypes.BEE_STATUS).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultStatus.push(value.value);
                })
            })
        });
    }
    onBeeAdd(tag: any): Observable<TagModel> {
        this.defaultStatus.push(tag.value);
        return Observable
            .of(tag)

    }
    onBeeRemove(tag: any): Observable<TagModel> {
        this.defaultIndustry.splice(this.defaultIndustry.indexOf(tag.value));
        return Observable
            .of(tag)
    }
    /**
   *END BeeStatus Related Code
   * */

    /**
   *BEGIN CompanySize Related Code
   * */
    getCompanySizes() {
        this._dataTableService.getAll(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            this.companySizes = result.items;
            for (let i = 0; i < this.companySizes.length; i++) {
                this.companySizeList.push(this.companySizes[i].value)
            }
            this._idealClientService.getDefaultValues(DataTableTypes.COMPANY_SIZE).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultCompanySize.push(value.value);
                })
            })
        });
    }
    onCompanySizeAdd(tag: any): Observable<TagModel> {
        this.defaultCompanySize.push(tag.value);
        return Observable
            .of(tag)

    }
    onCompanySizeRemove(tag: any): Observable<TagModel> {
        this.defaultCompanySize.splice(this.defaultCompanySize.indexOf(tag.value));
        return Observable
            .of(tag)
    }
    /**
   *END CompanySize Related Code
   * */

    /**
   *BEGIN Ages Related Code
   * */
    getAges() {
        this._dataTableService.getAll(DataTableTypes.AGE_BRACKET).subscribe((result) => {
            this.ages = result.items;
            this.ages.forEach(el => {
                this.ageList.push(el.value)
            });
            this._idealClientService.getDefaultValues(DataTableTypes.AGE_BRACKET).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultAge.push(value.value);
                })
            })
        });
    }

    onAgesAdd(tag: any): Observable<TagModel> {
        this.defaultAge.push(tag.value);
        return Observable
            .of(tag)

    }
    onAgesRemove(tag: any): Observable<TagModel> {
        this.defaultAge.splice(this.defaultAge.indexOf(tag.value));
        return Observable
            .of(tag)
    }
    /**
     *END Ages Related code
     * */

    /**
   *BEGIN Est Bracket Related Code
   * */
    getEstBracket() {
        this._dataTableService.getAll(DataTableTypes.EST_BRACKET).subscribe((result) => {
            this.estBracket = result.items;
            for (let i = 0; i < this.estBracket.length; i++) {
                this.estBracketList.push(this.estBracket[i].value)
            }
            this._idealClientService.getDefaultValues(DataTableTypes.EST_BRACKET).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultEst.push(value.value);
                })
            })
        });
    }
    onEstBracketAdd(tag: any): Observable<TagModel> {
        this.defaultEst.push(tag.value);
        return Observable
            .of(tag)

    }
    onEstBracketRemove(tag: any): Observable<TagModel> {
        this.defaultEst.splice(this.defaultEst.indexOf(tag.value));
        return Observable
            .of(tag)
    }
    /**
   *End Est Bracket Related Code
   * */


    /**
     * BEGIN Job Title Related Code
     */
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
            this.jobTitles.forEach(el => {
                this.jobTitleList.push(el.value);
            });
            this._idealClientService.getDefaultValues(DataTableTypes.JOB_TITLE).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultJob.push(value.value);
                })
            })
        })
    }

    onJobTitleAdd(tag: any): Observable<TagModel> {
        this.defaultJob.push(tag.value);
        return Observable
            .of(tag)

    }

    onJobTitleRemove(tag: any): Observable<TagModel> {
        this.defaultJob.splice(this.defaultJob.indexOf(tag.value));
        return Observable
            .of(tag)

    }
    /**
     * END Job Title Related Code
     */

    /**
     * BEGIN level at Company related code
     */
    getCompanyLevel() {
        this._dataTableService.getAll(DataTableTypes.LEVEL_AT_COMPANY).subscribe((result) => {
            this.levels = result.items;
            this.levels.forEach(el => {
                this.levelList.push(el.value)
            })

            this._idealClientService.getDefaultValues(DataTableTypes.LEVEL_AT_COMPANY).subscribe((res) => {
                res.items.forEach((value) => {
                    this.defaultLevel.push(value.value);
                })
            })
        })
    }

    onCompanyAdd(tag: any): Observable<TagModel> {
        this.defaultLevel.push(tag.value);
        return Observable
            .of(tag)

    }

    onCompanyRemove(tag: any): Observable<TagModel> {
        this.defaultLevel.splice(this.defaultLevel.indexOf(tag.value));
        return Observable
            .of(tag)

    }

    /**
     * END Level at Company related Code
     */
    /**
   *Initialize List
   * */
    initializeList() {
        // Industries
        this.defaultIndustry.forEach((value) => {
            this.myList.push(value)
        });
        // Cities
        this.defaultCity.forEach((value) => {
            this.myList.push(value)
        });
        // Bee Status
        this.defaultStatus.forEach((value) => {
            this.myList.push(value)
        });

        // CompanySize
        this.defaultCompanySize.forEach((value) => {
            this.myList.push(value)
        });

        // Est Bracket
        this.defaultEst.forEach((value) => {
            this.myList.push(value)
        });

        // Age Bracket
        this.defaultAge.forEach((value) => {
            this.myList.push(value)
        });

        // Job title
        this.defaultJob.forEach(value => {
            this.myList.push(value)
        })

        // Level At Company
        this.defaultLevel.forEach(value => {
            this.myList.push(value)
        })
        this.idealClientInput.value = this.myList;
    }

}
