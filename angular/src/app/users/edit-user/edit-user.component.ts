﻿import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, UserDto, RoleDto, DataTableListDto, DataTableServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';

import * as _ from 'lodash';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTableTypes } from '@shared/models/data-table';

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html'
})
export class EditUserComponent extends AppComponentBase implements OnInit {

    @ViewChild('editUserModal',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    user: UserDto = null;
    roles: RoleDto[] = null;
    userId: number;
    experiences: DataTableListDto[] = [];
    jobTitles: DataTableListDto[] = [];
    departments: DataTableListDto[] = [];
    editUserForm: FormGroup;

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _dataTableService: DataTableServiceProxy,
        private fb: FormBuilder
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.initializeForm();
    }
    initializeForm() {
        const emailReg = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
        this.editUserForm = this.fb.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            userName: ['', Validators.required],
            jobTitle: ['undefined', Validators.required],
            department: ['undefined', Validators.required],
            yearsOfExperience: ['undefined', Validators.required],
            emailAddress: ['', [Validators.required, Validators.pattern(emailReg)]],
            confirmEmailAddress: ['', [Validators.required, Validators.pattern(emailReg)]],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            isActive: [true],
            targetedMonthlyTurnoverSD: ['', Validators.required],
            averageDealSizeSD: [''],
            targetedMonthlyTurnoverMD: ['', Validators.required],
            averageDealSizeMD: [''],
            targetedMonthlyTurnoverBD: ['', Validators.required],
            averageDealSizeBD: ['']
        }, { validator: this.emailMatchValidator });
    }

    emailMatchValidator(g: FormGroup) {
        return g.get('emailAddress').value === g.get('confirmEmailAddress').value ? null : { 'mismatch': true };
    }

    userInRole(role: RoleDto, user: UserDto): string {
        if (user.roleNames.indexOf(role.normalizedName) !== -1) {
            return 'checked';
        } else {
            return '';
        }
    }

    show(id: number): void {
        this.userId = id;
        this._userService.getRoles()
            .subscribe((result) => {
                this.roles = result.items;
            });

        this._userService.get(id)
            .subscribe(
                (result) => {
                    this.user = result;
                    this.editUserForm.patchValue(this.user);
                    this.active = true;
                    this.modal.show();
                }
            );
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
        this.getDepartments();
        this.getExperiences();
        this.getJobTitles();
    }

    save(): void {
        const roles = [];
        $(this.modalContent.nativeElement).find('[name=role]').each(function (ind: number, elem: Element) {
            if ($(elem).is(':checked')) {
                roles.push(elem.getAttribute('value').valueOf());
            }
        });
        this.user = Object.assign({}, this.editUserForm.value);
        this.user.roleNames = roles;
        this.saving = true;
        this._userService.update(this.user)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    getDepartments() {
        this._dataTableService.getAll(DataTableTypes.DEPARTMENT).subscribe((result) => {
            this.departments = result.items;
        })
    }
    getExperiences() {
        this._dataTableService.getAll(DataTableTypes.EXPERIENCE).subscribe((result) => {
            this.experiences = result.items;
        })
    }
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        })
    }
}
