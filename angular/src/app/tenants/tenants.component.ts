import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TenantServiceProxy, TenantListDto, PagedResultDtoOfTenantListDto } from '@shared/service-proxies/service-proxies';

import { PagedListingComponentBase, PagedRequestDto } from "shared/paged-listing-component-base";
import { EditTenantComponent } from "app/tenants/edit-tenant/edit-tenant.component";
import { CreateTenantComponent } from "app/tenants/create-tenant/create-tenant.component";

@Component({
    templateUrl: './tenants.component.html',
    animations: [appModuleAnimation()]
})
export class TenantsComponent extends PagedListingComponentBase<TenantListDto> {

    @ViewChild('createTenantModal',{static:false}) createTenantModal: CreateTenantComponent;
    @ViewChild('editTenantModal',{static:false}) editTenantModal: EditTenantComponent;

    tenants: TenantListDto[] = [];

    constructor(
        injector: Injector,
        private _tenantService: TenantServiceProxy
    ) {
        super(injector);
    }
    //Update Filters Here(Lundi/Kipson)
    list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._tenantService.getTenants(null,null,null,null,null,null,null,null,null,null)
            .finally(()=>{
                finishedCallback();
            })
            .subscribe((result:PagedResultDtoOfTenantListDto)=>{
				this.tenants = result.items;
				this.showPaging(result, pageNumber);
            });
    }

    delete(tenant: TenantListDto): void {
		abp.message.confirm(
			"Delete tenant '"+ tenant.name +"'?",
			(result:boolean) => {
				if(result) {
					this._tenantService.deleteTenant(tenant.id)
						.finally(() => {
					        abp.notify.info("Deleted tenant: " + tenant.name );
							this.refresh();
						})
						.subscribe(() => { });
				}
			}
		);
    }

    // Show modals
    createTenant(): void {
        console.log(this.createTenantModal);
        this.createTenantModal.show();
    }

    editTenant(tenant: TenantListDto): void{

        this.editTenantModal.show(tenant.id);
    }
}
