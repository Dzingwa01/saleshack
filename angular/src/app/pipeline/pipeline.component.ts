import { Component, Injector, ViewChild, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CreateDealComponent } from '@app/deals/create-deal/create-deal.component';
import { Router, NavigationStart, Event, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
    templateUrl: './pipeline.component.html',
    animations: [appModuleAnimation()],
    //styleUrls: ['./pipeline.component.css']
})

export class PipelineComponent extends AppComponentBase {

    @ViewChild('createDealComponent',{static:false}) createDealComponent: CreateDealComponent;
    showSpinner = true;
    searchTerm:FormControl = new FormControl();
    constructor(private _router: Router, injector: Injector
    ) {
        super(injector);
    }
    createDeal() {
        this.createDealComponent.show();
    }
}
