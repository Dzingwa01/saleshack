import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injector } from '@angular/core';
import { ModalDirective, BsDatepickerConfig } from 'ngx-bootstrap';
import { AppComponentBase } from 'shared/app-component-base';
import {
    DealServiceProxy,
    CompanyListDto,
    ContactListDto,
    UserDto,
    CreateDealInput,
    Stage,
    UserServiceProxy,
    ContactServiceProxy
} from 'shared/service-proxies/service-proxies';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DealType } from '@shared/models/deal-type';

@Component({
    selector: 'app-create-deal',
    templateUrl: './create-deal.component.html',
    providers: [DealServiceProxy, ContactServiceProxy]
})
export class CreateDealComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createDealComponent',{static:false}) modal: ModalDirective;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @ViewChild('expectAmountRef',{static:false}) expectAmountRef: ElementRef;

    active = false;
    saving = false;
    companies: CompanyListDto[] = [];
    contacts: ContactListDto[] = [];
    user: UserDto = null;
    users: UserDto[] = [];
    dealTypes: DealType[] = [];
    stages: Stage[] = [];
    selectedCompany = 0;
    deal: CreateDealInput;
    createDealForm: FormGroup;
    datePickerConfig: Partial<BsDatepickerConfig>;
    constructor(injector: Injector,
        private _dealService: DealServiceProxy,
        private _userService: UserServiceProxy,
        private fb: FormBuilder,
        private _contactService: ContactServiceProxy) {
        super(injector);
        this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue', dateInputFormat: 'YYYY-MM-DD' });
    }
    ngOnInit() {
        this.initializeForm();
    }
    initializeForm() {
        this.createDealForm = this.fb.group({
            name: ['', Validators.required],
            ownerId: ['undefined', Validators.required],
            companyId: ['undefined', Validators.required],
            type: ['undefined', Validators.required],
            expectedAmount: ['', Validators.required],
            pipelineStageId: ['undefined', Validators.required],
            expectedCloseDate: ['', Validators.required],
            contactId: ['undefined']
        })
    }
    show() {
        this.active = true;
        this.deal = new CreateDealInput();
        this.modal.show();
    }
    onShown() {
        this.getCompanies();
        this.getUsers();
        this.getStages();
        this.getDealTypes();
        this.getUser();
    }
    close() {
        this.modal.hide();
        this.active = false;
    }
    getCompanies() {
        this._dealService.getCompanies().subscribe((result) => {
            this.companies = result.items;
        });
    }
    getUsers() {
        this._dealService.getUsers().subscribe((result) => {
            this.users = result.items;
        });
    }

    getStages() {
        this._dealService.getStages().subscribe((result) => {
            this.stages = result.items;
        })
    }
    getDealTypes() {
        this.dealTypes = [{ id: 0, name: 'SMALL' }, { id: 1, name: 'MEDIUM' }, { id: 2, name: 'BIG' }];
    }
    getUser() {
        const id = abp.session.userId;
        this._userService.get(id)
            .subscribe(
                (result) => {
                    this.user = result;
                });
    }
    onTypeChange(event) {
        const selectedVal = event.target.value;
        switch (selectedVal) {
            case '0':
                this.createDealForm.get('expectedAmount').setValue(this.user.averageDealSizeSD);
                this.expectAmountRef.nativeElement.focus();
                break;
            case '1':
                this.createDealForm.get('expectedAmount').setValue(this.user.averageDealSizeMD);
                this.expectAmountRef.nativeElement.focus();
                break;
            case '2':
                this.createDealForm.get('expectedAmount').setValue(this.user.averageDealSizeBD);
                this.expectAmountRef.nativeElement.focus();
                break;
            default:
                this.createDealForm.get('expectedAmount').setValue(0);
                break;
        }
    }
    onCompanyChange(event) {
        const companyId = event.target.value;
        this._contactService.getCompanyId(companyId).subscribe((result) => {
            this.contacts = result.items;
        });
    }
    save() {
        this.deal = Object.assign({}, this.createDealForm.value);
        this._dealService.create(this.deal).finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.success(this.l('Created Successfully'));
                this.close();
                this.modalSave.emit(this.deal);
            });
    }
    saveAndCreate() {
        // TODO: implement Method
    }
}
