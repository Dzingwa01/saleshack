import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { CreateTaskModalComponent } from 'app/general/tasks/create-task/create-task.component';
import { CreateNoteModalComponent } from 'app/general/notes/create-note/create-note.component';
import {
    DealDetailOutput,
    TasksServiceProxy,
    NoteServiceProxy,
    DealServiceProxy,
    ActivityServiceProxy,
    CompanyServiceProxy,
    NoteListDto,
    TasksListDto,
    ActivityListDto
} from 'shared/service-proxies/service-proxies';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { NewEmailComponent } from 'app/emails/new-email/new-email.component';
import { SendSmsComponent } from 'app/sms/send-sms/send-sms.component';
import { EditDealComponent } from '../edit-deal/edit-deal.component';

@Component({
    selector: 'app-view-deal',
    templateUrl: './view-deal.component.html',
    styleUrls: ['./view-deal.component.css'],
    providers: [TasksServiceProxy, NoteServiceProxy, DealServiceProxy, CompanyServiceProxy, ActivityServiceProxy]
})
export class ViewDealComponent implements OnInit {
    @ViewChild('editDealModal',{static:false}) editDealModal: EditDealComponent;
    @ViewChild('createTaskModal',{static:false}) createTaskModal: CreateTaskModalComponent;
    @ViewChild('createNoteModal',{static:false}) createNoteModal: CreateNoteModalComponent;
    @ViewChild('newEmailModal',{static:false}) newEmailModal: NewEmailComponent;
    @ViewChild('sendSmSModal',{static:false}) sendSmSModal: SendSmsComponent;

    deal: DealDetailOutput = new DealDetailOutput();
    dealId = '';
    stage: string;
    company: string;
    companyEmail: string;
    notes: NoteListDto[] = [];
    tasks: TasksListDto[] = [];
    activities: ActivityListDto[] = [];
    constructor(private _dealService: DealServiceProxy,
        private _companyService: CompanyServiceProxy, private _noteService: NoteServiceProxy,
        private _taskService: TasksServiceProxy, private activatedRoute: ActivatedRoute,
        private cdRef: ChangeDetectorRef) { }

    ngOnInit() {

        this.activatedRoute.params.subscribe((res) => {
            this.dealId = res.id
        });
        this.getDeal(this.dealId);
        this.getStage();
        this.getNotes('deal', this.dealId);
        this.getTasks('deal', this.dealId);
        this.getActivity('deal', this.dealId);
    }
    getDeal(id: string) {
        this._dealService.getById(id).subscribe((result) => {
            this.deal = result;
            console.log(this.deal);
        });

        this._companyService.getById(this.deal.companyId).subscribe((result) => {
            this.company = result.items[0].name;
            this.companyEmail = result.items[0].email;
        })
    }
    editDeal(deal: DealDetailOutput) {
        this.editDealModal.show(deal.id);
    }
    getNotes(targetType: string, targetId: string) {
        this._noteService.getNotes(targetType, targetId).subscribe((result) => {
            this.notes = result.items;
        });
    }
    getTasks(targetType: string, targetId: string) {
        this._taskService.getTasks(targetType, targetId).subscribe((result) => {
            this.tasks = result.items;
            this.cdRef.detectChanges();
        });
    }
    getActivity(targetType: string, targetId: string) {
        this._dealService.getActivities(targetType, targetId).subscribe((result) => {
            this.activities = result.items;
        });
    }
    getStage() {
        this._dealService.getStageById(this.deal.pipelineStageId).subscribe((res) => {
            this.stage = res.name;
        })
    }
    createTask(target: DealDetailOutput, targetType: string): void {
        this.createTaskModal.show(target.id, targetType);
    }

    createNote(target: DealDetailOutput, targetType: string): void {
        this.createNoteModal.show(target.id, targetType);
    }
    sendEmail(): void {
        this.newEmailModal.show();
    }
    sendSms(): void {
        this.sendSmSModal.show();
    }
}
