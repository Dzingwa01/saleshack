import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injector } from '@angular/core';
import { ModalDirective, BsDatepickerConfig } from 'ngx-bootstrap';
import {
  CompanyListDto,
  ContactListDto,
  UserDto,
  Stage,
  DealServiceProxy,
  DealDetailOutput,
  UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { DealType } from '@shared/models/deal-type';

@Component({
  selector: 'app-edit-deal',
  templateUrl: './edit-deal.component.html',
  styleUrls: ['./edit-deal.component.css'],
  providers: [DealServiceProxy]
})
export class EditDealComponent extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('editDealModal',{static:false}) modal: ModalDirective;
  @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
  @ViewChild('expectAmountRef',{static:false}) expectAmountRef: ElementRef;

  active = false;
  saving = false;
  companies: CompanyListDto[] = [];
  contacts: ContactListDto[] = [];
  users: UserDto[] = [];
  stages: Stage[] = [];
  //dealTypes: DealType[] = [];
  selectedCompany = 0;
  dealId = '';
  editDealForm: FormGroup;
  deal: DealDetailOutput = null;
  user: UserDto = null;
  datePickerConfig: Partial<BsDatepickerConfig>;
  constructor(injector: Injector,
    private _dealService: DealServiceProxy,
    private _userService: UserServiceProxy,
    private fb: FormBuilder) {
    super(injector);
    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' }, { dateInputFormat: 'YYYY-MM-DD' });
  }
  ngOnInit() {
    this.initializeForm();
    this.getCompanies();
    this.getUsers();
    this.getStages();
    this.getUser();
    //this.getDealTypes();
  }
  initializeForm() {
    this.editDealForm = this.fb.group({
      name: ['', Validators.required],
      ownerId: ['undefined', Validators.required],
      companyId: ['undefined', Validators.required],
      expectedAmount: ['', Validators.required],
      type: ['', Validators.required],
      pipelineStageId: ['undefined', Validators.required],
      expectedCloseDate: ['', Validators.required]
    })
  }
  show(id: string): void {
    this._dealService.getDetail(id)
      .finally(() => {
        this.active = true;
        this.modal.show();
      })
      .subscribe((result: DealDetailOutput) => {
        this.deal = result;
        this.dealId = this.deal.id;
        this.editDealForm.patchValue(result)
      });
  }
  //getDealTypes() {
  //  this.dealTypes = [{ id: 0, name: 'SMALL' }, { id: 1, name: 'MEDIUM' }, { id: 2, name: 'BIG' }];
  //}
  getUser() {
    const id = abp.session.userId;
    this._userService.get(id)
      .subscribe(
        (result) => {
          this.user = result;
        });
  }
  onTypeChange(event) {
    const selectedVal = event.target.value;
    switch (selectedVal) {
      case '0':
        this.editDealForm.get('expectedAmount').setValue(this.user.averageDealSizeSD);
        this.expectAmountRef.nativeElement.focus();
        break;
      case '1':
        this.editDealForm.get('expectedAmount').setValue(this.user.averageDealSizeMD);
        this.expectAmountRef.nativeElement.focus();
        break;
      case '2':
        this.editDealForm.get('expectedAmount').setValue(this.user.averageDealSizeBD);
        this.expectAmountRef.nativeElement.focus();
        break;
      default:
        this.editDealForm.get('expectedAmount').setValue(0);
        break;
    }
  }
  close() {
    this.modal.hide();
    this.active = false;
  }
  getCompanies() {
    this._dealService.getCompanies().subscribe((result) => {
      this.companies = result.items;
    });
  }
  getUsers() {
    this._dealService.getUsers().subscribe((result) => {
      this.users = result.items;
    });
  }

  getStages() {
    this._dealService.getStages().subscribe((result) => {
      this.stages = result.items;
    })
  }
  save() {
    this.saving = true;
    this.deal = Object.assign({}, this.editDealForm.value);
    this.deal.id = this.dealId;
    this._dealService.edit(this.deal).finally(() => this.saving = false)
      .subscribe(() => {
        this.notify.success(this.l('Updated Successfully'));
        this.close();
        this.modalSave.emit(this.deal);
      });

  }

}
