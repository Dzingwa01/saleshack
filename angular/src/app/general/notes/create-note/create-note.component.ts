import { Component, ViewChild, Injector, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    NoteServiceProxy, CreateNoteInput,
    CreateActivityInput, ActivityServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

declare var $: any;
@Component({
    selector: 'app-create-note',
    templateUrl: './create-note.component.html'
})
export class CreateNoteModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('createNoteModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    datePickerConfig: Partial<BsDatepickerConfig>;
    note: CreateNoteInput;
    activity: CreateActivityInput = new CreateActivityInput();
    active = false;
    saving = false;
    constructor(
        injector: Injector,

        private _noteService: NoteServiceProxy,
        private _activityService: ActivityServiceProxy
    ) {
        super(injector);
        this.datePickerConfig = Object.assign({}, { containerClass: 'theme-green' });
    }
    ngOnInit(): void {
        this.note = new CreateNoteInput();
        $('#summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            placeholder: 'Your Note Here...',
            height: 100
        });

    }
    show(targetId: string, targetType: string): void {
        this.active = true;
        this.note = new CreateNoteInput();
        this.note.targetType = targetType;
        this.note.targetId = targetId;
        this.modal.show();
    }
    save(): void {
        this.saving = true;
        const description = $('#summernote').summernote('code');
        this.note.description = description;
        this._noteService.createNote(this.note)
            .finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.success(this.l('Note Created Successfully'));
                this.createActivity();
                this.close();
                this.modalSave.emit(this.note);
            });
    }
    createActivity() {
        this.activity.targetId = this.note.targetId;
        this.activity.description = 'New Note';
        this.activity.targetType = this.note.targetType;
        this.activity.routeUrl = '';
        this._activityService.create(this.activity).subscribe(() => { })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}
