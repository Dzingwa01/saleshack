import { Component, ViewChild, Injector, ElementRef, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { TasksServiceProxy, CreateTasksInput, CreateActivityInput, ActivityServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'app-create-task',
    templateUrl: './create-task.component.html'
})
export class CreateTaskModalComponent extends AppComponentBase {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('createTaskModal',{static:false}) modal: ModalDirective;
    @ViewChild('nameInput',{static:false}) nameInput: ElementRef;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;

    datePickerConfig: Partial<BsDatepickerConfig>;

    task: CreateTasksInput;
    active = false;
    saving = false;
    activity: CreateActivityInput = new CreateActivityInput();

    constructor(
        injector: Injector,
        private _taskService: TasksServiceProxy,
        private _activityService: ActivityServiceProxy
    ) {
        super(injector);
        this.datePickerConfig = Object.assign({}, { containerClass: 'theme-green' });
    }

    show(targetId: string, targetType: string): void {
        this.active = true;
        this.task = new CreateTasksInput();
        this.task.targetType = targetType;
        this.task.targetId = targetId;
        this.modal.show();
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
    }

    save(): void {
        this.saving = true;
        this._taskService.create(this.task)
            .finally(() => this.saving = false)
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.createActivity();
                this.close();
                this.modalSave.emit(this.task);
            });
    }

    createActivity() {
        this.activity.targetId = this.task.targetId;
        this.activity.description = 'New Task';
        this.activity.targetType = this.task.targetType;
        this.activity.routeUrl = '';
        this._activityService.create(this.activity).subscribe(() => { })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}
