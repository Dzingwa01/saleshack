import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { CompaniesComponent } from './company/company.component';
import { ProspectsComponent } from './prospects/prospects.component';
import { ContactsComponent } from './contacts/contacts.component';
import { QualificationComponent } from './qualification/qualification.component';
import { MessengerComponent } from './messenger/messenger.component';
import { PipelineComponent } from './pipeline/pipeline.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { ContactSearchComponent } from './contacts/contact-search/contact.search.component';
import { DomainComponent } from './domain/domain.component';
import { CompanySearchComponent } from './company/company-search/company.search.component';
import { ProfileComponent } from 'app/profile/profile.component';
import { CompanyNameSearchComponent } from 'app/company/company-name-search/company-name-search.component';
import { ViewDealComponent } from 'app/deals/view-deal/view-deal.component';
import { CustomerProfileComponent } from 'app/profile/customer-profile/customer-profile.component';
import { RevenueProjectionsComponent } from 'app/profile/revenue-projections/revenue-projections.component';
import { IdealClientComponent } from 'app/prospects/ideal-client/ideal-client.component';
import { TotalAvailableMarketComponent } from './profile/customer-profile/total-available-market/total-available-market.component';
import { StagesResolver } from './_resolvers/stages.resolver';
import { SettingsComponent } from './settings/settings.component';
import { HelpComponent } from './help/help.component';
import { ReportsComponent } from './reports/reports.component';
import { SubscriptionManagementComponent } from '@app/subscription-management/subscription-management.component';
import { InvoiceComponent } from '@app/subscription-management/invoice/invoice.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                runGuardsAndResolvers: 'always',
                children: [
                    { path: 'home', component: HomeComponent, canActivate: [AppRouteGuard], resolve: { stages: StagesResolver } },
                    { path: 'companies', component: CompaniesComponent, canActivate: [AppRouteGuard] },
                    { path: 'prospects', component: ProspectsComponent, canActivate: [AppRouteGuard] },
                    { path: 'feeds/ideal-client', component: IdealClientComponent, canActivate: [AppRouteGuard] },
                    { path: 'user/profile', component: ProfileComponent, canActivate: [AppRouteGuard] },
                    { path: 'user/customer-profile', component: CustomerProfileComponent, canActivate: [AppRouteGuard] },
                    {
                        path: 'user/customer-profile/total-available-market', component: TotalAvailableMarketComponent,
                        canActivate: [AppRouteGuard]
                    },
                    { path: 'revenue-projections', component: RevenueProjectionsComponent, canActivate: [AppRouteGuard] },
                    { path: 'qualification/filter', component: QualificationComponent, canActivate: [AppRouteGuard] },
                    { path: 'qualification', component: QualificationComponent, canActivate: [AppRouteGuard] },
                    { path: 'contacts', component: ContactsComponent, canActivate: [AppRouteGuard] },
                    { path: 'pipeline', component: PipelineComponent, canActivate: [AppRouteGuard] },
                    { path: 'pipeline/deals/:id', component: ViewDealComponent, canActivate: [AppRouteGuard] },
                    { path: 'messenger', component: MessengerComponent, canActivate: [AppRouteGuard] },
                    { path: 'appointments', component: AppointmentsComponent, canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'contact/contact-search', component: ContactSearchComponent, canActivate: [AppRouteGuard] },
                    { path: 'company/company-search', component: CompanySearchComponent, canActivate: [AppRouteGuard] },
                    { path: 'company/company-name-search', component: CompanyNameSearchComponent, canActivate: [AppRouteGuard] },
                    { path: 'domain-search', component: DomainComponent, canActivate: [AppRouteGuard] },
                    { path: 'settings', component: SettingsComponent, canActivate: [AppRouteGuard] },
                    { path: 'help', component: HelpComponent, canActivate: [AppRouteGuard] },
                    { path: 'reports', component: ReportsComponent, canActivate: [AppRouteGuard] },
                    { path: 'subscription-management', component: SubscriptionManagementComponent, canActivate: [AppRouteGuard] },
                    { path: 'invoice/:paymentId', component: InvoiceComponent, canActivate: [AppRouteGuard] },

                ]
            }

        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
