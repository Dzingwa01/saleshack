import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    ProspectListsDto,
    ProspectListServiceProxy,
    ContactProspectServiceProxy,
    DataTableListDto,
    DataTableServiceProxy
} from 'shared/service-proxies/service-proxies';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { DataTableTypes } from '@shared/models/data-table';

@Component({
    templateUrl: './prospects.component.html',
    animations: [appModuleAnimation()],
    providers: [ProspectListServiceProxy, ContactProspectServiceProxy]
})

export class ProspectsComponent extends AppComponentBase implements OnInit {
    datePickerConfig: Partial<BsDatepickerConfig>;
    fromDate = null;
    toDate = null;
    city = '';
    industry = '';
    jobTitle = '';
    prospects: ProspectListsDto[] = [];
    prospectSearch: ProspectListsDto[] = [];
    industries: DataTableListDto[];
    jobTitles: DataTableListDto[];
    cities: DataTableListDto[] = [];
    filter = '';
    shown = true;
    constructor(
        injector: Injector,
        private _prospectListService: ProspectListServiceProxy,
        private _dataTableService: DataTableServiceProxy
    ) {
        super(injector);
        this.datePickerConfig = Object.assign({}, { containerClass: 'theme-default', dateInputFormat: 'YYYY-MM-DD'});
    }
    ngOnInit(): void {
        this.getAll();
        this.getCities();
        this.getIndustries();
        this.getJobTitles();
    }

    getAll(): void {
        this._prospectListService.getAllList(this.filter).subscribe((result) => {
            this.prospects = result.items;
        });
    }
    compile() {
        // this.shown = true;
        this.prospects = this.prospectSearch;
    }

    search() {

        this._prospectListService.getAll(this.jobTitle, this.industry, this.city, this.fromDate, this.toDate)
            .subscribe((result) => {
                this.prospectSearch = result.items;
            })
        this.compile()
    }
    qualify(i: number) {
        console.log(this.prospects[i].contactProspect.companyId)
        this._prospectListService.qualifyContact(this.prospects[i]).subscribe(() => { });
        this.getAll();
        console.log(this.prospects[i].contactProspect)
    }

    delete(prospectListsItem: ProspectListsDto): void {
        abp.message.confirm(
            'Delete Contact \'' + prospectListsItem.name + '\'?',
            (result: boolean) => {
                if (result) {
                    this._prospectListService.delete(prospectListsItem.id)
                        .finally(() => {
                            abp.notify.info('Deleted Prospect : ' + prospectListsItem.name);
                            this.getAll();
                        })
                        .subscribe(() => { });

                }
            }
        );
    }
    getIndustries() {
        this._dataTableService.getAll(DataTableTypes.INDUSTRY).subscribe((result) => {
            this.industries = result.items;
        })
    }
    getCities() {
        this._dataTableService.getAll(DataTableTypes.CITY).subscribe((result) => {
            this.cities = result.items;
        })
    }
    getJobTitles() {
        this._dataTableService.getAll(DataTableTypes.JOB_TITLE).subscribe((result) => {
            this.jobTitles = result.items;
        })
    }
}
