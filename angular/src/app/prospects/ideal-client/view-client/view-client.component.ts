import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    CompanyProspectDetailOutput,
    CompanyProspectServiceProxy,
    ContactProspectServiceProxy,
    CompanyProspect
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { CompanyInfoModalComponent } from '@app/company/company-info/company-info.component';

@Component({
    selector: 'app-view-ideal-client',
    templateUrl: './view-client.component.html',
    styleUrls: ['./view-client.component.css'],
    providers: [CompanyProspectServiceProxy, ContactProspectServiceProxy]
})
export class ViewClientComponent extends AppComponentBase implements OnInit {
    @ViewChild('viewIdealClientModal',{static:false}) modal: ModalDirective;
    @ViewChild('companyInfoModal',{static:false}) companyContacts: CompanyInfoModalComponent;
    @ViewChild('modalContent',{static:false}) modalContent: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    totalContacts = 0;
    city: string[] = [];
    beeStatus: string[] = [];
    companySize: string[] = [];
    colorBackground: string;
    company: CompanyProspectDetailOutput = null;
    constructor(injector: Injector,
        private _idealClientService: CompanyProspectServiceProxy,
        private _contactService: ContactProspectServiceProxy) {
        super(injector)
    }

    ngOnInit() {

    }

    show(id: string, backgroundClass: string): void {
        this._idealClientService.getDetail(id)
            .finally(() => {
                this.active = true;
                this.colorBackground = backgroundClass;
                this.modal.show();
            })
            .subscribe((result: CompanyProspectDetailOutput) => {
                this.company = result;
                this.city.push(this.company.city);
                this.companySize.push(this.company.size);
                this.beeStatus.push(this.company.beeStatus);
                this.getContacts();
            });
    }

    close(): void {
        this.active = false;
        this.beeStatus = [];
        this.companySize = [];
        this.city = [];
        this.modal.hide();
    }
    getInitial(name: string) {
        return name.substring(0, 1);
    }
    getContacts() {
        this._contactService.getCompanyId(this.company.id).subscribe((result) => {
            this.totalContacts = result.items.length;
        })
    }
    viewContacts(company: CompanyProspect) {
        this.close();
        this.companyContacts.show(company);
    }
    getContactEnding() {
        return this.totalContacts === 1 ? 'Contact' : 'Contacts';
    }
}
