import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from 'shared/app-component-base';
import { appModuleAnimation } from 'shared/animations/routerTransition';
import {
    IdealClientServiceProxy,
    CompanyProspectListDto,
    CompanyProspectDetailOutput,
    CompanyProspect
} from '@shared/service-proxies/service-proxies';
import { DataTableTypes } from '@shared/models/data-table';
import { ViewClientComponent } from './view-client/view-client.component';
import { CompanyInfoModalComponent } from '@app/company/company-info/company-info.component';

@Component({
    selector: 'app-ideal-client',
    templateUrl: './ideal-client.component.html',
    styleUrls: ['./ideal-client.component.css'],
    animations: [appModuleAnimation()],
    providers: [IdealClientServiceProxy]
})
export class IdealClientComponent extends AppComponentBase implements OnInit {
    @ViewChild('viewIdealClientModal',{static:false}) viewIdealClientModal: ViewClientComponent;
    @ViewChild('companyInfoModal',{static:false}) companyContacts: CompanyInfoModalComponent;
    industries: string[] = [];
    cities: string[] = [];
    beeStatus: string[] = [];
    companySize: string[] = [];
    estBracket: string[] = [];
    totalItems: number;
    page = 1;
    noOfItems = 8;
    idealClients: CompanyProspectListDto[] = [];
    constructor(injector: Injector, private _idealClientService: IdealClientServiceProxy) {
        super(injector)
    }
    ngOnInit() {
        this.getIndustries();
        this.getCities();
        this.getBeeStatus();
        this.getCompanySize();
        this.getEstBracket();
        this.getAll();
    }
    getAll() {
        if (this.industries.length > 0 &&
            this.cities.length > 0 &&
            this.beeStatus.length > 0 &&
            this.companySize.length > 0 &&
            this.estBracket.length > 0) {
            this._idealClientService.getAllCompanies(this.industries, this.cities,
                this.beeStatus, this.companySize, this.estBracket).subscribe((result) => {
                    this.idealClients = result.items;
                    this.totalItems = result.items.length;
                })
        } else {
            this._idealClientService.getDashBoardClients(abp.session.userId).subscribe((results) => {
                this.idealClients = results.items;
            })
        }
    }
    getIndustries() {
        this._idealClientService.getDefaultValues(DataTableTypes.INDUSTRY).subscribe((result) => {
            result.items.forEach((value) => {
                this.industries.push(value.value);
            })
        })
    }

    getCities() {
        this._idealClientService.getDefaultValues(DataTableTypes.CITY).subscribe((result) => {
            result.items.forEach((value) => {
                this.cities.push(value.value);
            })
        })
    }

    getBeeStatus() {
        this._idealClientService.getDefaultValues(DataTableTypes.BEE_STATUS).subscribe((result) => {
            result.items.forEach((value) => {
                this.beeStatus.push(value.value);
            })
        })
    }
    getCompanySize() {
        this._idealClientService.getDefaultValues(DataTableTypes.COMPANY_SIZE).subscribe((result) => {
            result.items.forEach((value) => {
                this.companySize.push(value.value);
            })
        })
    }
    getEstBracket() {
        this._idealClientService.getDefaultValues(DataTableTypes.EST_BRACKET).subscribe((result) => {
            result.items.forEach((value) => {
                this.estBracket.push(value.value);
            })
        })
    }
    getInitial(name: string) {
        return name.substring(0, 1);
    }

    viewFullProfile(client: CompanyProspectDetailOutput, colorClass: number) {
        const color = colorClass % 2 === 0 ? 'even-class' : 'odd-class';
        this.viewIdealClientModal.show(client.id, color);
    }
    viewContacts(company: CompanyProspect) {
        this.companyContacts.show(company);
    }
    filterContacts() {
        // TODO: implement Method
    }
}
