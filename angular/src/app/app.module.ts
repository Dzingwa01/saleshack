import { NgModule, PipeTransform, Pipe, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimeagoModule } from 'ngx-timeago';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TagInputModule } from 'ngx-chips';
import { MatInputModule } from '@angular/material';
import { MatAutocompleteModule, MatTabsModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { JoyrideModule } from 'ngx-joyride';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AbpModule } from '@abp/abp.module';
import { DomSanitizer } from '@angular/platform-browser'
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { OutreachEmailComponent } from 'app/qualification/outreach.email.component';
import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { CompaniesComponent } from '@app/company/company.component';
import { ProspectsComponent } from '@app/prospects/prospects.component';
import { QualificationComponent } from '@app/qualification/qualification.component';
import { ContactsComponent } from '@app/contacts/contacts.component';
import { MessengerComponent } from '@app/messenger/messenger.component';
import { PipelineComponent } from '@app/pipeline/pipeline.component';
import { AppointmentsComponent } from '@app/appointments/appointments.component';
import { UsersComponent } from '@app/users/users.component';
import { CreateUserComponent } from '@app/users/create-user/create-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleComponent } from '@app/roles/create-role/create-role.component';
import { EditRoleComponent } from './roles/edit-role/edit-role.component';
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantComponent } from './tenants/create-tenant/create-tenant.component';
import { EditTenantComponent } from './tenants/edit-tenant/edit-tenant.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { TopBarLanguageSwitchComponent } from '@app/layout/topbar-languageswitch.component';
import { SideBarUserAreaComponent } from '@app/layout/sidebar-user-area.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { SideBarFooterComponent } from '@app/layout/sidebar-footer.component';
import { RightSideBarComponent } from '@app/layout/right-sidebar.component';
import { CreateCompanyModalComponent } from '@app/company/create-company/create-company.component';
import { EditCompanyComponent } from '@app/company/edit-company/edit-company.component';
import { EditContactComponent } from '@app/contacts/edit-contact/edit-contact.component';
import { CreateContactModalComponent } from '@app/contacts/create-contact/create-contact.component';
import { CompanySearchComponent } from 'app/company/company-search/company.search.component';
import { DomainComponent } from 'app/domain/domain.component';
import { ContactSearchComponent } from 'app/contacts/contact-search/contact.search.component';
import { CreateContactInfoModalComponent } from 'app/domain/create-info/create-info.component';
import { ProfileComponent } from 'app/profile/profile.component';
import { QualifyComponent } from 'app/qualification/qualify-contact/qualify-contact.component';
import { CompanyInfoModalComponent } from 'app/company/company-info/company-info.component';
import { CompanyNameSearchComponent } from 'app/company/company-name-search/company-name-search.component';
import { OutreachSMSComponent } from 'app/qualification/outreach.sms.component';
import { CompanyContactsComponent } from './company/company-contacts/company-contacts.component';
import { ContactImportComponent } from './contacts/contact-import/contact-import.component';
import { BoardComponent } from './kanban/board/board.component';
import { CardComponent } from './kanban/card/card.component';
import { ListComponent } from './kanban/list/list.component';
import { CreateTaskModalComponent } from '@app/general/tasks/create-task/create-task.component';
import { CreateNoteModalComponent } from '@app/general/notes/create-note/create-note.component';
import { CreateDealComponent } from '@app/deals/create-deal/create-deal.component';
import { DealsComponent } from './deals/deals.component';
import { ViewDealComponent } from './deals/view-deal/view-deal.component';
import { VerticalTimelineModule } from 'angular-vertical-timeline';
import {
    ActivityServiceProxy,
    EmailServiceProxy,
    DataTableServiceProxy,
    IdealClientServiceProxy,
    DealServiceProxy,
    NoteServiceProxy,
    TasksServiceProxy,
    PaymentServiceProxy
} from 'shared/service-proxies/service-proxies';
import { EmailsComponent } from './emails/emails.component';
import { SmallDealsComponent } from 'app/profile/reproject-small-deals/reproject-small-deals.component';
import { MediumDealsComponent } from 'app/profile/reproject-medium-deals/reproject-medium-deals.component';
import { BigDealsComponent } from 'app/profile/reproject-big-deals/reproject-big-deals.component';
import { OrderModule } from 'ngx-order-pipe';
import { NewEmailComponent } from './emails/new-email/new-email.component';
import { ViewContactComponent } from './contacts/view-contact/view-contact.component';
import { CustomerProfileComponent } from './profile/customer-profile/customer-profile.component';
import { RevenueProjectionsComponent } from './profile/revenue-projections/revenue-projections.component';
import { IdealClientComponent } from './prospects/ideal-client/ideal-client.component';
import { SmsComponent } from './sms/sms.component';
import { SendSmsComponent } from './sms/send-sms/send-sms.component';
import { WidgetChartsModule } from 'app/charts/widget-charts.module';
import { TotalAvailableMarketComponent } from './profile/customer-profile/total-available-market/total-available-market.component';
import { CitiesResolver } from './_resolvers/cities.resolver';
import { IndustriesResolver } from './_resolvers/industries.resolver';
import { ViewClientComponent } from './prospects/ideal-client/view-client/view-client.component';
import { CompanyProfileComponent } from './company/company-profile/company-profile.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { EditDealComponent } from './deals/edit-deal/edit-deal.component';
import { StagesResolver } from './_resolvers/stages.resolver';
import { NotFoundComponent } from './not-found/not-found.component';
import { SettingsComponent } from './settings/settings.component';
import { HelpComponent } from './help/help.component';
import { ReportsComponent } from './reports/reports.component';
import { InvoiceComponent } from '@app/subscription-management/invoice/invoice.component';
import { SubscriptionManagementComponent } from '@app/subscription-management/subscription-management.component';
import { UtilsModule } from '@shared/utils/utils.module';
import { TableModule } from 'primeng/components/table/table';
import { PaginatorModule } from 'primeng/components/paginator/paginator';

@Pipe({ name: 'escapeHtml', pure: false })
export class EscapeHtmlPipe implements PipeTransform {
    constructor(private sanitized: DomSanitizer) { }
    transform(value: any, args: any[] = []) {
        // simple JS inj cleanup that should be done on server side primarily
        if (value.indexOf('<script>') !== -1) {
            return value.replace('<script>', '').replace('<\/script>', '');
        }
        return this.sanitized.bypassSecurityTrustHtml(value); // so ng2 does not remove CSS
    }
}
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};
TagInputModule.withDefaults({
    tagInput: {
        placeholder: 'Add a New Field',
        maxItems: 3
    }
});
@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        TenantsComponent,
        CreateTenantComponent,
        EditTenantComponent,
        CompaniesComponent,
        ProspectsComponent,
        ContactsComponent,
        QualificationComponent,
        MessengerComponent,
        AppointmentsComponent,
        PipelineComponent,
        UsersComponent,
        CreateUserComponent,
        EditUserComponent,
        RolesComponent,
        CreateRoleComponent,
        EditRoleComponent,
        TopBarComponent,
        TopBarLanguageSwitchComponent,
        SideBarUserAreaComponent,
        SideBarNavComponent,
        SideBarFooterComponent,
        RightSideBarComponent,
        CreateCompanyModalComponent,
        EditCompanyComponent,
        EditContactComponent,
        CreateContactModalComponent,
        CompanySearchComponent,
        DomainComponent,
        ContactSearchComponent,
        CreateContactInfoModalComponent,
        ProfileComponent,
        QualifyComponent,
        CompanyInfoModalComponent,
        CompanyNameSearchComponent,
        SmallDealsComponent,
        MediumDealsComponent,
        BigDealsComponent,
        OutreachEmailComponent,
        EscapeHtmlPipe,
        OutreachSMSComponent,
        CompanyContactsComponent,
        ContactImportComponent,
        BoardComponent,
        CardComponent,
        ListComponent,
        CreateDealComponent,
        CreateTaskModalComponent,
        CreateNoteModalComponent,
        DealsComponent,
        ViewDealComponent,
        EmailsComponent,
        NewEmailComponent,
        ViewContactComponent,
        CustomerProfileComponent,
        RevenueProjectionsComponent,
        IdealClientComponent,
        SmsComponent,
        SendSmsComponent,
        TotalAvailableMarketComponent,
        ViewClientComponent,
        CompanyProfileComponent,
        LoaderComponent,
        EditDealComponent,
        NotFoundComponent,
        SettingsComponent,
        HelpComponent,
        ReportsComponent,
        InvoiceComponent,
        SubscriptionManagementComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        PerfectScrollbarModule,
        ModalModule.forRoot(),
        AbpModule,
        AppRoutingModule,
        ServiceProxyModule,
        SharedModule,
        MatProgressSpinnerModule,
        NgxPaginationModule,
        BsDatepickerModule.forRoot(),
        TimeagoModule.forRoot(),
        PopoverModule.forRoot(),
        JoyrideModule.forRoot(),
        TooltipModule.forRoot(),
        MatTabsModule,
        VerticalTimelineModule,
        MatInputModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        TagInputModule,
        WidgetChartsModule,
        OrderModule,
        UtilsModule,
        TableModule,
        PaginatorModule,
         
    ],
    providers: [{
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
        
    }, ActivityServiceProxy, EmailServiceProxy, CitiesResolver,
        IndustriesResolver, DataTableServiceProxy, LoaderService,
        StagesResolver, DealServiceProxy, NoteServiceProxy,
        TasksServiceProxy, IdealClientServiceProxy, DealServiceProxy]
})
export class AppModule { }
