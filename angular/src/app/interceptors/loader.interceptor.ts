import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { finalize, map, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/throw';
import { LoaderService } from '../services/loader.service';
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    constructor(public loaderService: LoaderService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loaderService.show();
        return next.handle(req).finally(() => {
            this.loaderService.hide()
        });

    }
    // intercept(
    //     req: HttpRequest<any>,
    //     next: HttpHandler
    // ): Observable<HttpEvent<any>> {
    //     return next.handle(req).pipe(map(event => {
    //         this.loaderService.show();
    //         return event;
    //     }),
    //         catchError(error => {
    //             return Observable.throw(error);
    //         }),
    //         finalize(() => {
    //             // this.loaderService.hide();
    //         })
    //     )
    // }
}
