import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
    templateUrl: './sidebar-nav.component.html',
    selector: 'app-sidebar-nav',
    encapsulation: ViewEncapsulation.None
})
export class SideBarNavComponent extends AppComponentBase {

    menuItems: MenuItem[] = [
        new MenuItem(this.l('Dashboard'), '', 'fa-home', '/app/home'),

        new MenuItem(this.l('Prospect'), 'Pages.Users', 'fa-address-card-o', 'menu', [
            new MenuItem('Feeds', '', '', '/app/feeds/ideal-client'),
            new MenuItem('Company Search', '', '', '/app/company/company-search'),
            new MenuItem('Contact Search', '', '', '/app/contact/contact-search'),
            new MenuItem('Name Search', '', '', '/app/company/company-name-search'),
            new MenuItem('Domain Search', '', '', '/app/domain-search'),
            new MenuItem('Prospect List', '', '', '/app/prospects')

        ]),
        new MenuItem(this.l('Companies'), 'Pages.Users', 'fa-building-o', '/app/companies'),
        new MenuItem(this.l('Contacts'), 'Pages.Users', 'fa-users', '/app/contacts'),
        new MenuItem(this.l('Qualification'), 'Pages.Users', 'fa-list-ul', '/app/qualification'),
        new MenuItem(this.l('Pipeline'), 'Pages.Users', 'fa-database', '/app/pipeline'),
        new MenuItem(this.l('Messenger'), 'Pages.Users', 'fa-comments-o', '/app/messenger'),
        new MenuItem(this.l('Appointments'), 'Pages.Users', 'fa-calendar', '/app/appointments'),
        // new MenuItem('Subscription', 'Pages.Users', 'fa-money', '/app/subscription-management'),

        new MenuItem(this.l('Users'), 'Pages.Users', 'fa-address-book', '/app/users'),
        new MenuItem(this.l('Settings'), 'Pages.Users', 'fa-cog', '/app/settings'),
        new MenuItem(this.l('Help'), 'Pages.Users', 'fa-question-circle-o', '/app/help')
    ];

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    showMenuItem(menuItem): boolean {
        if (menuItem.permissionName) {
            return this.permission.isGranted(menuItem.permissionName);
        }

        return true;
    }
    // disabledBackground(menuItem: string): string {
    //     switch (menuItem) {
    //         case 'Reports':
    //             return 'disabled-bg';
    //         case 'Settings':
    //             return 'disabled-bg';
    //         case 'Help':
    //             return 'disabled-bg';
    //         case 'Messenger':
    //             return 'disabled-bg';
    //         case 'Appointments':
    //             return 'disabled-bg';
    //         default:
    //             break;
    //     }
    // }
}
