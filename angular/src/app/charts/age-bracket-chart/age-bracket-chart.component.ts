import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-age-bracket-chart',
    templateUrl: './age-bracket-chart.component.html',
    styleUrls: ['./age-bracket-chart.component.css']
})
export class AgeBracketChartComponent implements OnInit {
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barChartLabels: string[] = ['18-25', '26-35', '36-45', '46-55', '55+'];
    public barChartType = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [65, 59, 80, 81, 34], label: 'Males' },
        { data: [28, 48, 40, 19, 45], label: 'Females' }
    ];

    constructor() { }

    ngOnInit() {
    }
    // events
    chartClicked(e: any): void {
    }

    chartHovered(e: any): void {
    }
}
