import { Component, OnInit } from '@angular/core';
@Component({
    selector: 'app-bar-chart',
    templateUrl: './bar-chart.component.html',
    styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    public barChartType = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [65, 59, 80, 81, 56, 55, 40, 35, 58, 89, 48, 14], label: 'Target', backgroundColor: 'rgba(0, 0, 0, 0.2)' },
        { data: [28, 48, 40, 19, 86, 27, 90, 14, 58, 28, 24, 86], label: 'Closed', backgroundColor: 'rgba(255, 99, 132, 0.2)' }
    ];

    constructor() { }

    ngOnInit() {
    }

    // events
    chartClicked(e: any): void {
    }

    chartHovered(e: any): void {
    }
}
