import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';

@Component({
    selector: 'app-market-chart',
    templateUrl: './market-chart.component.html',
    styleUrls: ['./market-chart.component.css']
})
export class MarketChartComponent implements OnInit {
    public canvas: any;
    public ctx;
    public doughnutChartLabels: string[] = ['Total Availbale Market', 'In-Store Sales', 'Mail-Order Sales'];
    public doughnutChartData: any = [
        [350, 456, 0],
        [50, 0, 0],
        [250, 0, 0],
    ];
    public doughnutChartType: ChartType = 'doughnut';
    constructor() { }

    ngOnInit() {
        this.tamCircle();
        this.samCircle();
        this.somCircle();
    }

    tamCircle() {
        this.canvas = document.getElementById('tam');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.beginPath();
        this.ctx.arc(150, 160, 140, 0, 2 * Math.PI);
        this.ctx.fillStyle = '#FF8BA4';
        this.ctx.fillText('TAM: 900', 100, 50);
        this.ctx.closePath();
        this.ctx.fill();
    }
    samCircle() {
        this.canvas = document.getElementById('sam');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.beginPath();
        this.ctx.arc(150, 120, 120, 0, 2 * Math.PI);
        this.ctx.fillStyle = '#86C7F3';
        this.ctx.closePath();
        this.ctx.fill();
    }
    somCircle() {
        this.canvas = document.getElementById('som');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.beginPath();
        this.ctx.fillStyle = '#FFE29A';
        this.ctx.arc(150, 80, 70, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.closePath();
        this.ctx.fillStyle = 'red';
        this.ctx.font = '9pt';
        this.ctx.fillText('amit', 75, 160)
    }

    public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }
}
