import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { CommonModule } from '@angular/common';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './doughnut-chart/doughnut-chart.component';
import { TargetAudienceChartComponent } from './target-audience-chart/target-audience-chart.component';
import { MarketChartComponent } from './market-chart/market-chart.component';
import { AgeBracketChartComponent } from './age-bracket-chart/age-bracket-chart.component';


@NgModule({
    imports: [
        CommonModule,
        ChartsModule
    ],
    exports: [
        BarChartComponent, DoughnutChartComponent, TargetAudienceChartComponent, MarketChartComponent, AgeBracketChartComponent
    ],
    declarations: [BarChartComponent, DoughnutChartComponent, TargetAudienceChartComponent, MarketChartComponent, AgeBracketChartComponent]
})
export class WidgetChartsModule { }
