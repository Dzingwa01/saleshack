import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-target-audience-chart',
  templateUrl: './target-audience-chart.component.html',
  styleUrls: ['./target-audience-chart.component.css']
})
export class TargetAudienceChartComponent implements OnInit {
    // Pie
    public pieChartOptions: ChartOptions = {
        responsive: true,
    };
    public pieChartLabels: any[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
    public pieChartData: any = [300, 500, 100];
    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];
  constructor() { }

  ngOnInit() {
  }

}
