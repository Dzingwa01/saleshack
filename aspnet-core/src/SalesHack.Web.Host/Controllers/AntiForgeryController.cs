using Microsoft.AspNetCore.Antiforgery;
using SalesHack.Controllers;

namespace SalesHack.Web.Host.Controllers
{
    public class AntiForgeryController : SalesHackControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
