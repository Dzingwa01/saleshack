﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using SalesHack.Configuration;

namespace SalesHack.Web.Host.Startup
{
    [DependsOn(
       typeof(SalesHackWebCoreModule))]
    public class SalesHackWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SalesHackWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SalesHackWebHostModule).GetAssembly());
        }
    }
}
