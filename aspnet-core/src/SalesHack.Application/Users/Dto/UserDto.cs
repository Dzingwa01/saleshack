using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using SalesHack.Authorization.Users;

namespace SalesHack.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        //[Required]
        //[StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        //[Required]
        //[StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }
        public string BeeStatus { get; set; }
        public string CompanySize { get; set; }
        //[Required]
        //[StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        //[Required]
        //[EmailAddress]
        //[StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string FullName { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] RoleNames { get; set; }
        public virtual string PhoneNumber { get; set; }
        [StringLength(328)]
        public string EmailConfirmationCode { get; set; }
        [StringLength(328)]
        public string PasswordResetToken { get; set; }
        //Small Deals
        public virtual double TargetedMonthlyTurnoverSD { get; set; }
        public virtual double AverageDealSizeSD { get; set; }
        public virtual int TurnAroundTimeSD { get; set; }

        //Medium Deals
        public virtual double TargetedMonthlyTurnoverMD { get; set; }
        public virtual double AverageDealSizeMD { get; set; }
        public virtual int TurnAroundTimeMD { get; set; }

        //Big Deals
        public virtual double TargetedMonthlyTurnoverBD { get; set; }
        public virtual double AverageDealSizeBD { get; set; }
        public virtual int TurnAroundTimeBD { get; set; }

        public virtual string TelNumber { get; set; }

        //EMAILS        
        public virtual string MailUsername { get; set; }
        public virtual string MailPassword { get; set; }

        public virtual string IncomingMailServer { get; set; }
        public virtual string IncomingPort { get; set; }
        public virtual bool IncomingMailUseSSL { get; set; }
        public virtual string IncomingMailAuthenticationType { get; set; }

        public virtual string OutgoingMailServer { get; set; }
        public virtual string OutgoingPort { get; set; }
        public virtual bool SMTPAuthentication { get; set; }
        public virtual string MailEncryption { get; set; }

        //SOCIAL MEDIA
        public virtual string FacebookLink { get; set; }
        public virtual string LinkedInLink { get; set; }
        public virtual string GooglePlusLink { get; set; }
        public virtual string InstagramLink { get; set; }
        public virtual string TweeterLink { get; set; }

        //Others
        public virtual string ProfilePicture { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string EmailSignature { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string Industry { get; set; }
        public virtual string Size { get; set; }
        public virtual string ZipCode { get; set; }

        // SMALL DEAL CALCULATION
        public int LeadsTargetSD { get; set; }
        public int EmailsTargetSD { get; set; }
        public int CallsTargetSD { get; set; }
        public int NeedsAnalysisTargetSD { get; set; }
        public int MeetingsTargetSD { get; set; }
        public int ProposalsTargetSD { get; set; }
        public int WinsTargetSD { get; set; }
        public int FollowupSD { get; set; }
        public double RevenueTargetSD { get; set; }
        public int ProjectedTurnAroundTimeSD { get; set; }


        // MEDIUM DEAL CALCULATION
        public int LeadsTargetMD { get; set; }
        public int EmailsTargetMD { get; set; }
        public int CallsTargetMD { get; set; }
        public int NeedsAnalysisTargetMD { get; set; }
        public int MeetingsTargetMD { get; set; }
        public int ProposalsTargetMD { get; set; }
        public int WinsTargetMD { get; set; }
        public int FollowupMD { get; set; }
        public double RevenueTargetMD { get; set; }
        public int ProjectedTurnAroundTimeMD { get; set; }

        // BIG DEAL CALCULATION
        public int LeadsTargetBD { get; set; }
        public int EmailsTargetBD { get; set; }
        public int CallsTargetBD { get; set; }
        public int NeedsAnalysisTargetBD { get; set; }
        public int MeetingsTargetBD { get; set; }
        public int ProposalsTargetBD { get; set; }
        public int WinsTargetBD { get; set; }
        public int FollowupBD { get; set; }
        public double RevenueTargetBD { get; set; }
        public int ProjectedTurnAroundTimeBD { get; set; }
        public virtual string Turnover { get; set; }
        public virtual string Department { get; set; }
        public virtual string YearsOfExperience { get; set; }
        public List<int> Services { get; set; }
        public List<int> TargetIndustries { get; set; }
    }
}
