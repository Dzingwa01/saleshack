﻿using System.Collections.Generic;
using SalesHack.Authorization.Permissions.Dto;

namespace SalesHack.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}