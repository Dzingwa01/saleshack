﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;
using SalesHack.Authorization.Users.ProfilePhoto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Users.Dto
{
    [AutoMapFrom(typeof(ProfilePhoto))]
    public class ProfilePhotoDto : FullAuditedEntityDto<long>
    {
        public string Url { get; set; }
        public IFormFile File { get; set; }
        public string Description { get; set; }
        public string PublicId { get; set; }
        public long UserId { get; set; }
    }
}
