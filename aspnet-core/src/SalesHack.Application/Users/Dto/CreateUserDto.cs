using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using SalesHack.Authorization.Users;

namespace SalesHack.Users.Dto
{
    [AutoMapTo(typeof(User))]
    public class CreateUserDto : IShouldNormalize
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string[] RoleNames { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        //Small Deals
        public virtual double TargetedMonthlyTurnoverSD { get; set; }
        public virtual double AverageDealSizeSD { get; set; }
        public virtual int TurnAroundTimeSD { get; set; }

        //Medium Deals
        public virtual double TargetedMonthlyTurnoverMD { get; set; }
        public virtual double AverageDealSizeMD { get; set; }
        public virtual int TurnAroundTimeMD { get; set; }

        //Big Deals
        public virtual double TargetedMonthlyTurnoverBD { get; set; }
        public virtual double AverageDealSizeBD { get; set; }
        public virtual int TurnAroundTimeBD { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string TelNumber { get; set; }
        public virtual string BeeStatus { get; set; }
        public virtual string Turnover { get; set; }
        public virtual string Department { get; set; }
        public virtual string YearsOfExperience { get; set; }
        public List<int> Services { get; set; }
        public List<int> TargetIndustries { get; set; }
        public void Normalize()
        {
            if (RoleNames == null)
            {
                RoleNames = new string[0];
            }
        }
    }
}
