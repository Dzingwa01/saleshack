using System.ComponentModel.DataAnnotations;

namespace SalesHack.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}