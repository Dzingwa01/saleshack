﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Users.Dto
{
    class TargetsDto
    {
        public int LeadsTarget
        {
            get
            {
                return (int)(TargetedMonthlyTurnover / AvarageDealSize);
            }
        }

        public int EmailsTarget { get { return (int)Math.Round((AvarageDealSize * 0.001) / ProjectedTurnAroundTime); } }
        public int CallsTarget { get { return (int)Math.Round(EmailsTarget * 0.85); } }
        public int NeedsAnalysisTarget { get { return (int)Math.Round(CallsTarget * 0.75); } }
        public int MeetingsTarget { get { return (int)Math.Round(NeedsAnalysisTarget * 0.6); } }
        public int ProposalsTarget { get { return (int)Math.Round(NeedsAnalysisTarget * 0.6); } }
        public int WinsTarget { get { return (int)(ProjectedTurnAroundTime); } }
        public int Followup { get { return (int)Math.Round(ProposalsTarget * 0.5); } }
        public double RevenueTarget { get { return (int)(ProjectedTurnAroundTime); } }
        public double AvarageDealSize { get; set; }
        public double TargetedMonthlyTurnover { get; set; }
        public int ProjectedTurnAroundTime { get { return (int)Math.Round(LeadsTarget / 4.0); } }
    }
}
