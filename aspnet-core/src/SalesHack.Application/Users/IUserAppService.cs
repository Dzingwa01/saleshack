using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Roles.Dto;
using SalesHack.Users.Dto;

namespace SalesHack.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
        //Task SendConfirmation(string email, string name);
        Task ChangeLanguage(ChangeUserLanguageDto input);

        Task AddProfilePhoto(ProfilePhotoDto profilePhotoDto);
    }
}
