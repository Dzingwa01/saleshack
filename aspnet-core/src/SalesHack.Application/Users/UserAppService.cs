using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using SalesHack.Authorization;
using SalesHack.Authorization.Roles;
using SalesHack.Authorization.Users;
using SalesHack.Roles.Dto;
using SalesHack.Users.Dto;
using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using SalesHack.MultiTenancy;
using Microsoft.Extensions.Options;
using SalesHack.Helpers;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using SalesHack.Authorization.Users.ProfilePhoto;

namespace SalesHack.Users
{
    [AbpAuthorize(AppPermissions.Pages_Users)]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly TenantManager _tenantManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserServiceOffering, Guid> _userServiceOfferingRepository;
        private readonly IRepository<UserTargetIndustry, Guid> _userTargetIndustryRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IHostingEnvironment _environment;
        private readonly IUserServiceOfferingManager _userServiceOfferingManager;
        private readonly IUserTargetIndustryManager _userTargetIndustryManager;
        private readonly IAbpSession _session;
        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private Cloudinary _cloudinary;
        private readonly IProfilePhotoManager _profilePhotoManager;
        private readonly IRepository<ProfilePhoto, long> _profilephotoRespository;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            IRepository<User, long> userRepository,
            IRepository<UserServiceOffering, Guid> userServiceOfferingRepository,
            IRepository<UserTargetIndustry, Guid> userTargetIndustryRepository,
            IAbpSession session,
            TenantManager tenantManager,
            IHostingEnvironment environment,
            IUserServiceOfferingManager userServiceOfferingManager,
            IOptions<CloudinarySettings> cloudinaryConfig,
            IProfilePhotoManager profilePhotoManager,
            IRepository<ProfilePhoto, long> profilephotoRespository,
        IUserTargetIndustryManager userTargetIndustryManager)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _environment = environment;
            _userServiceOfferingManager = userServiceOfferingManager;
            _userTargetIndustryManager = userTargetIndustryManager;
            _userRepository = userRepository;
            _userServiceOfferingRepository = userServiceOfferingRepository;
            _userTargetIndustryRepository = userTargetIndustryRepository;
            _session = session;
            _tenantManager = tenantManager;
            _cloudinaryConfig = cloudinaryConfig;
            Account acc = new Account(
                _cloudinaryConfig.Value.CloudName,
                _cloudinaryConfig.Value.ApiKey,
                _cloudinaryConfig.Value.ApiSecret
                );
            //_cloudinary = new Cloudinary(acc);
            _profilePhotoManager = profilePhotoManager;
            _profilephotoRespository = profilephotoRespository;
        }

        public async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();
            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.Password = _passwordHasher.HashPassword(user, input.Password);
            user.IsEmailConfirmed = true;

            CheckErrors(await _userManager.CreateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }
            if (user != null)
            {
                user.IsEmailConfirmed = true;

                // SMALL DEAL CALCULATION
                //user.JobTitle = input.JobTitle;
                user.TargetedMonthlyTurnoverSD = input.TargetedMonthlyTurnoverSD;
                user.AverageDealSizeSD = input.TargetedMonthlyTurnoverSD / 4;
                user.LeadsTargetSD = (int)(user.TargetedMonthlyTurnoverSD / user.AverageDealSizeSD);
                user.ProjectedTurnAroundTimeSD = (int)(user.LeadsTargetSD / 4.0);
                user.EmailsTargetSD = (int)Math.Round((user.AverageDealSizeSD * 0.001) / user.ProjectedTurnAroundTimeSD);
                user.LeadsTargetSD = (int)((user.EmailsTargetSD * 0.1) + user.EmailsTargetSD); // Leads / Prospects
                user.CallsTargetSD = (int)Math.Round(user.EmailsTargetSD * 0.85);
                user.NeedsAnalysisTargetSD = (int)Math.Round(user.CallsTargetSD * 0.75);
                user.MeetingsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.ProposalsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.WinsTargetSD = (int)(user.ProjectedTurnAroundTimeSD);
                user.FollowupSD = (int)Math.Round(user.ProposalsTargetSD * 0.5);
                user.ProjectedTurnAroundTimeSD = (int)(user.ProjectedTurnAroundTimeSD);

                // MEDIUM DEAL CALCULATION
                user.TargetedMonthlyTurnoverMD = input.TargetedMonthlyTurnoverMD;
                user.AverageDealSizeMD = input.TargetedMonthlyTurnoverMD / 4;

                user.LeadsTargetMD = (int)(user.TargetedMonthlyTurnoverMD / user.AverageDealSizeMD);
                user.ProjectedTurnAroundTimeMD = (int)(user.LeadsTargetMD / 4.0);
                user.EmailsTargetMD = (int)Math.Round((user.AverageDealSizeMD * 0.001) / user.ProjectedTurnAroundTimeMD);
                user.LeadsTargetMD = (int)((user.EmailsTargetMD * 0.1) + user.EmailsTargetMD); //Leads/Prospects
                user.CallsTargetMD = (int)Math.Round(user.EmailsTargetMD * 0.85);
                user.NeedsAnalysisTargetMD = (int)Math.Round(user.CallsTargetMD * 0.75);
                user.MeetingsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.ProposalsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.WinsTargetMD = (int)(user.ProjectedTurnAroundTimeMD);
                user.FollowupMD = (int)Math.Round(user.ProposalsTargetMD * 0.5);

                //// BIG DEAL CALCULATION
                user.TargetedMonthlyTurnoverBD = input.TargetedMonthlyTurnoverBD;
                user.AverageDealSizeBD = input.TargetedMonthlyTurnoverBD / 4;

                user.LeadsTargetBD = (int)(user.TargetedMonthlyTurnoverBD / user.AverageDealSizeBD);
                user.ProjectedTurnAroundTimeBD = (int)(user.LeadsTargetBD / 4.0);

                user.EmailsTargetBD = (int)Math.Round((user.AverageDealSizeBD * 0.001) / user.ProjectedTurnAroundTimeBD);
                user.LeadsTargetBD = (int)((user.EmailsTargetBD * 0.1) + user.EmailsTargetBD); //Leads/Prospects
                user.CallsTargetBD = (int)Math.Round(user.EmailsTargetBD * 0.85);
                user.NeedsAnalysisTargetBD = (int)Math.Round(user.CallsTargetBD * 0.75);
                user.MeetingsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.ProposalsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.WinsTargetBD = (int)(user.ProjectedTurnAroundTimeBD);
                user.FollowupBD = (int)Math.Round(user.ProposalsTargetBD * 0.5);

                user.IsActive = true;
                try
                {
                    await SendConfirmation(user.EmailAddress, user.Name, user.UserName, input.Password);
                    foreach (int ServiceId in input.Services)
                    {
                        if (await _userServiceOfferingRepository.GetAll().Where(x => (x.ServiceId.Equals(ServiceId)) && (x.UserId.Equals(user.Id))).FirstOrDefaultAsync() == null)
                        {
                            await _userServiceOfferingManager.CreateAsync(new UserServiceOffering
                            {
                                TenantId = AbpSession.GetTenantId(),
                                UserId = user.Id,
                                ServiceId = ServiceId
                            });
                        }
                    }
                    foreach (int industryId in input.TargetIndustries)
                    {
                        if (await _userTargetIndustryRepository.GetAll().Where(x => (x.IndustryId.Equals(industryId)) && (x.UserId.Equals(user.Id))).FirstOrDefaultAsync() == null)
                        {
                            await _userTargetIndustryManager.CreateAsync(new UserTargetIndustry
                            {
                                TenantId = AbpSession.GetTenantId(),
                                UserId = user.Id,
                                IndustryId = industryId
                            });
                        }
                    }
                    await _userManager.UpdateAsync(user);
                }
                catch (Exception ex)
                {
                    return null;
                }

                return null;

            }

            CurrentUnitOfWork.SaveChanges();


            return MapToEntityDto(user);
        }


        public async Task<UserDto> Update(UserDto input)
        {
            CheckUpdatePermission();

            var user = await _userManager.GetUserByIdAsync(input.Id);

            MapToEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            return  input;
        }

        public async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override UserDto MapToEntityDto(User user)
        {
            var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id)).Select(r => r.NormalizedName);
            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();
            return userDto;
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            return await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        private async Task SendConfirmation(string email, string name, string username, string password)
        {
            var userId = _session.UserId;

            var user = _userRepository.FirstOrDefault(x => x.Id == userId);
            var tenant = await _tenantManager.GetByIdAsync((int)_session.TenantId);
            //send an email here
            string body = string.Empty;

            //using streamreader for reading my html template   

            var path = Path.Combine(_environment.WebRootPath, "templates/email/success-email.html");

            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            string fromName = user.Name;
            string link = "https://app.saleshack.io/account/tenant";
            body = body.Replace("#Name", name);
            body = body.Replace("#Link", link);
            body = body.Replace("#FromName", fromName);
            body = body.Replace("#DomainName", tenant.TenancyName);
            body = body.Replace("#UserName", username);
            body = body.Replace("#Password", password);

            // body = body.Replace("#PlainLink", link);
            //SendTest();
            Emailer.Send(to: email, subject: "Sales Hack New Account Registration!", body: body, fromEmail: user.EmailAddress, isBodyHtml: true);
        }

        public async Task AddProfilePhoto(ProfilePhotoDto profilePhotoDto)
        {
            var file = profilePhotoDto.File;
            var uploadResult = new ImageUploadResult();
            if (file.Length > 0)
            {
                using (var stream = file.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation()
                        .Width(300).Height(500).Crop("fill").Gravity("face")
                    };
                    uploadResult = _cloudinary.Upload(uploadParams);
                }
            }
            profilePhotoDto.Url = uploadResult.Uri.ToString();
            profilePhotoDto.PublicId = uploadResult.PublicId;

            if (await _profilephotoRespository.FirstOrDefaultAsync(x => x.UserId == profilePhotoDto.UserId) == null)
            {
                var profilePhoto = ProfilePhoto.Create(AbpSession.GetTenantId(), profilePhotoDto.Url, null, profilePhotoDto.PublicId, AbpSession.GetUserId());
                await _profilePhotoManager.CreateAsync(profilePhoto);
            }
            else
            {
                var currentPhoto = await _profilephotoRespository.FirstOrDefaultAsync(x => x.UserId == profilePhotoDto.UserId);
                var deletionParams = new DeletionParams(currentPhoto.PublicId);
                var result = _cloudinary.Destroy(deletionParams);
                if (result.Result == "ok")
                {
                    await _profilephotoRespository.DeleteAsync(x => x.PublicId == currentPhoto.PublicId);
                }
                var profilePhoto = ProfilePhoto.Create(AbpSession.GetTenantId(), profilePhotoDto.Url, null, profilePhotoDto.PublicId, AbpSession.GetUserId());
                await _profilePhotoManager.CreateAsync(profilePhoto);
            }
        }
    }
}
