﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace SalesHack.Security.Recaptcha
{
    public interface IRecaptchaValidator: IApplicationService
    {
        Task ValidateAsync(string captchaResponse);
    }
}