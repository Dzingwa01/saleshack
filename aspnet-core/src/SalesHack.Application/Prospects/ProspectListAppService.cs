﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.Companies;
using SalesHack.Prospects.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    [AbpAuthorize]
    public class ProspectListAppService : SalesHackServiceBase
    {
        private readonly IProspectListManager _prospectListManager;
        private readonly IRepository<ProspectList, Guid> __prospectListRepository;
        private readonly IRepository<CompanyProspect, Guid> __companyProspectRepository;
        private readonly ICompanyManager companyManager;
        private readonly IRepository<Company, Guid> __companyRepository;

        private readonly IRepository<Contact, Guid> _contactRepository;

        public ProspectListAppService(IRepository<ProspectList, Guid> prospectListRepository, ICompanyManager companyManager, IProspectListManager prospectListManager, IRepository<Contact, Guid> _contactRepository, IRepository<CompanyProspect, Guid> companyProspectRepository, IRepository<Company, Guid> __companyRepository)
        {
            this.__prospectListRepository = prospectListRepository;
            this._prospectListManager = prospectListManager;
            this._contactRepository = _contactRepository;
            this.companyManager = companyManager;
            this.__companyProspectRepository = companyProspectRepository;
            this.__companyRepository = __companyRepository;
        }
        public async Task Create(Guid ContactId)
        {
            try
            {
                var prospect = ProspectList.Add(ContactId);
                await _prospectListManager.CreateAsync(prospect);

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ListResultDto<ProspectListsDto>> GetAll(GetAllProspectsListInput input)
        {
            var prospectList = await __prospectListRepository.GetAll().Where(x => (input.JobTitle == null || x.ContactProspect.JobTitle == input.JobTitle) &&
            (input.Industry == null || x.ContactProspect.Industry == input.Industry) &&
            (input.City == null || x.ContactProspect.City == input.City) &&
            (input.FromDate == null || x.CreationTime.Date >= input.FromDate.Value.Date) &&
             (input.toDate == null || x.CreationTime.Date <= input.toDate.Value.Date) && x.IsQualified == false)
            .Include(e => e.ContactProspect).OrderByDescending(e => e.CreationTime).ToListAsync();

            return new ListResultDto<ProspectListsDto>(prospectList.MapTo<List<ProspectListsDto>>());
        }
        public async Task<ListResultDto<ProspectListsDto>> GetAllList(GetAllProspectInput Input)
        {
            var prospectList = await __prospectListRepository.GetAll().Where(x => x.IsQualified == false).Include(e => e.ContactProspect).OrderByDescending(e => e.CreationTime).ToListAsync();

            return new ListResultDto<ProspectListsDto>(prospectList.MapTo<List<ProspectListsDto>>());
        }
        //public async Task<ListResultDto<CompanyProspect>> GetCompanyProspect(Guid? Input)
        //{
        //    var prospect = await __companyProspectRepository.GetAll().Where(x => x.Id == Input).Include(e => e.Contacts).OrderByDescending(e => e.CreationTime).ToListAsync();

        //    return new ListResultDto<CompanyProspect>(prospect.MapTo<List<CompanyProspect>>());
        //}
        //public async Task<List<CompanyProspect>> GetProspect(Guid? Input)
        //{
        //    List<CompanyProspect> prospect = await __companyProspectRepository.GetAll().Where(x => x.Id == Input).OrderByDescending(e => e.CreationTime).ToListAsync();

        //    return prospect;
        //}
        public async Task<Company> Create2(Guid Id)
        {
            //var input = await __companyProspectRepository.GetAll().Where(x => x.Id == Id).Include(e => e.Contacts).OrderByDescending(e => e.CreationTime).ToListAsync();
            var companyProspect = __companyProspectRepository.Get(Id);
            try
            {
                //var company = Company.Create(AbpSession.GetTenantId(), input[0].Name, input[0].Email, AbpSession.GetUserId(), input[0].Size, input[0].Phase, input[0].Turnover, input[0].YearInBusiness, input[0].WebUrl, input[0].Telephone, input[0].Industry, input[0].BusinessType, input[0].Address,
                //   input[0].City, input[0].Province, input[0].Country, input[0].BeeStatus, input[0].PostalCode, input[0].Source, input[0].Stage, input[0].SpecialEvents, input[0].Facebook, input[0].LinkedIn, input[0].GooglePlus, input[0].Instagram, input[0].Twitter);


                var company = Company.Create(AbpSession.GetTenantId(), companyProspect.Name, companyProspect.Email, AbpSession.GetUserId(), companyProspect.Size, companyProspect.Phase, companyProspect.Turnover, companyProspect.YearInBusiness, companyProspect.WebUrl, companyProspect.Telephone, companyProspect.Industry, companyProspect.BusinessType, companyProspect.Address,
                   companyProspect.City, companyProspect.Province, companyProspect.Country, companyProspect.BeeStatus, companyProspect.PostalCode, companyProspect.Source, companyProspect.Stage, companyProspect.SpecialEvents, companyProspect.Facebook, companyProspect.LinkedIn, companyProspect.GooglePlus, companyProspect.Instagram, companyProspect.Twitter);

                await companyManager.CreateAsync(company);

                return company;

            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }
        }



        public async Task QualifyContact(ProspectListsDto input)
        {
            //get company id from input
            //get prospect company

            var company = new Company();

            var compId = company.Id == Guid.Empty ? null : (Guid?)company.Id;


            if (input.ContactProspect.CompanyId != null)
                company = await Create2(input.ContactProspect.CompanyId.Value);

            var contact = new Contact
            {
                TenantId = AbpSession.GetTenantId(),
                Name = input.ContactProspect.Name,
                Surname = input.ContactProspect.Surname,
                Source = input.ContactProspect.Source,
                JobTitle = input.ContactProspect.JobTitle,
                Department = input.ContactProspect.Department,
                Telephone = input.ContactProspect.Telephone,
                Cellphone = input.ContactProspect.Cellphone,
                Email = input.ContactProspect.Email,
                Industry = input.ContactProspect.Industry,
                Address = input.ContactProspect.Address,
                City = input.ContactProspect.City,
                Province = input.ContactProspect.Province,
                SpecialEvents = input.ContactProspect.SpecialEvents,
                Facebook = input.ContactProspect.Facebook,
                LinkedIn = input.ContactProspect.LinkedIn,
                GooglePlus = input.ContactProspect.GooglePlus,
                Instagram = input.ContactProspect.Instagram,
                Twitter = input.ContactProspect.Twitter,
                Status = "Qualify",
                AssignedId = AbpSession.GetUserId(),
                CompanyId = compId
            };
            var contact2 = await _contactRepository.GetAll().Where(x => x.Email == contact.Email).FirstOrDefaultAsync();

            if (contact2 == null)
            {
                await _contactRepository.InsertAsync(contact);
            }
            var prospectList = await __prospectListRepository.GetAsync(input.Id);
            prospectList.IsQualified = true;

            await __prospectListRepository.UpdateAsync(prospectList);

        }

        public async Task Delete(EntityDto<Guid> input)
        {
            await __prospectListRepository.DeleteAsync(input.Id);
        }


    }
}
