﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    public class GetCompanyProspectsInput
    {
        public string county { get; set; }
        public string industry { get; set; }
        public string city { get; set; }
        public string companysize { get; set; }
        public string bee { get; set; }
    }
}
