﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    public class GetContactProspectsInput
    {
      
        public string industry { get; set; }
        public string jobTitle { get; set; }
        public string city { get; set; }
        public string companySize { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        //public string skills { get; set; } this must be a list

    }
}
