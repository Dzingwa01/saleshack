﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    public class GetAllProspectsListInput
    {
        public string JobTitle { get; set; }
        public string Industry { get; set; }
        public string City { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? toDate { get; set; }
    }
}
