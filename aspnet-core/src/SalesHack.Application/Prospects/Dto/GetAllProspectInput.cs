﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    public class GetAllProspectInput
    {
        public string Filter { get; set; }
    }
}
