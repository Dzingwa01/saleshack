﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    [AutoMapFrom(typeof(CompanyProspect))]
    public class CompanyProspectDetailOutput : EntityDto<Guid>
    {
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Size { get; set; }
        public virtual string Phase { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual decimal Turnover { get; set; }
        public virtual int YearInBusiness { get; set; }
        public virtual string WebUrl { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Industry { get; set; }
        public virtual string BusinessType { get; set; }
        public virtual string BeeStatus { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Source { get; set; }
        public virtual string Stage { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
    }
}
