﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects.Dto
{
    public class GetCompanyNameInput
    {
        public string Filter { get; set; }
    }
}
