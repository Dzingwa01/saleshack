﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects
{
    public class DomainSearchAppService : SalesHackServiceBase, IDomainSearchAppService
    {
        
        private readonly IDomainSearchManager _domainSearchManager;

        public DomainSearchAppService(IDomainSearchManager _domainSearchManager)
        {          
            this._domainSearchManager = _domainSearchManager;
        }
        public List<string> GetWebEmails(string domain)
        {
            return _domainSearchManager.GetWebEmails(domain);
        }
    }
}
