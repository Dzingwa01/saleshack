﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using SalesHack.Prospects.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    [AbpAuthorize]
    public class ProspectAppService : SalesHackServiceBase
    {
        private readonly IProspectManager _prospectManager;

        private readonly IRepository<Prospect, Guid> _prospectRepository;

        public ProspectAppService(IRepository<Prospect, Guid> prospectRepository, IProspectManager _prospectManager)
        {
            _prospectRepository = prospectRepository;
            this._prospectManager = _prospectManager;
        }
        public async Task Create(CreateProspectInput input)
        {
            try
            {
                var prospect = Prospect.Create(input.Name, input.Email, input.Size, input.Phase, input.JobTitle, input.Turnover, input.YearInBusiness, input.WebUrl, input.Telephone, input.Industry, input.BusinessType, input.Address,
                   input.City, input.Province, input.PostalCode, input.Source, input.Stage, input.SpecialEvents, input.Facebook, input.LinkedIn, input.GooglePlus, input.Instagram, input.Twitter);

                await _prospectManager.CreateAsync(prospect);
            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }

        }
        public async Task EditProspect(ProspectDetailOutput input)
        {
            var contact = await _prospectRepository.GetAsync(input.Id);

            NewMethod(input, contact);

            await _prospectRepository.UpdateAsync(contact);


        }
        public async Task<ListResultDto<ProspectListDto>> GetAll(GetAllProspectInput input)
        {

            var prospects = await _prospectRepository
            .GetAll()
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<ProspectListDto>(prospects.MapTo<List<ProspectListDto>>());
        }
        
        private static void NewMethod(ProspectDetailOutput input, Prospect prospect)
        {
            prospect.Name = input.Name;
            prospect.Email = input.Email;
            prospect.Size = input.Size;
            prospect.Phase = input.Phase;
            prospect.JobTitle = input.JobTitle;
            prospect.Turnover = input.Turnover;
            prospect.YearInBusiness = input.YearInBusiness;
            prospect.WebUrl = input.WebUrl;
            prospect.Telephone = input.Telephone;
            prospect.Industry = input.Industry;
            prospect.BusinessType = input.BusinessType;
            prospect.Address = input.Address;
            prospect.City = input.City;
            prospect.Province = input.Province;
            prospect.PostalCode = input.PostalCode;
            prospect.Source = input.Source;
            prospect.Stage = input.Stage;
            prospect.SpecialEvents = input.SpecialEvents;
            prospect.Facebook = input.Facebook;
            prospect.LinkedIn = input.LinkedIn;
            prospect.GooglePlus = input.GooglePlus;
            prospect.Instagram = input.Instagram;
            prospect.Twitter = input.Twitter;
        }
    }
}
