﻿using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using SalesHack;
using SalesHack.Companies;
using Abp.Authorization;
using System;
using Abp.Runtime.Session;
using Abp.AutoMapper;
using SalesHack.Companies.Dto;
using SalesHack.Campanies.Dto;
using Abp.UI;
using SalesHack.Prospects.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;

namespace SalesHack.Prospects

{
    [AbpAuthorize]
    public class CompanyProspectAppService : SalesHackServiceBase
    {
        private readonly ICompanyProspectManager _companyProspectManager;
        private readonly IRepository<CompanyProspect, Guid> _companyProspectRepository;

        public CompanyProspectAppService(IRepository<CompanyProspect, Guid> companyProspectRepository, ICompanyProspectManager _companyProspectManager)
        {
            _companyProspectRepository = companyProspectRepository;
            this._companyProspectManager = _companyProspectManager;
        }

        public async Task Create(CreateCompanyProspectInput input)
        {
            var companyProspect = ObjectMapper.Map<CompanyProspect>(input);

            //var Id = 1;

            //if (AbpSession.TenantId.HasValue)
            //{
            //    Id = AbpSession.TenantId.Value;
            //}

            //company.TenantId = Id;

            await _companyProspectRepository.InsertAsync(companyProspect);
            try
            {

                // var id = AbpSession.GetTenantId();
                //int id = 1;
                var company = CompanyProspect.Create(input.Name, input.Email, 2, input.Size, input.Phase, input.Turnover, input.YearInBusiness, input.WebUrl, input.Telephone, input.Industry, input.BusinessType, input.Address,
                   input.City, input.Province, input.Country, input.BeeStatus, input.PostalCode, input.Source, input.Stage, input.SpecialEvents, input.Facebook, input.LinkedIn, input.GooglePlus, input.Instagram, input.Twitter);

                await _companyProspectManager.CreateAsync(company);
            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }

        }

        public async Task<ListResultDto<CompanyProspectListDto>> GetAll(GetCompanyProspectsInput input)
        {
            var companies = await _companyProspectRepository
            .GetAll().Where(x => (input.companysize == null || x.Size == input.companysize) &&
                (input.industry == null || x.Industry == input.industry) &&
                (input.city == null || x.City == input.city)
                 && (input.county == null || x.Country == input.county) &&
                 (input.bee == null || x.BeeStatus == input.bee)
            ).Distinct()
            //.WhereIf(!input.IncludeCanceledEvents, e => !e.IsCancelled)
            .OrderBy(e => e.Name)
            .ToListAsync();

            return new ListResultDto<CompanyProspectListDto>(companies.MapTo<List<CompanyProspectListDto>>());
        }
        public async Task<ListResultDto<CompanyProspectListDto>> GetById(Guid input)
        {

            var companies = await _companyProspectRepository
            .GetAll().Where(x => x.Id == input)
            .Include(e => e.Contacts)
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<CompanyProspectListDto>(companies.MapTo<List<CompanyProspectListDto>>());
        }
        public async Task<ListResultDto<CompanyProspectListDto>> NameSearch(string input)
        {

            var companies = await _companyProspectRepository
            .GetAll().WhereIf(
                !input.IsNullOrEmpty(),
                p => p.Name.Contains(input) ||
                     p.Address.Contains(input) ||
                     p.Email.Contains(input))
                        .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<CompanyProspectListDto>(companies.MapTo<List<CompanyProspectListDto>>());
        }
        public async Task Delete(EntityDto<Guid> input)
        {
            await _companyProspectRepository.DeleteAsync(input.Id);
        }

        public async Task<CompanyProspectDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var company = await _companyProspectRepository
                .GetAll()
                .Include(e => e.Contacts)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();
            if (company == null)
            {
                throw new UserFriendlyException("Could not find the company, maybe it's deleted.");
            }

            return company.MapTo<CompanyProspectDetailOutput>();
        }
    }

}
