﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Prospects.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public interface IProspectAppService : IApplicationService
    {
        Task<ProspectDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<ListResultDto<ProspectListDto>> GetAll(GetAllProspectInput input);
        Task<ListResultDto<ProspectListDto>> Search(GetCompanyNameInput input); 
        Task Create(CreateProspectInput input);
        Task Delete(EntityDto<Guid> input);
    }
}
