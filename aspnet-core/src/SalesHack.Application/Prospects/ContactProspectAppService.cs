﻿using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using SalesHack;
using SalesHack.Companies;
using Abp.Authorization;
using System;
using Abp.Runtime.Session;
using Abp.AutoMapper;
using SalesHack.Companies.Dto;
using SalesHack.Campanies.Dto;
using Abp.UI;
using SalesHack.Prospects.Dto;

namespace SalesHack.Prospects

{
    [AbpAuthorize]
    public class ContactProspectAppService : SalesHackServiceBase
    {
        private readonly IContactProspectManager _contactProspectManager;
        private readonly IRepository<ContactProspect, Guid> _contactProspectRepository;

        public ContactProspectAppService(IRepository<ContactProspect, Guid> contactProspectRepository, IContactProspectManager contactProspectManager)
        {
            _contactProspectRepository = contactProspectRepository;
            _contactProspectManager = contactProspectManager;
        }

        public async Task Create(CreateContactProspectInput input)
        {
            var contactProspect = ObjectMapper.Map<ContactProspect>(input);


            await _contactProspectRepository.InsertAsync(contactProspect);
            try
            {

                //var id = 1;
                var contact = ContactProspect.Create(input.CompanyId, input.Name, input.Surname, input.Source, input.JobTitle, input.Department,
                    input.Telephone, input.Cellphone, input.Email, input.Industry, input.Address, input.City, input.Province, input.SpecialEvents,
                    input.Facebook, input.LinkedIn, input.GooglePlus, input.Instagram, input.Twitter);

                await _contactProspectManager.CreateAsync(contact);
            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }

        }
        public async Task UpdateStatus(ContactProspect input)
        {
            var contact = await _contactProspectRepository.GetAsync(input.Id);
            contact.Status = input.Status;
            await _contactProspectRepository.UpdateAsync(contact);
        }

        public async Task<ListResultDto<ContactProspectListDto>> GetAll(GetContactProspectsInput input)
        {
            var contacts = await _contactProspectRepository
            .GetAll().Where(x => (input.jobTitle == null || x.JobTitle == input.jobTitle) &&
                (input.companySize == null || x.Cellphone == input.companySize) &&
                (input.firstName == null || x.Name == input.firstName) &&
                  (input.lastName == null || x.Surname == input.lastName) &&
                 (input.industry == null || x.Industry == input.industry) &&
                 (input.city == null || x.City == input.city)
            //&& (input.county == null || x.PostalCode == input.county)
            ).Include(e => e.ContactCompany)
            //.WhereIf(!input.IncludeCanceledEvents, e => !e.IsCancelled)
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<ContactProspectListDto>(contacts.MapTo<List<ContactProspectListDto>>());
        }

        public async Task<ListResultDto<ContactProspectListDto>> GetCompanyId(Guid input)
        {
            var contacts = await _contactProspectRepository
            .GetAll().Where(x => x.CompanyId.Equals(input)).OrderBy(e => e.Name).ToListAsync();
            return new ListResultDto<ContactProspectListDto>(contacts.MapTo<List<ContactProspectListDto>>());
        }
        public async Task<ContactProspectDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var contact = await _contactProspectRepository
                .GetAll()
                .Include(e => e.ContactCompany)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();
            if (contact == null)
            {
                throw new UserFriendlyException("Could not find the Contact, maybe it's deleted.");
            }
            return contact.MapTo<ContactProspectDetailOutput>();
        }
    }
}
