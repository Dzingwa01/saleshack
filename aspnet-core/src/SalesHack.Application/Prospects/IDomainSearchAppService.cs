﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Prospects.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public interface IDomainSearchAppService : IApplicationService
    {
        List<string> GetWebEmails(string domain);
    }
}
