﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects
{
    [AutoMapFrom(typeof(ProspectList))]
    public class ProspectListsDto : EntityDto<Guid>, IHasCreationTime
    {
        public virtual Guid ContactProspectId { get; set; }
        public virtual string Name { get; set; }
        public virtual ContactProspect ContactProspect { get; set; }
        public bool IsQualified { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
