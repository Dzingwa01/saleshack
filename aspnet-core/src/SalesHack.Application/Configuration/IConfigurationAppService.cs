﻿using System.Threading.Tasks;
using SalesHack.Configuration.Dto;

namespace SalesHack.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
