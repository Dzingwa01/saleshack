﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using SalesHack.Configuration.Dto;

namespace SalesHack.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : SalesHackServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
