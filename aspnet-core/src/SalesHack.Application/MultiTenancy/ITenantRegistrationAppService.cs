﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SalesHack.Editions.Dto;
using SalesHack.MultiTenancy.Dto;

namespace SalesHack.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        //Task<TenantDto> RegisterTenantAsync(CreateTenantDto input);

        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}