﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using SalesHack.Authorization.Roles;
using SalesHack.Authorization.Users;
using SalesHack.Editions;
using SalesHack.MultiTenancy.Dto;
using Microsoft.AspNetCore.Identity;
using Abp.Net.Mail;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net.Mail;
using System.Net;
using SalesHack.MultiTenancy.Payments.Dto;
using System;
using Abp.UI;
using SalesHack.Editions.Dto;
using SalesHack.Features;
using SalesHack.Configuration;
using System.Collections.Generic;
using SalesHack.MultiTenancy.Payments;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.Configuration.Startup;
using Abp.Localization;
using SalesHack.MultiTenancy.Payments.Cache;
using SalesHack.Url;
using SalesHack.Security.Recaptcha;
using SalesHack.Notifications;
using Abp.Application.Features;
using Abp.Zero.Configuration;
using Abp.Authorization.Users;

using Abp.Configuration;
using SalesHack.Debugging;

namespace SalesHack.MultiTenancy
{
    public class TenantRegistrationAppService : SalesHackServiceBase, ITenantRegistrationAppService
    {
        //private readonly TenantManager _tenantManager;
        //private readonly EditionManager _editionManager;
        //private readonly UserManager _userManager;
        //private readonly RoleManager _roleManager;
        //private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        //private readonly IPasswordHasher<User> _passwordHasher;
        //private readonly IEmailSender _emailSender;
        //private readonly IHostingEnvironment _environment;

        //public IAppUrlService AppUrlService { get; set; }

        //private readonly IMultiTenancyConfig _multiTenancyConfig;
        //private readonly IRecaptchaValidator _recaptchaValidator;
        ////private readonly EditionManager _editionManager;
        //private readonly IAppNotifier _appNotifier;
        //private readonly ILocalizationContext _localizationContext;
        ////private readonly TenantManager _tenantManager;
        //private readonly ISubscriptionPaymentRepository _subscriptionPaymentRepository;
        //private readonly IPaymentGatewayManagerFactory _paymentGatewayManagerFactory;
        //private readonly IPaymentCache _paymentCache;

        //public TenantRegistrationAppService(
        //    IRepository<Tenant, int> repository,
        //    TenantManager tenantManager,
        //    EditionManager editionManager,
        //    UserManager userManager,
        //    RoleManager roleManager,
        //    IAbpZeroDbMigrator abpZeroDbMigrator,
        //    IPasswordHasher<User> passwordHasher,
        //     IEmailSender emailSender,
        //      IHostingEnvironment environment,
        //      IMultiTenancyConfig multiTenancyConfig,
        //    IRecaptchaValidator recaptchaValidator,        
        //    IAppNotifier appNotifier,
        //    ILocalizationContext localizationContext,

        //    ISubscriptionPaymentRepository subscriptionPaymentRepository,
        //    IPaymentGatewayManagerFactory paymentGatewayManagerFactory,
        //    IPaymentCache paymentCache)
        //    : base(repository)
        //{
        //    _tenantManager = tenantManager;
        //    _editionManager = editionManager;
        //    _userManager = userManager;
        //    _roleManager = roleManager;
        //    _abpZeroDbMigrator = abpZeroDbMigrator;
        //    _passwordHasher = passwordHasher;
        //    _emailSender = emailSender;
        //    _environment = environment;

        //    _multiTenancyConfig = multiTenancyConfig;
        //    _recaptchaValidator = recaptchaValidator;         
        //    _appNotifier = appNotifier;
        //    _localizationContext = localizationContext;      
        //    _subscriptionPaymentRepository = subscriptionPaymentRepository;
        //    _paymentGatewayManagerFactory = paymentGatewayManagerFactory;
        //    _paymentCache = paymentCache;

        //    AppUrlService = NullAppUrlService.Instance;
        //}

        //public async Task<TenantDto> RegisterTenantAsync(CreateTenantDto input)
        //{
        //    CheckCreatePermission();

        //    // Create tenant
        //    var tenant = ObjectMapper.Map<Tenant>(input);
        //    tenant.ConnectionString = input.ConnectionString.IsNullOrEmpty()
        //        ? null
        //        : SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

        //    var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
        //    if (defaultEdition != null)
        //    {
        //        tenant.EditionId = defaultEdition.Id;
        //    }
        //    tenant.IsActive = false;
        //    await _tenantManager.CreateAsync(tenant);
        //    await CurrentUnitOfWork.SaveChangesAsync(); // To get new tenant's id.

        //    // Create tenant database
        //    _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

        //    // We are working entities of new tenant, so changing tenant filter
        //    using (CurrentUnitOfWork.SetTenantId(tenant.Id))
        //    {
        //        // Create static roles for new tenant
        //        CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));

        //        await CurrentUnitOfWork.SaveChangesAsync(); // To get static role ids

        //        // Grant all permissions to admin role
        //        var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
        //        await _roleManager.GrantAllPermissionsAsync(adminRole);


        //        // Create admin user for the tenant
        //        var adminUser = User.CreateTenantAdminUser(tenant.Id, input.AdminEmailAddress, input.Name, input.LastName, input.Password, input.Cellphone);
        //        adminUser.Password = _passwordHasher.HashPassword(adminUser, input.Password);
        //        //adminUser.IsEmailConfirmed = true;


        //        var emailConfirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(adminUser);
        //        adminUser.IsEmailConfirmed = false;
        //        adminUser.EmailConfirmationCode = emailConfirmationToken;
        //        // send email using emailConfirmationToken and user.Id

        //        CheckErrors(await _userManager.CreateAsync(adminUser));
        //        await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

        //        // Assign admin user to role!
        //        CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));
        //        await CurrentUnitOfWork.SaveChangesAsync();

        //        //send an email here
        //        string body = string.Empty;

        //        //using streamreader for reading my html template   

        //        var path = Path.Combine(_environment.WebRootPath, "templates/email/cornfim-account.html");

        //        using (StreamReader reader = new StreamReader(path))
        //        {
        //            body = reader.ReadToEnd();
        //        }


        //        // 

        //        //string link = "http://localhost:4200/account/confirmation?code=" + emailConfirmationToken + "&id=" + adminUser.Id;
        //        //string link = "http://"+ tenant.TenancyName.ToLower()+ ".saleshack.io/account/confirmation?code=" + emailConfirmationToken + "&id=" + adminUser.Id;
        //        string link = "https://app.saleshack.io/account/confirmation?code=" + emailConfirmationToken + "&id=" + adminUser.Id;
        //        //string messageBody = string.Format(body, adminUser.Name, link);

        //        body = body.Replace("#Name", adminUser.Name);
        //        body = body.Replace("#Link", link);

        //        // body = body.Replace("#PlainLink", link);
        //        //SendTest();
        //        Emailer.Send(to: adminUser.EmailAddress, subject: "Sales Hack Registration!", body: body, isBodyHtml: true);

        //        //await _emailSender.SendAsync(to: "lundi@developmenthub.co.za", subject: "Sales Hack Registration!", body: "test", isBodyHtml: true);
        //        //await _emailSender.SendAsync(to: adminUser.EmailAddress, subject: "Sales Hack Registration!", body: body, isBodyHtml: true);


        //    }

        //    return MapToEntityDto(tenant);
        //}

        //private void CheckErrors(IdentityResult identityResult)
        //{
        //    identityResult.CheckErrors(LocalizationManager);
        //}


        //public void SendTest()
        //{

        //    MailAddress to = new MailAddress("lundi@developmenthub.co.za");


        //    MailAddress from = new MailAddress("info@kipson.co.za");

        //    MailMessage mail = new MailMessage(from, to);


        //    mail.Subject = "Testing Subject";


        //    mail.Body = "Testing Body";


        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "smtp.sendgrid.net";
        //    smtp.Port = 587;

        //    smtp.Credentials = new NetworkCredential(
        //        "apikey", "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");
        //    // smtp.EnableSsl = true;


        //    //try
        //    //{
        //        smtp.Send(mail);
        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //    throw;
        //    //}
        //}

        public IAppUrlService AppUrlService { get; set; }

        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly IRecaptchaValidator _recaptchaValidator;
        private readonly EditionManager _editionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly ILocalizationContext _localizationContext;
        private readonly TenantManager _tenantManager;
        private readonly ISubscriptionPaymentRepository _subscriptionPaymentRepository;
        private readonly IPaymentGatewayManagerFactory _paymentGatewayManagerFactory;
        private readonly IPaymentCache _paymentCache;
     

        public TenantRegistrationAppService(
            IMultiTenancyConfig multiTenancyConfig,
            IRecaptchaValidator recaptchaValidator,
            EditionManager editionManager,
            IAppNotifier appNotifier,
            ILocalizationContext localizationContext,
            TenantManager tenantManager,
            ISubscriptionPaymentRepository subscriptionPaymentRepository,
            IPaymentGatewayManagerFactory paymentGatewayManagerFactory,
            IPaymentCache paymentCache)
        {
            _multiTenancyConfig = multiTenancyConfig;
            _recaptchaValidator = recaptchaValidator;
            _editionManager = editionManager;
            _appNotifier = appNotifier;
            _localizationContext = localizationContext;
            _tenantManager = tenantManager;
            _subscriptionPaymentRepository = subscriptionPaymentRepository;
            _paymentGatewayManagerFactory = paymentGatewayManagerFactory;
            _paymentCache = paymentCache;
           

            AppUrlService = NullAppUrlService.Instance;
            //AppUrlService = new AppUrlServiceBase();
        }

        public async Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input)
        {
            if (input.EditionId.HasValue)
            {
                await CheckEditionSubscriptionAsync(input.EditionId.Value, input.SubscriptionStartType, input.Gateway, input.PaymentId);
            }
            else
            {
                await CheckRegistrationWithoutEdition();
            }

            
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                //CheckTenantRegistrationIsEnabled();

                //if (UseCaptchaOnRegistration())
                //{
                //    await _recaptchaValidator.ValidateAsync(input.CaptchaResponse);
                //}

                //Getting host-specific settings
                var isNewRegisteredTenantActiveByDefault = true;// await SettingManager.GetSettingValueForApplicationAsync<bool>(AppSettings.TenantManagement.IsNewRegisteredTenantActiveByDefault);
                var isEmailConfirmationRequiredForLogin = true;// await SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

                DateTime? subscriptionEndDate = null;
                var isInTrialPeriod = false;

                if (input.EditionId.HasValue)
                {
                    isInTrialPeriod = input.SubscriptionStartType == SubscriptionStartType.Trial;

                    if (isInTrialPeriod)
                    {
                        var edition = (SubscribableEdition)await _editionManager.GetByIdAsync(input.EditionId.Value);
                        subscriptionEndDate = Clock.Now.AddDays(edition.TrialDayCount ?? 0);
                    }
                }

                subscriptionEndDate = Clock.Now.AddDays(30);


                var tenantId = await _tenantManager.CreateWithAdminUserAsync(
                    input.TenancyName,
                    input.Name,
                    input.Password,
                    input.AdminEmailAddress,
                    null,
                    isNewRegisteredTenantActiveByDefault,
                    input.EditionId,
                    false,
                    true,
                    subscriptionEndDate,
                    isInTrialPeriod,
                   ""//AppUrlService.CreateEmailActivationUrlFormat(input.TenancyName)
                );

                Tenant tenant = await TenantManager.GetByIdAsync(tenantId); ;

                if (input.SubscriptionStartType == SubscriptionStartType.Paid)
                {
                    if (!input.Gateway.HasValue)
                    {
                        throw new Exception("Gateway is missing!");
                    }

                    var payment = await _subscriptionPaymentRepository.GetByGatewayAndPaymentIdAsync(
                        input.Gateway.Value,
                        input.PaymentId
                    );

                    tenant = await _tenantManager.UpdateTenantAsync(
                        tenantId,
                        true,
                        false,
                        payment.PaymentPeriodType,
                        payment.EditionId,
                        EditionPaymentType.NewRegistration);

                    await _subscriptionPaymentRepository.UpdateByGatewayAndPaymentIdAsync(input.Gateway.Value,
                        input.PaymentId, tenantId, SubscriptionPaymentStatus.Completed);
                }
                else
                {
                    tenant = await TenantManager.GetByIdAsync(tenantId);
                }

                await _appNotifier.NewTenantRegisteredAsync(tenant);

                if (input.EditionId.HasValue && input.Gateway.HasValue && !input.PaymentId.IsNullOrEmpty())
                {
                    _paymentCache.RemoveCacheItem(input.Gateway.Value, input.PaymentId);
                }

                
                return new RegisterTenantOutput
                {
                    TenantId = tenant.Id,
                    TenancyName = input.TenancyName,
                    Name = input.Name,
                    UserName = AbpUserBase.AdminUserName,
                    EmailAddress = input.AdminEmailAddress,
                    IsActive = tenant.IsActive,
                    IsEmailConfirmationRequired = isEmailConfirmationRequiredForLogin,
                    IsTenantActive = tenant.IsActive
                };
            }


        }

       

        private async Task CheckRegistrationWithoutEdition()
        {
            var editions = await _editionManager.GetAllAsync();
            if (editions.Any())
            {
                throw new Exception("Tenant registration is not allowed without edition because there are editions defined !");
            }
        }

        public async Task<EditionsSelectOutput> GetEditionsForSelect()
        {
            var features = FeatureManager
                .GetAll()
                .Where(feature => (feature[FeatureMetadata.CustomFeatureKey] as FeatureMetadata)?.IsVisibleOnPricingTable ?? false);

            var flatFeatures = ObjectMapper
                .Map<List<FlatFeatureSelectDto>>(features)
                .OrderBy(f => f.DisplayName)
                .ToList();

            var editions = (await _editionManager.GetAllAsync())
                .Cast<SubscribableEdition>()
                .OrderBy(e => e.MonthlyPrice)
                .ToList();

            var featureDictionary = features.ToDictionary(feature => feature.Name, f => f);

            var editionWithFeatures = new List<EditionWithFeaturesDto>();
            foreach (var edition in editions)
            {
                editionWithFeatures.Add(await CreateEditionWithFeaturesDto(edition, featureDictionary));
            }

            int? tenantEditionId = null;
            if (AbpSession.UserId.HasValue)
            {
                tenantEditionId = (await _tenantManager.GetByIdAsync(AbpSession.GetTenantId()))
                    .EditionId;
            }

            return new EditionsSelectOutput
            {
                AllFeatures = flatFeatures,
                EditionsWithFeatures = editionWithFeatures,
                TenantEditionId = tenantEditionId
            };
        }

        public async Task<EditionSelectDto> GetEdition(int editionId)
        {
            var edition = await _editionManager.GetByIdAsync(editionId);
            var editionDto = ObjectMapper.Map<EditionSelectDto>(edition);

            foreach (var paymentGateway in Enum.GetValues(typeof(SubscriptionPaymentGatewayType)).Cast<SubscriptionPaymentGatewayType>())
            {
                using (var paymentGatewayManager = _paymentGatewayManagerFactory.Create(paymentGateway))
                {
                    var additionalData = await paymentGatewayManager.Object.GetAdditionalPaymentData(ObjectMapper.Map<SubscribableEdition>(edition));
                    editionDto.AdditionalData.Add(paymentGateway, additionalData);
                }
            }

            return editionDto;
        }

        private async Task<EditionWithFeaturesDto> CreateEditionWithFeaturesDto(SubscribableEdition edition, Dictionary<string, Feature> featureDictionary)
        {
            return new EditionWithFeaturesDto
            {
                Edition = ObjectMapper.Map<EditionSelectDto>(edition),
                FeatureValues = (await _editionManager.GetFeatureValuesAsync(edition.Id))
                    .Where(featureValue => featureDictionary.ContainsKey(featureValue.Name))
                    .Select(fv => new NameValueDto(
                        fv.Name,
                        featureDictionary[fv.Name].GetValueText(fv.Value, _localizationContext))
                    )
                    .ToList()
            };
        }

        private void CheckTenantRegistrationIsEnabled()
        {
            if (!IsSelfRegistrationEnabled())
            {
                throw new UserFriendlyException(L("SelfTenantRegistrationIsDisabledMessage_Detail"));
            }

            if (!_multiTenancyConfig.IsEnabled)
            {
                throw new UserFriendlyException(L("MultiTenancyIsNotEnabled"));
            }
        }

        private bool IsSelfRegistrationEnabled()
        {
            var getAll = SettingManager.GetAllSettingValuesForApplicationAsync();

            return SettingManager.GetSettingValueForApplication<bool>(AppSettings.TenantManagement.AllowSelfRegistration);
        }

        private bool UseCaptchaOnRegistration()
        {
            if (DebugHelper.IsDebug)
            {
                return false;
            }

            return SettingManager.GetSettingValueForApplication<bool>(AppSettings.TenantManagement.UseCaptchaOnRegistration);
        }

        private async Task CheckEditionSubscriptionAsync(int editionId, SubscriptionStartType subscriptionStartType, SubscriptionPaymentGatewayType? gateway, string paymentId)
        {
            var edition = await _editionManager.GetByIdAsync(editionId) as SubscribableEdition;

            CheckSubscriptionStart(edition, subscriptionStartType);
            CheckPaymentCache(edition, subscriptionStartType, gateway, paymentId);
        }

        private void CheckPaymentCache(SubscribableEdition edition, SubscriptionStartType subscriptionStartType, SubscriptionPaymentGatewayType? gateway, string paymentId)
        {
            if (edition.IsFree || subscriptionStartType != SubscriptionStartType.Paid)
            {
                return;
            }

            if (!gateway.HasValue)
            {
                throw new Exception("Gateway cannot be empty !");
            }

            if (paymentId.IsNullOrEmpty())
            {
                throw new Exception("PaymentId cannot be empty !");
            }

            var paymentCacheItem = _paymentCache.GetCacheItemOrNull(gateway.Value, paymentId);
            if (paymentCacheItem == null)
            {
                throw new UserFriendlyException(L("PaymentMightBeExpiredWarning"));
            }
        }

        private static void CheckSubscriptionStart(SubscribableEdition edition, SubscriptionStartType subscriptionStartType)
        {
            switch (subscriptionStartType)
            {
                case SubscriptionStartType.Free:
                    if (!edition.IsFree)
                    {
                        throw new Exception("This is not a free edition !");
                    }
                    break;
                case SubscriptionStartType.Trial:
                    if (!edition.HasTrial())
                    {
                        throw new Exception("Trial is not available for this edition !");
                    }
                    break;
                case SubscriptionStartType.Paid:
                    if (edition.IsFree)
                    {
                        throw new Exception("This is a free edition and cannot be subscribed as paid !");
                    }
                    break;
            }
        }

    }
}
