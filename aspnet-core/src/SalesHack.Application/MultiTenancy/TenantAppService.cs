using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using SalesHack.Authorization;
using SalesHack.Authorization.Roles;
using SalesHack.Authorization.Users;
using SalesHack.Editions;
using SalesHack.MultiTenancy.Dto;
using SalesHack.Url;
using Abp.Authorization.Users;
using Abp;
using SalesHack.Editions.Dto;
using System.Collections.Generic;
using Abp.Domain.Uow;


using System.Linq.Dynamic.Core;
using Abp.Application.Features;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;


namespace SalesHack.MultiTenancy
{
    //[AbpAuthorize(PermissionNames.Pages_Tenants)]
    //public class TenantAppService : AsyncCrudAppService<Tenant, TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>, ITenantAppService
    [AbpAuthorize(AppPermissions.Pages_Tenants)]
    public class TenantAppService : SalesHackServiceBase, ITenantAppService
    {
        //private readonly TenantManager _tenantManager;
        //private readonly EditionManager _editionManager;
        //private readonly UserManager _userManager;
        //private readonly RoleManager _roleManager;
        //private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        //private readonly IPasswordHasher<User> _passwordHasher;

        public IAppUrlService AppUrlService { get; set; }

        public TenantAppService()
        {
            AppUrlService = NullAppUrlService.Instance;
        }

        //public TenantAppService(
        //    IRepository<Tenant, int> repository, 
        //    TenantManager tenantManager, 
        //    EditionManager editionManager,
        //    UserManager userManager,            
        //    RoleManager roleManager, 
        //    IAbpZeroDbMigrator abpZeroDbMigrator, 
        //    IPasswordHasher<User> passwordHasher) 
        //    : base(repository)
        //{
        //    _tenantManager = tenantManager; 
        //    _editionManager = editionManager;
        //    _userManager = userManager;
        //    _roleManager = roleManager;
        //    _abpZeroDbMigrator = abpZeroDbMigrator;
        //    _passwordHasher = passwordHasher;
        //    AppUrlService = NullAppUrlService.Instance;
        //}

        //public override async Task<TenantDto> Create(CreateTenantDto input)
        //{
        //    CheckCreatePermission();

        //    // Create tenant
        //    var tenant = ObjectMapper.Map<Tenant>(input);
        //    tenant.ConnectionString = input.ConnectionString.IsNullOrEmpty()
        //        ? null
        //        : SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

        //    var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
        //    if (defaultEdition != null)
        //    {
        //        tenant.EditionId = defaultEdition.Id;
        //    }

        //    await _tenantManager.CreateAsync(tenant);
        //    await CurrentUnitOfWork.SaveChangesAsync(); // To get new tenant's id.

        //    // Create tenant database
        //    _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

        //    // We are working entities of new tenant, so changing tenant filter
        //    using (CurrentUnitOfWork.SetTenantId(tenant.Id))
        //    {
        //        // Create static roles for new tenant
        //        CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));

        //        await CurrentUnitOfWork.SaveChangesAsync(); // To get static role ids

        //        // Grant all permissions to admin role
        //        var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
        //        await _roleManager.GrantAllPermissionsAsync(adminRole);

        //        // Create admin user for the tenant
        //        var adminUser = User.CreateTenantAdminUser(tenant.Id, input.AdminEmailAddress);
        //        adminUser.Password = _passwordHasher.HashPassword(adminUser, User.DefaultPassword);
        //        CheckErrors(await _userManager.CreateAsync(adminUser));
        //        await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

        //        // Assign admin user to role!
        //        CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));
        //        await CurrentUnitOfWork.SaveChangesAsync();
        //    }

        //    return MapToEntityDto(tenant);
        //}

        //protected override void MapToEntity(TenantDto updateInput, Tenant entity)
        //{
        //    // Manually mapped since TenantDto contains non-editable properties too.
        //    entity.Name = updateInput.Name;
        //    entity.TenancyName = updateInput.TenancyName;
        //    entity.IsActive = updateInput.IsActive;
        //}

        //public override async Task Delete(EntityDto<int> input)
        //{
        //    CheckDeletePermission();

        //    var tenant = await _tenantManager.GetByIdAsync(input.Id);
        //    await _tenantManager.DeleteAsync(tenant);
        //}

        //private void CheckErrors(IdentityResult identityResult)
        //{
        //    identityResult.CheckErrors(LocalizationManager);
        //}


        public async Task<PagedResultDto<TenantListDto>> GetTenants(GetTenantsInput input)
        {
            var query = TenantManager.Tenants
                .Include(t => t.Edition)
                .WhereIf(!input.Filter.IsNullOrWhiteSpace(), t => t.Name.Contains(input.Filter) || t.TenancyName.Contains(input.Filter))
                .WhereIf(input.CreationDateStart.HasValue, t => t.CreationTime >= input.CreationDateStart.Value)
                .WhereIf(input.CreationDateEnd.HasValue, t => t.CreationTime <= input.CreationDateEnd.Value)
                .WhereIf(input.SubscriptionEndDateStart.HasValue, t => t.SubscriptionEndDateUtc >= input.SubscriptionEndDateStart.Value.ToUniversalTime())
                .WhereIf(input.SubscriptionEndDateEnd.HasValue, t => t.SubscriptionEndDateUtc <= input.SubscriptionEndDateEnd.Value.ToUniversalTime())
                .WhereIf(input.EditionIdSpecified, t => t.EditionId == input.EditionId);

            var tenantCount = await query.CountAsync();
            var tenants = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<TenantListDto>(
                tenantCount,
                ObjectMapper.Map<List<TenantListDto>>(tenants)
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Create)]
        [UnitOfWork(IsDisabled = true)]
        public async Task CreateTenant(CreateTenantInput input)
        {
            await TenantManager.CreateWithAdminUserAsync(input.TenancyName,
                input.Name,
                input.AdminPassword,
                input.AdminEmailAddress,
                input.ConnectionString,
                input.IsActive,
                input.EditionId,
                input.ShouldChangePasswordOnNextLogin,
                input.SendActivationEmail,
                input.SubscriptionEndDateUtc?.ToUniversalTime(),
                input.IsInTrialPeriod,
                AppUrlService.CreateEmailActivationUrlFormat(input.TenancyName)
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Edit)]
        public async Task<TenantEditDto> GetTenantForEdit(EntityDto input)
        {
            var tenantEditDto = ObjectMapper.Map<TenantEditDto>(await TenantManager.GetByIdAsync(input.Id));
            tenantEditDto.ConnectionString = SimpleStringCipher.Instance.Decrypt(tenantEditDto.ConnectionString);
            return tenantEditDto;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Edit)]
        public async Task UpdateTenant(TenantEditDto input)
        {
            await TenantManager.CheckEditionAsync(input.EditionId, input.IsInTrialPeriod);

            input.ConnectionString = SimpleStringCipher.Instance.Encrypt(input.ConnectionString);
            var tenant = await TenantManager.GetByIdAsync(input.Id);
            ObjectMapper.Map(input, tenant);
            tenant.SubscriptionEndDateUtc = tenant.SubscriptionEndDateUtc?.ToUniversalTime();

            await TenantManager.UpdateAsync(tenant);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Delete)]
        public async Task DeleteTenant(EntityDto input)
        {
            var tenant = await TenantManager.GetByIdAsync(input.Id);
            await TenantManager.DeleteAsync(tenant);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task<GetTenantFeaturesEditOutput> GetTenantFeaturesForEdit(EntityDto input)
        {
            var features = FeatureManager.GetAll()
                .Where(f => f.Scope.HasFlag(FeatureScopes.Tenant));
            var featureValues = await TenantManager.GetFeatureValuesAsync(input.Id);

            return new GetTenantFeaturesEditOutput
            {
                Features = ObjectMapper.Map<List<FlatFeatureDto>>(features).OrderBy(f => f.DisplayName).ToList(),
                FeatureValues = featureValues.Select(fv => new NameValueDto(fv)).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task UpdateTenantFeatures(UpdateTenantFeaturesInput input)
        {
            await TenantManager.SetFeatureValuesAsync(input.Id, input.FeatureValues.Select(fv => new NameValue(fv.Name, fv.Value)).ToArray());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task ResetTenantSpecificFeatures(EntityDto input)
        {
            await TenantManager.ResetAllFeaturesAsync(input.Id);
        }

        public async Task UnlockTenantAdmin(EntityDto input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.Id))
            {
                var tenantAdmin = await UserManager.FindByNameAsync(AbpUserBase.AdminUserName);
                if (tenantAdmin != null)
                {
                    tenantAdmin.Unlock();
                }
            }
        }

    }
}
