﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using SalesHack.MultiTenancy.Payments;
using SalesHack.MultiTenancy.Payments.Dto;

namespace SalesHack.MultiTenancy.Dto
{
    public class RegisterTenantInput
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string AdminEmailAddress { get; set; }

        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        public string AdminPassword { get; set; }

        [DisableAuditing]
        public string CaptchaResponse { get; set; }

        public SubscriptionStartType SubscriptionStartType { get; set; }

        public SubscriptionPaymentGatewayType? Gateway { get; set; }

        public int? EditionId { get; set; }

        public string PaymentId { get; set; }

       
        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string LastName { get; set; }

        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string CompanyName { get; set; }


        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string Cellphone { get; set; }

        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string Password { get; set; }


        [StringLength(AbpTenantBase.MaxConnectionStringLength)]
        public string ConnectionString { get; set; }

        public bool IsActive { get; set; }
    }
}