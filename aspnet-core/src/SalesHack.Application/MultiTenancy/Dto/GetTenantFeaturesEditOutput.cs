﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using SalesHack.Editions.Dto;

namespace SalesHack.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}