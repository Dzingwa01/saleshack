﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace SalesHack.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task UpgradeTenantToEquivalentEdition(int upgradeEditionId);
    }
}
