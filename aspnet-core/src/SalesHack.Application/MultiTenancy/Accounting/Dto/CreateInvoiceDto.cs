﻿namespace SalesHack.MultiTenancy.Accounting.Dto
{
    public class CreateInvoiceDto
    {
        public long SubscriptionPaymentId { get; set; }
    }
}