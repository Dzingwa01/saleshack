﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SalesHack.MultiTenancy.Accounting.Dto;

namespace SalesHack.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
