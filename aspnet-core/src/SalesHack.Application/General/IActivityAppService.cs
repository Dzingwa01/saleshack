﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IActivityAppService : IApplicationService
    {
        Task<ActivityDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<ListResultDto<ActivityListDto>> GetById(Guid input);

    }
}
