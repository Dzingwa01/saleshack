﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IEmailAppService : IApplicationService
    {
        Task<EmailDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<ListResultDto<EmailListDto>> GetById(Guid input);
        Task CreateEmail(CreateEmailInput input);
        bool SendEmail(CreateEmailInput email);
    }
}
