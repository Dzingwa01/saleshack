﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface ITasksAppService : IApplicationService
    {
        Task<TasksDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<ListResultDto<TasksListDto>> GetById(Guid input);
        Task Edit(TasksDetailOutput input);
        Task<ListResultDto<TasksListDto>> GetTasks(string TargetType, Guid targetId);
        Task<ListResultDto<TasksListDto>> GetAllUserTasks(long userId);
    }
}
