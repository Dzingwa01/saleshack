﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class TasksAppService : AsyncCrudAppService<Tasks, TasksListDto, Guid, PagedAndSortedResultRequestDto, CreateTasksInput, TasksListDto>, ITasksAppService
    {
        private readonly ITasksManager _tasksManager;
        private readonly IRepository<Tasks, Guid> _tasksRepository;

        public TasksAppService(ITasksManager tasksManager, IRepository<Tasks, Guid> tasksRepository) : base(tasksRepository)
        {
            _tasksManager = tasksManager;
            _tasksRepository = tasksRepository;
        }

        /* public async Task Create(Tasks input)
         {
             try
             {
                 var task = Tasks.Create(AbpSession.GetTenantId(), input.Description, input.TaskStatusId, input.DueDate, input.CompletedDate, input.TargetId, input.TargetType);
                 await _tasksManager.CreateAsync(task);
             }
             catch (Exception e)
             {

             }
         }*/

        public async Task Edit(TasksDetailOutput input)
        {
            var task = await _tasksRepository.GetAsync(input.Id);
            EditTask(input, task);
            try
            {
                await _tasksRepository.UpdateAsync(task);
            }
            catch (Exception e)
            {

            }
        }

        public async Task<ListResultDto<TasksListDto>> GetById(Guid input)
        {
            var task = await _tasksRepository.FirstOrDefaultAsync(input);
            if (task == null)
            {
                throw new UserFriendlyException("Task Not Found, Maybe it's Deleted");
            }
            return new ListResultDto<TasksListDto>(task.MapTo<List<TasksListDto>>());

        }

        public async Task<ListResultDto<TasksListDto>> GetTasks(string TargetType, Guid targetId)
        {
            var task = await _tasksRepository.GetAll()
                    .Where(x => x.TargetType == TargetType && x.TargetId == targetId).ToListAsync();
            if (task == null)
            {
                throw new UserFriendlyException("Task Not Found, Maybe it's Deleted");
            }
            return new ListResultDto<TasksListDto>(task.MapTo<List<TasksListDto>>());
        }

        public async Task<ListResultDto<TasksListDto>> GetAllUserTasks(long userId)
        {
            var task = await _tasksRepository.GetAll()
                    .Where(x => x.CreatorUserId == userId ).ToListAsync();
            if (task == null)
            {
                throw new UserFriendlyException("Tasks Not Found, Maybe They're Deleted");
            }
            return new ListResultDto<TasksListDto>(task.MapTo<List<TasksListDto>>());
        }

        public async Task<TasksDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var task = await _tasksRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (task == null)
            {
                throw new UserFriendlyException("Could Not Find The Task, Maybe it's deleted");
            }
            return task.MapTo<TasksDetailOutput>();
        }

        private static void EditTask(TasksDetailOutput input, Tasks task)
        {
            task.Description = input.Description == null ? task.Description : input.Description;
            task.TaskStatusId = input.TaskStatusId == -1 ? task.TaskStatusId : input.TaskStatusId;
            task.DueDate = input.DueDate == null ? task.DueDate : input.DueDate;
            task.CompletedDate = input.CompletedDate == null ? task.CompletedDate : input.CompletedDate;
            task.TargetType = input.TargetType == null ? task.TargetType : input.TargetType;
        }
    }
}
