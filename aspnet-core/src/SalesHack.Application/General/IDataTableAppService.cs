﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IDataTableAppService : IApplicationService
    {
        Task<ListResultDto<DataTableListDto>> GetAll(string Type);
        Task<ListResultDto<DataTableListDto>> GetById(int id);
    }
}
