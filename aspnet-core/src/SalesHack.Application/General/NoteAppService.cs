﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class NoteAppService : AsyncCrudAppService<Note, NoteListDto, Guid, PagedAndSortedResultRequestDto, CreateNoteInput, NoteListDto>, INoteAppService
    {
        private readonly INoteManager _noteManager;
        private readonly IActivityManager _activityManager;
        private readonly IRepository<Note, Guid> _noteRepository;
        public NoteAppService(INoteManager noteManager, IActivityManager activityManager, IRepository<Note, Guid> noteRepository) : base(noteRepository)
        {
            _noteManager = noteManager;
            _noteRepository = noteRepository;
            _activityManager = activityManager;
        }
        public async Task CreateNote(CreateNoteInput input)
        {
            var note = Note.Create(AbpSession.GetTenantId(), input.Description, input.TargetId, input.TargetType);
            await _noteManager.CreateAsync(note);
            //var activity = Activity.Create(AbpSession.GetTenantId(), "New Note", "", input.TargetId, input.TargetType);
            //await _activityManager.CreateAsync(activity);

        }
        public async Task Edit(NoteDetailOutput input)
        {
            var note = await _noteRepository.GetAsync(input.Id);
            EditNote(input, note);
            try
            {
                await _noteRepository.UpdateAsync(note);
            }
            catch (Exception e)
            {

            }
        }

        public async Task<ListResultDto<NoteListDto>> GetById(Guid input)
        {
            var note = await _noteRepository.GetAll().Where(x => x.Id == input).ToListAsync();
            if (note == null)
            {
                throw new UserFriendlyException("Deal Not Found Maybe it's deleted");
            }
            return new ListResultDto<NoteListDto>(note.MapTo<List<NoteListDto>>());
        }

        public async Task<ListResultDto<NoteListDto>> GetNotes(string TargetType, Guid targetId)
        {
            var note = await _noteRepository.GetAll()
                .Where(x => x.TargetType == TargetType && x.TargetId == targetId).ToListAsync();
            if (note == null)
            {
                throw new UserFriendlyException("Deal Not Found Maybe it's deleted");
            }
            return new ListResultDto<NoteListDto>(note.MapTo<List<NoteListDto>>());
        }
        public async Task<ListResultDto<NoteListDto>> GetAllUserNotes(long userId)
        {
            var notes = await _noteRepository.GetAll()
                    .Where(x => x.CreatorUserId == userId).ToListAsync();
            if (notes == null)
            {
                throw new UserFriendlyException("Notes Not Found, Maybe They're Deleted");
            }
            return new ListResultDto<NoteListDto>(notes.MapTo<List<NoteListDto>>());
        }
        public async Task<NoteDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var note = await _noteRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (note == null)
            {
                throw new UserFriendlyException("Could not find the Note, maybe it's deleted.");
            }

            return note.MapTo<NoteDetailOutput>();
        }
        private static void EditNote(NoteDetailOutput input, Note note)
        {
            note.Description = input.Description == null ? note.Description : input.Description;
            note.TargetType = input.TargetType == null ? note.TargetType : input.TargetType;
            note.TargetId = input.TargetId == null ? note.TargetId : input.TargetId;
        }
    }
}
