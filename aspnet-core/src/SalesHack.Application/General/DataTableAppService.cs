﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.General.Dto;

namespace SalesHack.General
{
    public class DataTableAppService : SalesHackServiceBase, IDataTableAppService
    {
        private readonly IRepository<DataTable, int> _dataTableRepository;

        public DataTableAppService(IRepository<DataTable, int> dataTableRepository)
        {
            _dataTableRepository = dataTableRepository;
        }
        public async Task<ListResultDto<DataTableListDto>> GetAll(string Type)
        {
            var data = await _dataTableRepository.GetAll().Where(x => x.Type.Equals(Type)).ToListAsync();
            if (data == null)
            {
                throw new UserFriendlyException("No Data Found, Maybe It's Deleted");
            }

            return new ListResultDto<DataTableListDto>(data.MapTo<List<DataTableListDto>>());
        }

        public async Task<ListResultDto<DataTableListDto>> GetById(int id)
        {
            var data = await _dataTableRepository.FirstOrDefaultAsync(id);
            if (data == null)
            {
                throw new UserFriendlyException("Data Not Found, Maybe It's Deleted");
            }

            return new ListResultDto<DataTableListDto>(data.MapTo<List<DataTableListDto>>());
        }
    }
}
