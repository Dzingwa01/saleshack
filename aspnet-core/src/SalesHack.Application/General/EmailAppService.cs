﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using SalesHack.Authorization.Users;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class EmailAppService : AsyncCrudAppService<Email, EmailListDto, Guid, PagedAndSortedResultRequestDto, CreateEmailInput, EmailListDto>, IEmailAppService
    {
        private readonly IAbpSession _session;
        private readonly IEmailManager _emailManager;
        private readonly IRepository<Email, Guid> _emailRepository;
        private readonly IRepository<User, long> _userRepository;

        public EmailAppService(IAbpSession session, IEmailManager emailManager, IRepository<Email, Guid> emailRepository, IRepository<User, long> userRepository) : base(emailRepository)
        {
            _session = session;
            _emailManager = emailManager;
            _emailRepository = emailRepository;
            _userRepository = userRepository;
        }
        public async Task CreateEmail(CreateEmailInput input)
        {
            input.UserId = _session.UserId.Value;
            var user = _userRepository.FirstOrDefault(x => x.Id == input.UserId);
            var email = Email.Create(AbpSession.GetTenantId(), input.From, input.Recipient, input.Subject, input.Message, input.UserId, input.FollowUpDate);
            if (email != null)
            {
                await _emailManager.CreateAsync(email);
                SendEmail(input);
            }
        }
        public bool SendEmail(CreateEmailInput email)
        {
            var user = _userRepository.FirstOrDefault(x => x.Id == email.UserId);
            try
            {
                Emailer.Send(to: email.Recipient, subject: email.Subject, body: email.Message, fromEmail: user.EmailAddress, isBodyHtml: true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<ListResultDto<EmailListDto>> GetById(Guid input)
        {
            var email = await _emailRepository.FirstOrDefaultAsync(input);
            if (email == null)
            {
                throw new UserFriendlyException("Email Not Found Maybe it's deleted");
            }
            return new ListResultDto<EmailListDto>(email.MapTo(new List<EmailListDto>()));
        }

        public async Task<EmailDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var email = await _emailRepository.FirstOrDefaultAsync(input.Id);
            if (email == null)
            {
                throw new UserFriendlyException("Email Not Found Maybe it's deleted");
            }
            return email.MapTo<EmailDetailOutput>();
        }
    }
}
