﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface INoteAppService: IApplicationService
    {
        Task<NoteDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<ListResultDto<NoteListDto>> GetById(Guid input);
        Task Edit(NoteDetailOutput input);
        Task<ListResultDto<NoteListDto>> GetNotes(string TargetType, Guid targetId);
    }
}
