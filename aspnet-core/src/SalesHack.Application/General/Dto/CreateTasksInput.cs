﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    [AutoMapTo(typeof(Tasks))]
    public class CreateTasksInput
    {
        public int TenantId { get; set; }
        public string Description { get; set; }
        public int TaskStatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }
    }
}
