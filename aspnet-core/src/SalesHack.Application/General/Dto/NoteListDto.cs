﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto 
{
    [AutoMapFrom(typeof(Note))]
    public class NoteListDto : FullAuditedEntityDto<Guid>
    {
        public string Description { get; set; }
        public string TargetType { get; set; }
    }
}
