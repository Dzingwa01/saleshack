﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    [AutoMapTo(typeof(Activity))]
    public class CreateActivityInput
    {
        public int TenantId { get; set; }
        public string Description { get; set; }
        public string RouteUrl { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }
    }
}
