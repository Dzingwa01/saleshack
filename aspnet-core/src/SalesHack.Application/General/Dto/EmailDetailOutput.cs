﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    [AutoMapFrom(typeof(Email))]
    public class EmailDetailOutput : FullAuditedEntityDto<Guid>
    {
        public virtual string From { get; set; }
        public virtual string Recipient { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Message { get; set; }
        public virtual long UserId { get; set; }
        public DateTime FollowUpDate { get; set; }
    }
}
