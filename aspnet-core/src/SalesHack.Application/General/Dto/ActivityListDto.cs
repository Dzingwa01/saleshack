﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{

    [AutoMapFrom(typeof(Activity))]
    public class ActivityListDto : FullAuditedEntityDto<Guid>
    {
        public string Description { get; set; }
        public string RouteUrl { get; set; }
        public string TargetType { get; set; }

    }
}
