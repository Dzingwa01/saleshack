﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    public class GetAllDataTableInput
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
