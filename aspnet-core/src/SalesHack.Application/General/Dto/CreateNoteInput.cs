﻿using Abp.AutoMapper;
using SalesHack.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto 
{
    [AutoMapTo(typeof(Note))]
    public class CreateNoteInput
    {
        public string Description { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }
    }
}
