﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SalesHack.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    [AutoMapFrom(typeof(Note))]
    public class NoteDetailOutput : FullAuditedEntityDto<Guid>
    {
        public string Description { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }
    }
}
