﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General.Dto
{
    [AutoMapFrom(typeof(DataTable))]
    public class DataTableListDto : FullAuditedEntityDto<int>
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
