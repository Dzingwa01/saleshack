﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.General.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class ActivityAppService : AsyncCrudAppService<Activity, ActivityListDto, Guid, PagedAndSortedResultRequestDto, CreateActivityInput, ActivityListDto>, IActivityAppService
    {

        private readonly IActivityManager _activityManager;
        private readonly IRepository<Activity, Guid> _activityRepository;
        public ActivityAppService(IActivityManager activityManager, IRepository<Activity, Guid> activityRepository) : base(activityRepository)
        {
            _activityManager = activityManager;
            _activityRepository = activityRepository;
        }
        public async Task<ListResultDto<ActivityListDto>> GetById(Guid input)
        {
            var activity = await _activityRepository.FirstOrDefaultAsync(input);
            if (activity == null)
            {
                throw new UserFriendlyException("Activity Not Found, Maybe it's Deleted");
            }
            return new ListResultDto<ActivityListDto>(activity.MapTo<List<ActivityListDto>>());
        }
        public async Task<ListResultDto<ActivityListDto>> GetAllActivities()
        {
            var activities = await _activityRepository.GetAll().ToListAsync();
            if (activities == null)
            {
                throw new UserFriendlyException("Activities Not Found, Maybe They're Deleted");
            }
            return new ListResultDto<ActivityListDto>(activities.MapTo<List<ActivityListDto>>());
        }

        public async Task<ActivityDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var activity = await _activityRepository.FirstOrDefaultAsync(input.Id);
            if (activity == null)
            {
                throw new UserFriendlyException("Activity Not Found, Maybe it's Deleted");
            }
            return activity.MapTo<ActivityDetailOutput>();
        }
    }
}
