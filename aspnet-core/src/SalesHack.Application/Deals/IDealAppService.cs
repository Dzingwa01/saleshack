﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Deals.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Deals
{
    public interface IDealAppService : IApplicationService
    {
        Task<DealDetailOutput> GetDetail(EntityDto<Guid> input);
        Task<DealDetailOutput> GetById(Guid input);
        Task Edit(DealDetailOutput input);
        Task<ListResultDto<DealListDto>> GetByStage(Guid input);
    }
}
