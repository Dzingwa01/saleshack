﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Deals.Dto
{
    [AutoMapTo(typeof(Deal))]
    public class CreateDealInput
    {
        public long OwnerId { get; set; }
        public Guid CompanyId { get; set; }
        public virtual string Name { get; set; }
        public virtual double? ExpectedAmount { get; set; }
        public virtual double? Amount { get; set; }
        public int Type { get; set; }
        public Guid ContactId { get; set; }
        public virtual Guid PipelineStageId { get; set; }
        public virtual DateTime? ExpectedCloseDate { get; set; }
        public virtual DateTime? CloseDate { get; set; }
    }
}
