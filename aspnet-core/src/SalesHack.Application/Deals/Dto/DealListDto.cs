﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Deals.Dto
{
    [AutoMapFrom(typeof(Deal))]
    public class DealListDto : FullAuditedEntityDto<Guid>
    {
        public virtual string Name { get; set; }
        public virtual double? ExpectedAmount { get; set; }
        public virtual double? Amount { get; set; }
        public virtual DateTime? ExpectedCloseDate { get; set; }
        public virtual DateTime? CloseDate { get; set; }
        public Guid PipelineStageId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ContactId { get; set; }
        public Contact Contact { get; set; }
        public int Type { get; set; }
        public int TenantId { get; set; }

    }
}
