﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using SalesHack.Authorization.Users;
using SalesHack.Campanies.Dto;
using SalesHack.Companies;
using SalesHack.Companies.Dto;
using SalesHack.Deals.Dto;
using SalesHack.General;
using SalesHack.General.Dto;
using SalesHack.Users.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Deals
{
    public class DealAppService : AsyncCrudAppService<Deal, DealListDto, Guid, PagedAndSortedResultRequestDto, CreateDealInput, DealListDto>, IDealAppService
    {
        private readonly IRepository<Deal, Guid> _dealRepository;
        private readonly IDealManager _dealManager;
        private readonly IRepository<Stage, Guid> _stageRepository;
        private readonly IRepository<Company, Guid> _companyRepository;
        private readonly IRepository<Contact, Guid> _contactRepository;
        private readonly IRepository<Activity, Guid> _activityRepository;
        private readonly IRepository<User, long> _userRepository;

        public DealAppService(IRepository<Deal, Guid> dealRepository,
            IDealManager dealManager, IRepository<Stage, Guid> stageRepository,
            IRepository<Company, Guid> companyRepository,
            IRepository<Contact, Guid> contactRepository,
            IRepository<Activity, Guid> activityRepository,
            IRepository<User, long> userRepository) : base(dealRepository)
        {
            _dealManager = dealManager;
            _dealRepository = dealRepository;
            _stageRepository = stageRepository;
            _companyRepository = companyRepository;
            _contactRepository = contactRepository;
            _activityRepository = activityRepository;
            _userRepository = userRepository;
        }

        public  Task<DealListDto> Create(CreateDealInput input)
        {
            //return base.Create(input);
            return null;
        }
        public async Task<DealDetailOutput> GetById(Guid input)
        {
            var deal = await _dealRepository.GetAll()
                .Where(x => x.Id == input)
                .Include(e => e.Contact)
                .FirstOrDefaultAsync();
            if (deal == null)
            {
                throw new UserFriendlyException("Deal Not Found Maybe it's Deleted");
            }
            return deal.MapTo<DealDetailOutput>();
        }

        public async Task<DealDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var deal = await _dealRepository
                .GetAll()
                .Where(e => e.Id == input.Id).
                Include(e => e.Contact)
                .FirstOrDefaultAsync();

            if (deal == null)
            {
                throw new UserFriendlyException("Could not find the Deal, Maybe it's Deleted.");
            }

            return deal.MapTo<DealDetailOutput>();
        }

        public async Task Edit(DealDetailOutput input)
        {
            var deal = await _dealRepository.GetAsync(input.Id);
            EditDeal(input, deal);
            try
            {
                await _dealRepository.UpdateAsync(deal);
            }
            catch (Exception e)
            {

            }
        }
        private static void EditDeal(DealDetailOutput input, Deal deal)
        {
            deal.Name = input.Name ?? deal.Name;
            deal.ExpectedAmount = input.ExpectedAmount == null ? deal.ExpectedAmount : input.ExpectedAmount;
            deal.Amount = input.Amount == null ? deal.Amount : input.Amount;
            deal.ExpectedCloseDate = input.ExpectedCloseDate == null ? deal.ExpectedCloseDate : input.ExpectedCloseDate;
            deal.CloseDate = input.CloseDate == null ? deal.CloseDate : input.CloseDate;
            deal.PipelineStageId = input.PipelineStageId == null ? deal.PipelineStageId : input.PipelineStageId;
            deal.OwnerId = input.OwnerId;
            deal.Type = input.Type;
            deal.ContactId = input.ContactId;
        }

        public async Task<ListResultDto<Stage>> GetStages()
        {
            var stages = await _stageRepository.GetAll()
                 .Include(e => e.Deals)
                 .OrderBy(p => p.Position)
                 .ToListAsync();

            return new ListResultDto<Stage>(stages.MapTo<List<Stage>>());
        }
        public async Task<Stage> GetStageById(Guid Id)
        {
            var stage = await _stageRepository.FirstOrDefaultAsync(Id);

            return stage;
        }
        public async Task<ListResultDto<DealListDto>> GetDeals(Guid input)
        {
            var deals = await _dealRepository.GetAll().Where(p => p.PipelineStageId.Equals(input)).ToListAsync();
            if (deals == null)
            {
                throw new UserFriendlyException("Deals Not Found Maybe They're Deleted");
            }
            return new ListResultDto<DealListDto>(deals.MapTo<List<DealListDto>>());
        }

        public async Task UpdateStage(Guid dealId, Guid stageId)
        {
            var deal = await _dealRepository.FirstOrDefaultAsync(x => x.Id == dealId);
            deal.PipelineStageId = stageId;
            await _dealRepository.UpdateAsync(deal);
        }
        public async Task<ListResultDto<UserDto>> GetUsers()
        {
            var users = await _userRepository.GetAll().Where(x => x.TenantId == AbpSession.TenantId).ToListAsync();
            if (users == null)
            {
                throw new UserFriendlyException("Users Not Found Maybe They're Deleted");
            }
            return new ListResultDto<UserDto>(users.MapTo<List<UserDto>>());
        }

        public async Task<ListResultDto<CompanyListDto>> GetCompanies()
        {
            var companies = await _companyRepository.GetAll().Where(p => p.TenantId.Equals(AbpSession.TenantId)).ToListAsync();
            if (companies == null)
            {
                throw new UserFriendlyException("Companies Not Found Maybe They're Deleted");
            }
            return new ListResultDto<CompanyListDto>(companies.MapTo<List<CompanyListDto>>());
        }

        public async Task<ListResultDto<ContactListDto>> GetContacts(Guid companyId)
        {
            var contacts = await _contactRepository.GetAll().Where(p => p.CompanyId.Equals(companyId)).ToListAsync();
            if (contacts == null)
            {
                throw new UserFriendlyException("Companies Not Found Maybe They're Deleted");
            }
            return new ListResultDto<ContactListDto>(contacts.MapTo<List<ContactListDto>>());
        }

        public async Task<ListResultDto<ActivityListDto>> GetActivities(string TargetType, Guid targetId)
        {
            var activities = await _activityRepository.GetAll()
                .Where(p => p.TargetType.Equals(TargetType) && p.TargetId == targetId).ToListAsync();
            if (activities == null)
            {
                throw new UserFriendlyException("Activities Not Found Maybe They're Deleted");
            }
            return new ListResultDto<ActivityListDto>(activities.MapTo<List<ActivityListDto>>());
        }

        public async Task<ListResultDto<DealListDto>> GetByStage(Guid input)
        {
            var deals = await _dealRepository.GetAll().Where(x => x.PipelineStageId.Equals(input)).ToListAsync();
            return new ListResultDto<DealListDto>(deals.MapTo<List<DealListDto>>());
        }
    }
}
