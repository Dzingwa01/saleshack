﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SalesHack.Sessions.Dto;

namespace SalesHack.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
