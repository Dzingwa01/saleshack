using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Zero.Configuration;
using SalesHack.Authorization.Accounts.Dto;
using SalesHack.Authorization.Users;
using SalesHack.Users.Dto;
using SalesHack.Users;
using System;
using SalesHack.MultiTenancy;
using Abp.UI;
using Abp.Domain.Repositories;
using System.Linq;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;

namespace SalesHack.Authorization.Accounts
{
    public class AccountAppService : SalesHackServiceBase, IAccountAppService
    {
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly IRepository<UserServiceOffering, Guid> _userServiceOfferingRepository;
        private readonly IRepository<UserTargetIndustry, Guid> _userTargetIndustryRepository;
        private readonly IUserServiceOfferingManager _userServiceOfferingManager;
        private readonly IUserTargetIndustryManager _userTargetIndustryManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IHostingEnvironment _environment;
        private readonly IRepository<User, long> _userRepository;
        private readonly IUserAppService _userAppService;
        private readonly UserManager _userManager;
        private readonly TenantManager _tenantManager;
        public AccountAppService(UserRegistrationManager userRegistrationManager,
            IRepository<UserServiceOffering, Guid> userServiceOfferingRepository,
            IRepository<UserTargetIndustry, Guid> userTargetIndustryRepository,
            IUserServiceOfferingManager userServiceOfferingManager,
            IPasswordHasher<User> passwordHasher,
            IUserTargetIndustryManager userTargetIndustryManager,
            IRepository<User, long> userRepository,
            IHostingEnvironment environment,
            IUserAppService userAppService, UserManager userManager, TenantManager tenantManager)
        {
            _userRegistrationManager = userRegistrationManager;
            _userServiceOfferingRepository = userServiceOfferingRepository;
            _userTargetIndustryRepository = userTargetIndustryRepository;
            _userServiceOfferingManager = userServiceOfferingManager;
            _userTargetIndustryManager = userTargetIndustryManager;
            _passwordHasher = passwordHasher;
            _environment = environment;
            _userRepository = userRepository;
            _userAppService = userAppService;
            _userManager = userManager;
            _tenantManager = tenantManager;
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
            );

            var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }
        public async Task<UserDto> GetUser(long id, string code)
        {

            var user = await _userManager.GetUserByIdAsync(id);
            UserDto input = new UserDto();

            if (user.EmailConfirmationCode == code)
            {
                var newUser = ObjectMapper.Map(user, input);

                return ObjectMapper.Map(user, input);
            }

            return null;
        }
        public async Task Confirmation(UserDto input)
        {
            //get User

            // var user = await _userRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            var user = await _userManager.GetUserByIdAsync(input.Id);
            //var test = "CfDJ8M7IHHxGXQxDkv1w5eGILkU0WJwlYaeNNIc5qCCcbcTUxYErXV3ahxdfSfNwZMFmOh/ekvmGtl5aX5Ha560COeM7ylUOG9sYc+8uK9TOCLQ3ZqIxJjU/EEXMjVJZuur5Mn19VWh6DHP97afeXG/Vzq3UKEeYaN6AMIGpdKbzD3olAhS+Muhfc0IPyCm3dWButo6N8hljec3dEAAL9swgMCg=" == "CfDJ8M7IHHxGXQxDkv1w5eGILkU0WJwlYaeNNIc5qCCcbcTUxYErXV3ahxdfSfNwZMFmOh/ekvmGtl5aX5Ha560COeM7ylUOG9sYc+8uK9TOCLQ3ZqIxJjU/EEXMjVJZuur5Mn19VWh6DHP97afeXG/Vzq3UKEeYaN6AMIGpdKbzD3olAhS+Muhfc0IPyCm3dWButo6N8hljec3dEAAL9swgMCg=";
            //var userL = user.EmailConfirmationCode.Length;
            //var inputL = input.EmailConfirmationCode.Length;
            //var test2 = user.EmailConfirmationCode.ToLower().Trim() == input.EmailConfirmationCode.ToLower().Trim();

            if (user != null)
            {
                var userTenant = new Tenant();
                userTenant = await TenantManager.FindByIdAsync(user.TenantId.Value);

                userTenant.IsActive = true;
                userTenant.BeeStatus = input.BeeStatus;
                userTenant.CompanySize = input.CompanySize;
                userTenant.Industry = input.Industry;
                await _tenantManager.UpdateAsync(userTenant);

                user.IsEmailConfirmed = true;

                // SMALL DEAL CALCULATION
                user.JobTitle = input.JobTitle;
                user.TargetedMonthlyTurnoverSD = input.TargetedMonthlyTurnoverSD;
                user.AverageDealSizeSD = input.TargetedMonthlyTurnoverSD / 4;
                user.LeadsTargetSD = (int)(user.TargetedMonthlyTurnoverSD / user.AverageDealSizeSD);
                user.ProjectedTurnAroundTimeSD = (int)(user.LeadsTargetSD / 4.0);
                user.EmailsTargetSD = (int)Math.Round((user.AverageDealSizeSD * 0.001) / user.ProjectedTurnAroundTimeSD);
                user.LeadsTargetSD = (int)((user.EmailsTargetSD * 0.1) + user.EmailsTargetSD); // Leads / Prospects
                user.CallsTargetSD = (int)Math.Round(user.EmailsTargetSD * 0.85);
                user.NeedsAnalysisTargetSD = (int)Math.Round(user.CallsTargetSD * 0.75);
                user.MeetingsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.ProposalsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.WinsTargetSD = (int)(user.ProjectedTurnAroundTimeSD);
                user.FollowupSD = (int)Math.Round(user.ProposalsTargetSD * 0.5);
                user.ProjectedTurnAroundTimeSD = (int)(user.ProjectedTurnAroundTimeSD);

                // MEDIUM DEAL CALCULATION
                user.TargetedMonthlyTurnoverMD = input.TargetedMonthlyTurnoverMD;
                user.AverageDealSizeMD = input.TargetedMonthlyTurnoverMD / 4;

                user.LeadsTargetMD = (int)(user.TargetedMonthlyTurnoverMD / user.AverageDealSizeMD);
                user.ProjectedTurnAroundTimeMD = (int)(user.LeadsTargetMD / 4.0);
                user.EmailsTargetMD = (int)Math.Round((user.AverageDealSizeMD * 0.001) / user.ProjectedTurnAroundTimeMD);
                user.LeadsTargetMD = (int)((user.EmailsTargetMD * 0.1) + user.EmailsTargetMD); //Leads/Prospects
                user.CallsTargetMD = (int)Math.Round(user.EmailsTargetMD * 0.85);
                user.NeedsAnalysisTargetMD = (int)Math.Round(user.CallsTargetMD * 0.75);
                user.MeetingsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.ProposalsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.WinsTargetMD = (int)(user.ProjectedTurnAroundTimeMD);
                user.FollowupMD = (int)Math.Round(user.ProposalsTargetMD * 0.5);

                //// BIG DEAL CALCULATION
                user.TargetedMonthlyTurnoverBD = input.TargetedMonthlyTurnoverBD;
                user.AverageDealSizeBD = input.TargetedMonthlyTurnoverBD / 4;

                user.LeadsTargetBD = (int)(user.TargetedMonthlyTurnoverBD / user.AverageDealSizeBD);
                user.ProjectedTurnAroundTimeBD = (int)(user.LeadsTargetBD / 4.0);

                user.EmailsTargetBD = (int)Math.Round((user.AverageDealSizeBD * 0.001) / user.ProjectedTurnAroundTimeBD);
                user.LeadsTargetBD = (int)((user.EmailsTargetBD * 0.1) + user.EmailsTargetBD); //Leads/Prospects
                user.CallsTargetBD = (int)Math.Round(user.EmailsTargetBD * 0.85);
                user.NeedsAnalysisTargetBD = (int)Math.Round(user.CallsTargetBD * 0.75);
                user.MeetingsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.ProposalsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.WinsTargetBD = (int)(user.ProjectedTurnAroundTimeBD);
                user.FollowupBD = (int)Math.Round(user.ProposalsTargetBD * 0.5);

                user.IsActive = true;
                try
                {
                    foreach (int ServiceId in input.Services)
                    {
                        if (await _userServiceOfferingRepository.GetAll().Where(x => (x.ServiceId.Equals(ServiceId)) && (x.UserId.Equals(user.Id))).FirstOrDefaultAsync() == null)
                        {
                            await _userServiceOfferingManager.CreateAsync(new UserServiceOffering
                            {
                                TenantId = AbpSession.GetTenantId(),
                                UserId = user.Id,
                                ServiceId = ServiceId
                            });
                        }
                    }
                    foreach (int industryId in input.TargetIndustries)
                    {
                        if (await _userTargetIndustryRepository.GetAll().Where(x => (x.IndustryId.Equals(industryId)) && (x.UserId.Equals(user.Id))).FirstOrDefaultAsync() == null)
                        {
                            await _userTargetIndustryManager.CreateAsync(new UserTargetIndustry
                            {
                                TenantId = AbpSession.GetTenantId(),
                                UserId = user.Id,
                                IndustryId = industryId
                            });
                        }
                    }
                    await _userManager.UpdateAsync(user);
                }
                catch (Exception ex)
                {

                }

            }

        }

        public async Task<string> ReprojectSmallDeals(UserDto input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            if (user != null)
            {
                user.JobTitle = input.JobTitle;
                user.TargetedMonthlyTurnoverSD = input.TargetedMonthlyTurnoverSD;
                user.AverageDealSizeSD = input.TargetedMonthlyTurnoverSD / 4;
                user.LeadsTargetSD = (int)(user.TargetedMonthlyTurnoverSD / user.AverageDealSizeSD);
                user.ProjectedTurnAroundTimeSD = (int)(user.LeadsTargetSD / 4.0);
                user.EmailsTargetSD = (int)Math.Round((user.AverageDealSizeSD * 0.001) / user.ProjectedTurnAroundTimeSD);
                user.LeadsTargetSD = (int)((user.EmailsTargetSD * 0.1) + user.EmailsTargetSD); // Leads / Prospects
                user.CallsTargetSD = (int)Math.Round(user.EmailsTargetSD * 0.85);
                user.NeedsAnalysisTargetSD = (int)Math.Round(user.CallsTargetSD * 0.75);
                user.MeetingsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.ProposalsTargetSD = (int)Math.Round(user.NeedsAnalysisTargetSD * 0.6);
                user.WinsTargetSD = (int)(user.ProjectedTurnAroundTimeSD);
                user.FollowupSD = (int)Math.Round(user.ProposalsTargetSD * 0.5);
                user.ProjectedTurnAroundTimeSD = (int)(user.ProjectedTurnAroundTimeSD);
                user.LastModifierUserId = null;
                return null;
            }
            else
            {
                throw new UserFriendlyException("The Current Session has expired, please try to log in again");
            }
        }

        public async Task<string> ReprojectMediumDeals(UserDto input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            if (user != null)
            {
                user.TargetedMonthlyTurnoverMD = input.TargetedMonthlyTurnoverMD;
                user.AverageDealSizeMD = input.TargetedMonthlyTurnoverMD / 4;

                user.LeadsTargetMD = (int)(user.TargetedMonthlyTurnoverMD / user.AverageDealSizeMD);
                user.ProjectedTurnAroundTimeMD = (int)(user.LeadsTargetMD / 4.0);
                user.EmailsTargetMD = (int)Math.Round((user.AverageDealSizeMD * 0.001) / user.ProjectedTurnAroundTimeMD);
                user.LeadsTargetMD = (int)((user.EmailsTargetMD * 0.1) + user.EmailsTargetMD); //Leads/Prospects
                user.CallsTargetMD = (int)Math.Round(user.EmailsTargetMD * 0.85);
                user.NeedsAnalysisTargetMD = (int)Math.Round(user.CallsTargetMD * 0.75);
                user.MeetingsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.ProposalsTargetMD = (int)Math.Round(user.NeedsAnalysisTargetMD * 0.6);
                user.WinsTargetMD = (int)(user.ProjectedTurnAroundTimeMD);
                user.FollowupMD = (int)Math.Round(user.ProposalsTargetMD * 0.5);
                user.LastModifierUserId = null;
                return null;
            }
            else
            {
                throw new UserFriendlyException("The Current Session has expired, please try to log in again");
            }
        }

        public async Task<string> ReprojectBigDeals(UserDto input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            if (user != null)
            {
                user.TargetedMonthlyTurnoverBD = input.TargetedMonthlyTurnoverBD;
                user.AverageDealSizeBD = input.TargetedMonthlyTurnoverBD / 4;

                user.LeadsTargetBD = (int)(user.TargetedMonthlyTurnoverBD / user.AverageDealSizeBD);
                user.ProjectedTurnAroundTimeBD = (int)(user.LeadsTargetBD / 4.0);

                user.EmailsTargetBD = (int)Math.Round((user.AverageDealSizeBD * 0.001) / user.ProjectedTurnAroundTimeBD);
                user.LeadsTargetBD = (int)((user.EmailsTargetBD * 0.1) + user.EmailsTargetBD); //Leads/Prospects
                user.CallsTargetBD = (int)Math.Round(user.EmailsTargetBD * 0.85);
                user.NeedsAnalysisTargetBD = (int)Math.Round(user.CallsTargetBD * 0.75);
                user.MeetingsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.ProposalsTargetBD = (int)Math.Round(user.NeedsAnalysisTargetBD * 0.6);
                user.WinsTargetBD = (int)(user.ProjectedTurnAroundTimeBD);
                user.FollowupBD = (int)Math.Round(user.ProposalsTargetBD * 0.5);
                user.LastModifierUserId = null;
                return null;
            }
            else
            {
                throw new UserFriendlyException("The Current Session has expired, please try to log in again");
            }
        }

        public async Task ForgotPassword(string emailAddress)
        {
            var user = await _userManager.FindByEmailAsync(emailAddress);
            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
            {
                throw new UserFriendlyException("User With Account " + emailAddress + "Not Found On The System");
            }

            string body = string.Empty;
            var path = Path.Combine(_environment.WebRootPath, "Templates/Email/forgot-password.html");
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            string link = "http://localhost:4200/account/reset-password?code=" + code + "&id=" + user.Id;
            body = body.Replace("#Link", link);
            user.PasswordResetCode = code;
            Emailer.Send(to: emailAddress, subject: "Password Reset", body: body, isBodyHtml: true);
        }

        public async Task PasswordReset(UserDto input, string newPassword)
        {
            var user = await _userManager.FindByIdAsync(input.Id.ToString());
            if (user != null && user.PasswordResetCode.Equals(input.PasswordResetToken))
            {
                user.Password = _passwordHasher.HashPassword(user, newPassword);
                await _userManager.UpdateAsync(user);
            }
            else
            {
                throw new UserFriendlyException("Your Token Has Expired");
            }

        }
    }
}
