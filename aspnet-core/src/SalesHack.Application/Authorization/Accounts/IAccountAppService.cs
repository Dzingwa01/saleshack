﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SalesHack.Authorization.Accounts.Dto;
using SalesHack.Users.Dto;

namespace SalesHack.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
        Task<UserDto> GetUser(long id, string code);
        Task Confirmation(UserDto input);
        Task<string> ReprojectSmallDeals(UserDto input);
        Task<string> ReprojectMediumDeals(UserDto input);
        Task<string> ReprojectBigDeals(UserDto input);
        Task ForgotPassword(string emailAddress);
        Task PasswordReset(UserDto input, string newPassword);

    }
}
