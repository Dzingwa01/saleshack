﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Authorization.Permissions.Dto;

namespace SalesHack.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
