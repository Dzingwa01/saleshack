﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using SalesHack.Companies.Dto;
using SalesHack.Campanies.Dto;
using SalesHack.Companies;

namespace SalesHack.Campanies
{
    public interface ICompanyAppServices : IApplicationService
    {
        Task<CompanyDetailOutput> GetDetail(EntityDto<Guid> input);
        // Task<ListResultDto<CompanyListDto>> GetAll(GetAllCompaniesInput input);
        //Task Create(CreateCompanyInput input);
        //Task Delete(EntityDto<Guid> input);
        Task<ListResultDto<CompanyListDto>> GetById(Guid input);
        Task EditCompany(CompanyDetailOutput input);
    }
}
