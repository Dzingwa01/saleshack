﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Companies.Dto
{
    [AutoMapFrom(typeof(Contact))]
    public class ContactListDto : FullAuditedEntityDto<Guid>, IHasCreationTime
    {
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public long AssignedId { get; set; }
        public User AssingedUser { get; set; }
        public virtual string Source { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string Department { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Industry { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }

        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public virtual string Instagram { get; set; }
        public virtual string Twitter { get; set; }
        public virtual Company Company { get; set; }

    }
}
