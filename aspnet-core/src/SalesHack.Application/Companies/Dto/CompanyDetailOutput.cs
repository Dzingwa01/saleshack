﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SalesHack.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesHack.Companies.Dto
{
    [AutoMapFrom(typeof(Company))]
    public class CompanyDetailOutput: EntityDto<Guid>
    {
        //[Key]
        //public String Id { get; set;}
        public String Name { get; set; }
        public String Email { get; set; }
        public virtual User AssingedUser { get; protected set; }
        public long AssignedId { get; protected set; }
        public String Size { get; set; }
        public String Phase { get; set; }
        public Decimal Turnover { get; set; }
        public int YearInBusiness { get; set; }
        public String WebUrl { get; set; }
        public String Telephone { get; set; }
        public String Industry { get; set; }
        public String BusinessType { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String Province { get; set; }
        public String Country { get; set; }
        public String BeeStatus { get; set; }
        public String Source { get; set; }
        public String PostalCode { get; set; }
        public String Stage { get; set; }
        public String SpecialEvents { get; set; }
        public String Facebook { get; set; }
        public String LinkedIn { get; set; }
        public String GooglePlus { get; set; }
        public String Instagram { get; set; }
        public String Twitter { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual int ContactId { get; set; }
    }
}
