﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Companies.Dto
{
    public class CreateContactProspectInput
    {
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Source { get; set; }
        public String JobTitle { get; set; }
        public String Department { get; set; }
        public String Telephone { get; set; }
        public String Cellphone { get; set; }
        public String Email { get; set; }
        public String Status { get; set; }
        public String Address { get; set; }
        public String Industry { get; set; }
        public String City { get; set; }
        public String Province { get; set; }
        public String SpecialEvents { get; set; }
        public  String Facebook { get; set; }
        public String LinkedIn { get; set; }
        public String GooglePlus { get; set; }
        public String Instagram { get; set; }
        public String Twitter { get; set; }
        public Guid? CompanyId { get; protected set; }
    }
}
