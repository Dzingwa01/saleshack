﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SalesHack.Authorization.Users;
using SalesHack.Companies;
using SalesHack.Prospects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies.Dto
{
    [AutoMapTo(typeof(Contact))]
    public class CreateContactInput : EntityDto<Guid>
    {

        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual User AssingedUser { get; set; }
        public long AssignedId { get; set; }
        public virtual string Industry { get; set; }
        public virtual string Source { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string Department { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }

        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public virtual string Instagram { get; set; }
        public virtual string Twitter { get; set; }
        [ForeignKey(nameof(CompanyId))]
        public virtual Company ContactCompany { get; set; }
        public Guid? CompanyId { get; set; }
    }
}
