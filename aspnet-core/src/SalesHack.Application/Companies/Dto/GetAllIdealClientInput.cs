﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Companies.Dto
{
    public class GetAllIdealClientInput
    {
        public List<string> Industries { get; set; }
        public List<string> Cities { get; set; }
        public List<string> BeeStatuses { get; set; }
        public List<string> CompanySizes { get; set; }
        public List<string> EstBracket { get; set; }
    }
}
