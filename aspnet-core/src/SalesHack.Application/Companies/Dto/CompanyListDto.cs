﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Campanies.Dto
{
    [AutoMapFrom(typeof(Company))]
    public class CompanyListDto : EntityDto<Guid>, IHasCreationTime
    {
        //this class before everything (reference IHasCreationTime)
        public DateTime CreationTime { get; set; }
        public virtual int TenantId { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public User AssingedUser { get; set; }
        public long AssignedId { get; protected set; }
        public String Size { get; set; }
        public String Phase { get; set; }
        public decimal Turnover { get; set; }
        public int YearInBusiness { get; set; }
        public String WebUrl { get; set; }
        public String Telephone { get; set; }
        public String Industry { get; set; }
        public String BusinessType { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String Province { get; set; }
        public String Country { get; set; }
        public String BeeStatus { get; set; }
        public String Source { get; set; }
        public String PostalCode { get; set; }
        public String Stage { get; set; }
        public String SpecialEvents { get; set; }
        public String Facebook { get; set; }
        public String LinkedIn { get; set; }
        public String GooglePlus { get; set; }
        public String Instagram { get; set; }
        public String Twitter { get; set; }
        //public virtual ICollection<Contact> Contacts { get; set; }


    }
}
