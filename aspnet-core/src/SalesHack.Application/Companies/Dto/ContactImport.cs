﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies.Dto
{
    public class ContactImport
    {
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Source { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string Department { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Industry { get; set; }
        public string CompanyName { get; set; }
        public string BeeStatus { get; set; }
        public string CompanySize { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }

    }
}
