﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Companies.Dto
{
    public class IdealClientListDto : FullAuditedEntityDto<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }
    }
}
