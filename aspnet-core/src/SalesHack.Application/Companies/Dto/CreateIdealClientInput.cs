﻿using Abp.AutoMapper;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Companies.Dto
{
    [AutoMapTo(typeof(IdealClient))]
    public class CreateIdealClientInput
    {
        public List<string> Value { get; set; }
    }
}
