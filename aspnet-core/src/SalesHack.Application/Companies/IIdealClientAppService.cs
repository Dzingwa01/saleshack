﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SalesHack.Companies.Dto;
using SalesHack.General.Dto;
using SalesHack.Prospects.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public interface IIdealClientAppService : IApplicationService
    {
        Task<ListResultDto<CompanyProspectListDto>> GetAllCompanies(GetAllIdealClientInput input);
        Task CreateIdealClient(CreateIdealClientInput input);
        Task<ListResultDto<DataTableListDto>> GetDefaultValues(string type);
        Task<ListResultDto<CompanyProspectListDto>> GetDashBoardClients(long userId);
        Task Remove(string value);

    }
}
