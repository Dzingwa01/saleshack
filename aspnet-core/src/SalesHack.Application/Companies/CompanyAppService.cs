﻿using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using SalesHack;
using SalesHack.Companies;
using Abp.Authorization;
using System;
using Abp.Runtime.Session;
using Abp.AutoMapper;
using SalesHack.Companies.Dto; 
using SalesHack.Campanies.Dto;
using Abp.UI;
using SalesHack.Prospects;
using Abp.Application.Services;
using Abp.Extensions;

namespace SalesHack.Campanies
{
    [AbpAuthorize]
    public class CompanyAppService : AsyncCrudAppService<Company, CompanyListDto, Guid, PagedAndSortedResultRequestDto, CreateCompanyInput, CompanyListDto>, ICompanyAppServices
    {
        private readonly ICompanyManager _companyManager;
        private readonly IRepository<Company, Guid> _companyRepository;
 
        private readonly ICompanyProspectManager _CompanyProspectManager;
        //private readonly IRepository<Company, Guid> _companyRepository;
        private readonly IRepository<CompanyProspect, Guid> _companyProspectRepository;

        public CompanyAppService(IRepository<Company, Guid> companyRepository, ICompanyManager _companyManager, ICompanyProspectManager _CompanyProspectManager, IRepository<CompanyProspect, Guid> _companyProspectRepository) : base(companyRepository)
        {
            _companyRepository = companyRepository;
            this._companyManager = _companyManager;
            this._CompanyProspectManager = _CompanyProspectManager;
            this._companyProspectRepository = _companyProspectRepository;
        }

        public  async Task<CompanyListDto> Create(CreateCompanyInput input)
        {
            try
            {
                var id = AbpSession.GetTenantId();
                var company = Company.Create(id, input.Name, input.Email, AbpSession.GetUserId(), input.Size, input.Phase, input.Turnover, input.YearInBusiness, input.WebUrl, input.Telephone, input.Industry, input.BusinessType, input.Address,
                   input.City, input.Province, input.Country, input.BeeStatus, input.PostalCode, input.Source, input.Stage, input.SpecialEvents, input.Facebook, input.LinkedIn, input.GooglePlus, input.Instagram, input.Twitter);
                var companies = _companyRepository.FirstOrDefault(x => (x.Email == company.Email) || (x.Name == company.Name));
                if (companies == null)
                {
                    await _companyManager.CreateAsync(company);
                }
                else
                {
                    throw new UserFriendlyException("Company Already Exists");
                }
                await _companyManager.CreateAsync(company);
                var companyProspect = _companyProspectRepository.FirstOrDefault(x => x.Email == company.Email);

                if (companyProspect == null)
                {
                    await _CompanyProspectManager.CreateAsync(new CompanyProspect
                    {
                        Name = company.Name,
                        Email = company.Email,
                        Size = company.Size,
                        Phase = company.Phase,
                        Turnover = company.Turnover,
                        YearInBusiness = company.YearInBusiness,
                        WebUrl = company.WebUrl,
                        Telephone = company.Telephone,
                        Industry = company.Industry,
                        BusinessType = company.BusinessType,
                        Address = company.Address,
                        City = company.City,
                        Province = company.Province,
                        Country = company.Country,
                        BeeStatus = company.BeeStatus,
                        PostalCode = company.PostalCode,
                        Source = company.Source,
                        Stage = company.Stage,
                        SpecialEvents = company.SpecialEvents,
                        Facebook = company.Facebook,
                        LinkedIn = company.LinkedIn,
                        GooglePlus = company.GooglePlus,
                        Instagram = company.Instagram,
                        Twitter = company.Twitter
                    });
                }
                else
                {
                    companyProspect.Name = company.Name == null ? companyProspect.Name : company.Name;
                    companyProspect.Email = company.Email == null ? companyProspect.Email : company.Email;
                    companyProspect.Size = company.Size == null ? companyProspect.Size : company.Size;
                    companyProspect.Phase = company.Phase == null ? companyProspect.Phase : company.Phase;
                    companyProspect.Turnover = company.Turnover == 0 ? companyProspect.Turnover : company.Turnover;
                    companyProspect.YearInBusiness = company.YearInBusiness == 0 ? companyProspect.YearInBusiness : company.YearInBusiness;
                    companyProspect.WebUrl = company.WebUrl == null ? companyProspect.WebUrl : company.WebUrl;
                    companyProspect.Telephone = company.Telephone == null ? companyProspect.Telephone : company.Telephone;
                    companyProspect.Industry = company.Industry == null ? companyProspect.Industry : company.Industry;
                    companyProspect.BusinessType = company.BusinessType == null ? companyProspect.BusinessType : company.BusinessType;
                    companyProspect.Address = company.Address == null ? companyProspect.Address : company.Address;
                    companyProspect.City = company.City == null ? companyProspect.City : company.City;
                    companyProspect.Province = company.Province == null ? companyProspect.Province : company.Province;
                    companyProspect.Country = company.Country == null ? companyProspect.Country : company.Country;
                    companyProspect.BeeStatus = company.BeeStatus == null ? companyProspect.BeeStatus : company.BeeStatus;
                    companyProspect.PostalCode = company.PostalCode == null ? companyProspect.PostalCode : company.PostalCode;
                    companyProspect.Source = company.Source == null ? companyProspect.Source : company.Source;
                    companyProspect.Stage = company.Stage == null ? companyProspect.Stage : company.Stage;
                    companyProspect.SpecialEvents = company.SpecialEvents == null ? companyProspect.SpecialEvents : company.SpecialEvents;
                    companyProspect.Facebook = company.Facebook == null ? companyProspect.Facebook : company.Facebook;
                    companyProspect.LinkedIn = company.LinkedIn == null ? companyProspect.LinkedIn : company.LinkedIn;
                    companyProspect.GooglePlus = company.GooglePlus == null ? companyProspect.GooglePlus : company.GooglePlus;
                    companyProspect.Instagram = company.Instagram == null ? companyProspect.Instagram : company.Instagram;
                    companyProspect.Twitter = company.Twitter == null ? companyProspect.Twitter : company.Twitter;

                    await _companyProspectRepository.UpdateAsync(companyProspect);
                }
            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }
            CurrentUnitOfWork.SaveChanges();

            var returnCompany = ObjectMapper.Map<Company>(input);
            return MapToEntityDto(returnCompany);
        }

        public async Task<ListResultDto<CompanyListDto>> GetAllCompanies(GetAllCompaniesInput input)
        {

            var companies = await _companyRepository
            .GetAll()
            .Include(e => e.Contacts).Include(e => e.AssingedUser)
            //.WhereIf(!input.IncludeCanceledEvents, e => !e.IsCancelled)
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<CompanyListDto>(companies.MapTo<List<CompanyListDto>>());
        }
        public async Task<ListResultDto<CompanyListDto>> GetById(Guid input)
        {

            var companies = await _companyRepository
            .GetAll().Where(x => x.Id == input)
            .Include(e => e.Contacts).Include(e => e.AssingedUser)
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<CompanyListDto>(companies.MapTo<List<CompanyListDto>>());
        }

        public async Task<CompanyDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var company = await _companyRepository
                .GetAll()
                .Include(e => e.Contacts)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();


            if (company == null)
            {
                throw new UserFriendlyException("Could not find the company, maybe it's deleted.");
            }

            return company.MapTo<CompanyDetailOutput>();
        }
        //public async Task Delete(EntityDto<Guid> input)
        //{
        //    //CheckDeletePermission();
        //    await _companyRepository.DeleteAsync(input.Id);
        //}
        public async Task EditCompany(CompanyDetailOutput input)
        {
            var company = await _companyRepository.GetAsync(input.Id);
            ////company. = input.Name;
            company.Name = input.Name;

            //TenantId = input.
            NewMethod(input, company);


            //var company = ObjectMapper.Map<Company>(input);
            try
            {
                await _companyRepository.UpdateAsync(company);
            }
            catch (Exception ex)
            {

            }

        }

        private static void NewMethod(CompanyDetailOutput input, Company company)
        {
            //company.Id = input.Id;
            company.Name = input.Name == null ? company.Name : input.Name;
            company.Email = input.Email == null ? company.Email : input.Email;
            company.Size = input.Size == null ? company.Size : input.Size;
            company.Phase = input.Phase == null ? company.Phase : input.Phase;
            company.Turnover = input.Turnover == 0 ? company.Turnover : input.Turnover;
            company.YearInBusiness = input.YearInBusiness == 0 ? company.YearInBusiness : input.YearInBusiness;
            company.WebUrl = input.WebUrl == null ? company.WebUrl : input.WebUrl;
            company.Telephone = input.Telephone == null ? company.Telephone : input.Telephone;
            company.Industry = input.Industry == null ? company.Industry : input.Industry;
            company.BusinessType = input.BusinessType == null ? company.BusinessType : input.BusinessType;
            company.Address = input.Address == null ? company.Address : input.Address;
            company.City = input.City == null ? company.City : input.City;
            company.Province = input.Province == null ? company.Province : input.Province;
            company.Country = input.Country == null ? company.Country : input.Country;
            company.BeeStatus = input.BeeStatus == null ? company.BeeStatus : input.BeeStatus;
            company.PostalCode = input.PostalCode == null ? company.PostalCode : input.PostalCode;
            company.Source = input.Source == null ? company.Source : input.Source;
            company.Stage = input.Stage == null ? company.Stage : input.Stage;
            company.SpecialEvents = input.SpecialEvents == null ? company.SpecialEvents : input.SpecialEvents;
            company.Facebook = input.Facebook == null ? company.Facebook : input.Facebook;
            company.LinkedIn = input.LinkedIn == null ? company.LinkedIn : input.LinkedIn;
            company.GooglePlus = input.GooglePlus == null ? company.GooglePlus : input.GooglePlus;
            company.Instagram = input.Instagram == null ? company.Instagram : input.Instagram;
            company.Twitter = input.Twitter == null ? company.Twitter : input.Twitter;
        }

        public async Task<ListResultDto<CompanyListDto>> CompanyFilter(string input)
        {
            var companies = await _companyRepository
            .GetAll().WhereIf(
                !input.IsNullOrEmpty(),
                p => p.Name.Contains(input) ||
                     p.Address.Contains(input) ||
                     p.Email.Contains(input) ||
                     p.Industry.Contains(input) ||
                     p.WebUrl.Contains(input) ||
                     p.Telephone.Contains(input))
                        .OrderByDescending(e => e.CreationTime)
            .ToListAsync();
            return new ListResultDto<CompanyListDto>(companies.MapTo<List<CompanyListDto>>());
        }
    }
}
