﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using SalesHack.Prospects;
using SalesHack.Prospects.Dto;
using Abp.Linq.Extensions;
using SalesHack.Companies.Dto;
using SalesHack.General.Dto;
using SalesHack.General;
using Abp.Runtime.Session;
using Abp.Application.Services;
using Abp.UI;
using SalesHack.Authorization.Users;

namespace SalesHack.Companies
{
    public class IdealClientAppService : AsyncCrudAppService<IdealClient, IdealClientListDto, Guid, PagedAndSortedResultRequestDto, CreateIdealClientInput, IdealClientListDto>, IIdealClientAppService
    {
        private readonly IRepository<IdealClient, Guid> _idealClientRepository;
        private readonly IdealClientManger _idealClientManger;
        private readonly IRepository<CompanyProspect, Guid> _companyProspectRepository;
        private readonly IRepository<DataTable, int> _dataTableRepository;
        private readonly IRepository<User, long> _userRepository;

        public IdealClientAppService(
            IRepository<IdealClient, Guid> idealClientRepository,
            IdealClientManger idealClientManger,
            IRepository<CompanyProspect, Guid> companyProspectRepository,
            IRepository<DataTable, int> dataTableRepository,
            IRepository<User, long> userRepository) : base(idealClientRepository)
        {
            _idealClientRepository = idealClientRepository;
            _idealClientManger = idealClientManger;
            _companyProspectRepository = companyProspectRepository;
            _dataTableRepository = dataTableRepository;
            _userRepository = userRepository;
        }

        public async Task CreateIdealClient(CreateIdealClientInput input)
        {
            foreach (string str in input.Value)
            {
                var dataTableValue = await _dataTableRepository.FirstOrDefaultAsync(x => x.Value.Equals(str));
                var exist = await _idealClientRepository.FirstOrDefaultAsync(x => x.DataTableId == dataTableValue.Id);
                if (exist == null)
                {
                    var idealClientInput = IdealClient.Create(AbpSession.GetTenantId(), dataTableValue.Id);
                    await _idealClientManger.CreateAsync(idealClientInput);
                }
            }
        }

        public async Task Remove(string value)
        {
            var dataTableValue = await _dataTableRepository.FirstOrDefaultAsync(x => x.Value.Equals(value));
            if (dataTableValue != null)
            {
                try
                {
                    await _idealClientRepository.DeleteAsync(x => x.DataTableId.Equals(dataTableValue.Id));
                }
                catch (Exception e)
                {

                }

            }

        }

        public async Task<ListResultDto<CompanyProspectListDto>> GetAllCompanies(GetAllIdealClientInput input)
        {
            if (input.Industries != null || input.Cities != null || input.BeeStatuses != null || input.CompanySizes != null)
            {
                try
                {
                    var companies = await _companyProspectRepository.GetAll().Where(p => (input.Industries.Any(x => x.Equals(p.Industry)))
               && (input.Cities.Any(x => x.Equals(p.City)))
               && (input.BeeStatuses.Any(x => x.Equals(p.BeeStatus)))
               && (input.CompanySizes.Any(x => x.Equals(p.Size)))).ToListAsync();
                    return new ListResultDto<CompanyProspectListDto>(companies.MapTo<List<CompanyProspectListDto>>());
                }
                catch (Exception e)
                {
                    throw new UserFriendlyException(e.Message);
                }
            }

            return new ListResultDto<CompanyProspectListDto>();
            // && (input.CompanySizes.Any(x => x.Equals(p.Size))) || (input.EstBracket.Any(x => x.Equals(p.Turnover)))).ToListAsync();

        }
        public async Task<ListResultDto<CompanyProspectListDto>> GetDashBoardClients(long userId)
        {
            var user = await _userRepository.FirstOrDefaultAsync(userId);
            try
            {
                var companies = await _companyProspectRepository.GetAll().Where(p => p.Industry == user.Industry).ToListAsync();
                return new ListResultDto<CompanyProspectListDto>(companies.MapTo<List<CompanyProspectListDto>>());
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
        public async Task<ListResultDto<DataTableListDto>> GetDefaultValues(string type)
        {
            var idealClientInfo = await _idealClientRepository.GetAll().ToListAsync();
            List<DataTable> dataTableInfo = new List<DataTable>();
            foreach (IdealClient idealClient in idealClientInfo)
            {
                var dataTableValue = await _dataTableRepository.GetAsync(idealClient.DataTableId);
                dataTableInfo.Add(dataTableValue);
            }
            var list = dataTableInfo.Where(x => x.Type.Equals(type)).ToList();
            return new ListResultDto<DataTableListDto>(list.MapTo<List<DataTableListDto>>());
        }

    }
}
