﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalesHack.Companies.Dto;
using System.Collections.Generic;

namespace SalesHack.Companies
{
    public interface IContactAppServices : IApplicationService
    {
        Task<ContactDetailOutput> GetDetail(EntityDto<Guid> input);
        //Task<ListResultDto<ContactListDto>> GetAll(GetAllContactInput input);
        //Task Create(CreateContactInput input);
        //Task Delete(EntityDto<Guid> input);
        Task EditContact(ContactDetailOutput input);
        Task EditStatus(Guid id, string status);
        Task<Company> GetCompany(Guid id);
        Task<ListResultDto<ContactListDto>> GetCompanies(GetAllContactInput input);
        Task<ListResultDto<ContactListDto>> Filter(int input, string status);
        Task<ListResultDto<ContactListDto>> GetCompanyId(Guid input);
        Task<ListResultDto<ContactListDto>> GetById(Guid input);

        Task ImportContact(List<ContactImport> input); 
    }
}
