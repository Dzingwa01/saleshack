﻿using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using Abp.Authorization;
using System;
using Abp.Runtime.Session;
using Abp.AutoMapper;
using SalesHack.Companies.Dto;
using Abp.UI;
using SalesHack.Prospects;
using Abp.Application.Services;
using Abp.Extensions;

namespace SalesHack.Companies
{
    [AbpAuthorize]
    //public class ContactAppService : SalesHackServiceBase, IContactAppServices
    public class ContactAppService : AsyncCrudAppService<Contact, ContactListDto, Guid, PagedAndSortedResultRequestDto, CreateContactInput, ContactListDto>, IContactAppServices
    {
        private readonly IContactManager _ContactManager;
        private readonly ICompanyManager _CompanyManager;

        private readonly IRepository<Contact, Guid> _ContactRepository;
        private readonly IRepository<Company, Guid> _CompanyRepository;


        private readonly IContactProspectManager _ContactProspectManager;
        private readonly ICompanyProspectManager _companyProspectManager;


        private readonly IRepository<ContactProspect, Guid> _contactProspectRepository;

        private readonly IRepository<CompanyProspect, Guid> _companyProspectRepository;

        public ContactAppService(IRepository<Contact, Guid> ContactRepository, IRepository<CompanyProspect, Guid> _companyProspectRepository, IContactManager _ContactManager, ICompanyManager _CompanyManager, IContactProspectManager _ContactProspectManager,
            IRepository<ContactProspect, Guid> _contactProspectRepository, IRepository<Company, Guid> CompanyRepository, ICompanyProspectManager companyProspectManager) : base(ContactRepository)
        {
            _ContactRepository = ContactRepository;
            this._ContactManager = _ContactManager;
            this._ContactProspectManager = _ContactProspectManager;
            this._contactProspectRepository = _contactProspectRepository;
            this._CompanyManager = _CompanyManager;
            this._companyProspectRepository = _companyProspectRepository;
            _CompanyRepository = CompanyRepository;
            _companyProspectManager = companyProspectManager;
        }

        public  async Task<ContactListDto> Create(CreateContactInput input)
        {
            try
            {
                var id = AbpSession.GetTenantId();

                //if (input.CompanyId != null)
                //{
                var company = await _CompanyManager.GetAsync((Guid)input.CompanyId);
                var companyProspect = await _companyProspectRepository.GetAll().Where(x => x.Email == company.Email).FirstOrDefaultAsync();
                //}

                var contact = Contact.Create(input.CompanyId, id, input.Name, input.Surname, input.Source, input.Industry, input.JobTitle, input.Department,
                    input.Telephone, input.Cellphone, input.Email, input.Address, input.City, input.Province, input.SpecialEvents,
                    input.Facebook, input.LinkedIn, input.GooglePlus, input.Instagram, input.Twitter, AbpSession.GetUserId(), input.Status);
                await _ContactManager.CreateAsync(contact);
                var contactProspect = _contactProspectRepository.FirstOrDefault(x => x.Email == contact.Email);

                //add ContactProspect (validate if it already exists)
                if (contactProspect == null)
                {
                    await _ContactProspectManager.CreateAsync(new ContactProspect
                    {
                        Name = contact.Name,
                        Surname = contact.Surname,
                        Source = contact.Source,
                        JobTitle = contact.JobTitle,
                        Department = contact.Department,
                        Telephone = contact.Telephone,
                        Cellphone = contact.Cellphone,
                        Email = contact.Email,
                        Industry = contact.Industry,
                        Address = contact.Address,
                        City = contact.City,
                        Province = contact.Province,
                        SpecialEvents = contact.SpecialEvents,
                        Facebook = contact.Facebook,
                        LinkedIn = contact.LinkedIn,
                        GooglePlus = contact.GooglePlus,
                        Instagram = contact.Instagram,
                        Twitter = contact.Twitter,
                        CompanyId = companyProspect.Id

                    });
                }
                else
                {

                    contactProspect.Name = contact.Name == null ? contactProspect.Name : contact.Name;
                    contactProspect.Surname = contact.Surname == null ? contactProspect.Surname : contact.Surname;
                    contactProspect.Source = contact.Source == null ? contactProspect.Source : contact.Source;
                    contactProspect.JobTitle = contact.JobTitle == null ? contactProspect.JobTitle : contact.JobTitle;
                    contactProspect.Department = contact.Department == null ? contactProspect.Department : contact.Department;
                    contactProspect.Telephone = contact.Telephone == null ? contactProspect.Telephone : contact.Telephone;
                    contactProspect.Cellphone = contact.Cellphone == null ? contactProspect.Cellphone : contact.Cellphone;
                    contactProspect.Email = contact.Email == null ? contactProspect.Email : contact.Email;
                    contactProspect.Industry = contact.Industry == null ? contactProspect.Industry : contact.Industry;
                    contactProspect.Address = contact.Address == null ? contactProspect.Address : contact.Address;
                    contactProspect.City = contact.City == null ? contactProspect.City : contact.City;
                    contactProspect.Province = contact.Province == null ? contactProspect.Province : contact.Province;
                    contactProspect.SpecialEvents = contact.SpecialEvents == null ? contactProspect.SpecialEvents : contact.SpecialEvents;
                    contactProspect.Facebook = contact.Facebook == null ? contactProspect.Facebook : contact.Facebook;
                    contactProspect.LinkedIn = contact.LinkedIn == null ? contactProspect.LinkedIn : contact.LinkedIn;
                    contactProspect.GooglePlus = contact.GooglePlus == null ? contactProspect.GooglePlus : contact.GooglePlus;
                    contactProspect.Instagram = contact.Instagram == null ? contactProspect.Instagram : contact.Instagram;
                    contactProspect.Twitter = contact.Twitter == null ? contactProspect.Twitter : contact.Twitter;
                    contactProspect.CompanyId = contact.CompanyId == null ? contactProspect.CompanyId : companyProspect.Id;

                    await _contactProspectRepository.UpdateAsync(contactProspect);

                }
            }
            catch (Exception e)
            {
                var m = e.Message;
                throw;
            }
            CurrentUnitOfWork.SaveChanges();

            var user = ObjectMapper.Map<Contact>(input);
            return MapToEntityDto(user);

        }


        //TODO: INCLUDE USERS


        public async Task<ListResultDto<ContactListDto>> GetAllContacts(GetAllContactInput input)
        {

            var contacts = await _ContactRepository
            .GetAll().Include(e => e.AssingedUser).Include(e => e.ContactCompany)
            .Include(e => e.ContactCompany)
            //.WhereIf(!input.IncludeCanceledEvents, e => !e.IsCancelled)
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<ContactListDto>(contacts.MapTo<List<ContactListDto>>());
        }
        public async Task<ListResultDto<ContactListDto>> GetById(Guid input)
        {

            var contacts = await _ContactRepository
            .GetAll().Where(x => x.Id == input)
            .ToListAsync();


            var page = new PagedAndSortedResultRequestDto();
            var page2 = new PagedResultRequestDto();


            return new ListResultDto<ContactListDto>(contacts.MapTo<List<ContactListDto>>());
        }
        public async Task<ListResultDto<ContactListDto>> GetCompanies(GetAllContactInput input)
        {

            var companyName = await _ContactRepository.GetAll().Include(e => e.ContactCompany)
            .OrderByDescending(e => e.Name)
            .ToListAsync();
            return new ListResultDto<ContactListDto>(companyName.MapTo<List<ContactListDto>>());
        }


        public async Task<ContactDetailOutput> GetDetail(EntityDto<Guid> input)
        {
            var Contact = await _ContactRepository
                .GetAll()
                .Include(e => e.ContactCompany)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            if (Contact == null)
            {
                throw new UserFriendlyException("Could not find the Contact, maybe it's deleted.");
            }

            return Contact.MapTo<ContactDetailOutput>();
        }
        public async Task<ListResultDto<ContactListDto>> Filter(int input, string status)
        {

            var list = await _ContactRepository.GetAll().Where(x => (x.Status == status) && (x.CreationTime.Date >= x.CreationTime.Date.AddDays(-input)))
            .OrderByDescending(e => e.CreationTime)
            .ToListAsync();
            return new ListResultDto<ContactListDto>(list.MapTo<List<ContactListDto>>());
        }

        public async Task<ListResultDto<ContactListDto>> GetQualificationContacts(string status)
        {
            var contacts = await _ContactRepository.GetAll().Where(x => x.Status != status).ToListAsync();
            return new ListResultDto<ContactListDto>(contacts.MapTo<List<ContactListDto>>());
        }
        public async Task<ListResultDto<ContactListDto>> GetCompanyId(Guid input)
        {
            var list = await _ContactRepository.GetAll().Where((x => x.CompanyId.Equals(input)))
                    .OrderByDescending(e => e.Name)
                    .ToListAsync();
            return new ListResultDto<ContactListDto>(list.MapTo<List<ContactListDto>>());
        }

        public async Task EditContact(ContactDetailOutput input)
        {
            var contact = await _ContactRepository.GetAsync(input.Id);
            ContactUpdate(input, contact);
            await _ContactRepository.UpdateAsync(contact);
        }

        public async Task EditStatus(Guid id, string status)
        {
            var contact = await _ContactRepository.GetAsync(id);
            contact.Status = status;
            await _ContactRepository.UpdateAsync(contact);
        }

        private static void ContactUpdate(ContactDetailOutput input, Contact contact)
        {
            contact.Name = input.Name == null ? contact.Name : input.Name;
            contact.Surname = input.Surname == null ? contact.Surname : input.Surname;
            contact.Source = input.Source == null ? contact.Source : input.Source;
            contact.Industry = input.Industry == null ? contact.Industry : input.Industry;
            contact.JobTitle = input.JobTitle == null ? contact.JobTitle : input.JobTitle;
            contact.Department = input.Department == null ? contact.Department : input.Department;
            contact.Telephone = input.Telephone == null ? contact.Telephone : input.Telephone;
            contact.Cellphone = input.Cellphone == null ? contact.Cellphone : input.Cellphone;
            contact.Email = input.Email == null ? contact.Email : input.Email;
            contact.Status = input.Status == null ? contact.Status : input.Status;
            contact.Address = input.Address == null ? contact.Address : input.Address;
            contact.City = input.City == null ? contact.City : input.City;
            contact.Province = input.Province == null ? contact.Province : input.Province;
            contact.SpecialEvents = input.SpecialEvents == null ? contact.SpecialEvents : input.SpecialEvents;
            contact.Facebook = input.Facebook == null ? contact.Facebook : input.Facebook;
            contact.LinkedIn = input.LinkedIn == null ? contact.LinkedIn : input.LinkedIn;
            contact.GooglePlus = input.GooglePlus == null ? contact.GooglePlus : input.GooglePlus;
            contact.Instagram = input.Instagram == null ? contact.Instagram : input.Instagram;
            contact.Twitter = input.Twitter == null ? contact.Twitter : input.Twitter;
            contact.CompanyId = input.CompanyId == null ? contact.CompanyId : input.CompanyId;
        }

        public Task<Company> GetCompany(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task ImportContact(List<ContactImport> input)
        {
            input = input.Where(x => x.Name != null).ToList();

            foreach (ContactImport contact in input)
            {
                var company = await _CompanyRepository.GetAll().Where(x => x.Email == contact.Email).FirstOrDefaultAsync();
                Guid? companyId = Guid.NewGuid();
                if (contact.CompanyName != null)
                {
                    var newCompany = Company.Create(AbpSession.GetTenantId(), contact.CompanyName, contact.Email, AbpSession.GetUserId(),
                                      contact.CompanySize, null, 0, 0, contact.Website, contact.Phone, contact.Industry, null, contact.Address, contact.City,
                                      contact.Province, contact.Country, contact.BeeStatus, null, null, null, null, null, null, null, null, null);
                    if (company == null)
                    {
                        await _CompanyManager.CreateAsync(newCompany);
                        companyId = newCompany.Id;
                    }
                    else
                    {
                        try
                        {
                            UpdateCompany(newCompany, company);
                            await _CompanyRepository.UpdateAsync(company);
                            companyId = company.Id;
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else { companyId = null; }

                var newContact = Contact.Create(companyId, AbpSession.GetTenantId(), contact.Name, contact.Surname, contact.Source,
                        contact.Industry, contact.JobTitle, contact.Department, contact.Telephone, contact.Cellphone, contact.Email,
                        contact.Address, contact.City, contact.Province, null, null, null, null, null, null, AbpSession.GetUserId(), "New");
                var oldContact = await _ContactRepository.GetAll().Where(x => x.Email == newContact.Email).FirstOrDefaultAsync();

                if (oldContact == null)
                {
                    await _ContactManager.CreateAsync(newContact);
                }
                else
                {
                    UpdateContact(oldContact, newContact);
                    await _ContactRepository.UpdateAsync(oldContact);
                }

                var companyProspect = await _companyProspectRepository.GetAll().Where(x => x.Email == newContact.Email).FirstOrDefaultAsync();
                var contactProspect = await _contactProspectRepository.GetAll().Where(x => x.Email == newContact.Email).FirstOrDefaultAsync();
                if (companyProspect == null)
                {
                    var newCompanyProspect = CompanyProspect.Create(contact.CompanyName, contact.Email, AbpSession.GetUserId(), contact.CompanySize, null, 0, 0, contact.Website
                        , contact.Phone, contact.Industry, null, contact.Address, contact.City, contact.Province, contact.Country, contact.BeeStatus, null, null, null, null, null, null, null, null, null);
                    await _companyProspectManager.CreateAsync(newCompanyProspect);
                }
                else if (contactProspect == null)
                {
                    var newContactProspect = ContactProspect.Create(companyId, contact.Name, contact.Surname, contact.Source, contact.JobTitle,
                    contact.Department, contact.Phone, contact.Cellphone, contact.Email, contact.Industry, contact.Address, contact.City, contact.Province,
                    null, null, null, null, null, null);

                    await _ContactProspectManager.CreateAsync(newContactProspect);
                }
            }

        }

        private static void UpdateCompany(Company input, Company company)
        {
            //company.Id = input.Id;
            company.Name = input.Name == null ? company.Name : input.Name;
            company.Email = input.Email == null ? company.Email : input.Email;
            company.Size = input.Size == null ? company.Size : input.Size;
            company.Phase = input.Phase == null ? company.Phase : input.Phase;
            company.Turnover = input.Turnover == 0 ? company.Turnover : input.Turnover;
            company.YearInBusiness = input.YearInBusiness == 0 ? company.YearInBusiness : input.YearInBusiness;
            company.WebUrl = input.WebUrl == null ? company.WebUrl : input.WebUrl;
            company.Telephone = input.Telephone == null ? company.Telephone : input.Telephone;
            company.Industry = input.Industry == null ? company.Industry : input.Industry;
            company.BusinessType = input.BusinessType == null ? company.BusinessType : input.BusinessType;
            company.Address = input.Address == null ? company.Address : input.Address;
            company.City = input.City == null ? company.City : input.City;
            company.Province = input.Province == null ? company.Province : input.Province;
            company.Country = input.Country == null ? company.Country : input.Country;
            company.BeeStatus = input.BeeStatus == null ? company.BeeStatus : input.BeeStatus;
            company.PostalCode = input.PostalCode == null ? company.PostalCode : input.PostalCode;
            company.Source = input.Source == null ? company.Source : input.Source;
            company.Stage = input.Stage == null ? company.Stage : input.Stage;
            company.SpecialEvents = input.SpecialEvents == null ? company.SpecialEvents : input.SpecialEvents;
            company.Facebook = input.Facebook == null ? company.Facebook : input.Facebook;
            company.LinkedIn = input.LinkedIn == null ? company.LinkedIn : input.LinkedIn;
            company.GooglePlus = input.GooglePlus == null ? company.GooglePlus : input.GooglePlus;
            company.Instagram = input.Instagram == null ? company.Instagram : input.Instagram;
            company.Twitter = input.Twitter == null ? company.Twitter : input.Twitter;
        }
        public async Task<ListResultDto<ContactListDto>> ContactFilter(string input)
        {
            var contacts = await _ContactRepository
            .GetAll().WhereIf(
                !input.IsNullOrEmpty(),
                p => p.Name.Contains(input) ||
                     p.Address.Contains(input) ||
                     p.Email.Contains(input) ||
                     p.Industry.Contains(input) ||
                     p.Surname.Contains(input) ||
                     p.Telephone.Contains(input))
                        .OrderByDescending(e => e.CreationTime)
            .ToListAsync();

            return new ListResultDto<ContactListDto>(contacts.MapTo<List<ContactListDto>>());
        }
        private static void UpdateContact(Contact input, Contact contact)
        {
            contact.Name = input.Name == null ? contact.Name : input.Name;
            contact.Surname = input.Surname == null ? contact.Surname : input.Surname;
            contact.Source = input.Source == null ? contact.Source : input.Source;
            contact.Industry = input.Industry == null ? contact.Industry : input.Industry;
            contact.JobTitle = input.JobTitle == null ? contact.JobTitle : input.JobTitle;
            contact.Department = input.Department == null ? contact.Department : input.Department;
            contact.Telephone = input.Telephone == null ? contact.Telephone : input.Telephone;
            contact.Cellphone = input.Cellphone == null ? contact.Cellphone : input.Cellphone;
            contact.Email = input.Email == null ? contact.Email : input.Email;
            contact.Status = input.Status == null ? contact.Status : input.Status;
            contact.Address = input.Address == null ? contact.Address : input.Address;
            contact.City = input.City == null ? contact.City : input.City;
            contact.Province = input.Province == null ? contact.Province : input.Province;
            contact.SpecialEvents = input.SpecialEvents == null ? contact.SpecialEvents : input.SpecialEvents;
            contact.Facebook = input.Facebook == null ? contact.Facebook : input.Facebook;
            contact.LinkedIn = input.LinkedIn == null ? contact.LinkedIn : input.LinkedIn;
            contact.GooglePlus = input.GooglePlus == null ? contact.GooglePlus : input.GooglePlus;
            contact.Instagram = input.Instagram == null ? contact.Instagram : input.Instagram;
            contact.Twitter = input.Twitter == null ? contact.Twitter : input.Twitter;
            contact.CompanyId = input.CompanyId == null ? contact.CompanyId : input.CompanyId;
        }
    }
}
