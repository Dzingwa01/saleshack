﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Outreach.Dto
{
    public class EmailDto
    {
        public List<string> Recipients { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public bool isBulk { get; set; }
    }
}
