﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Identity;
using SalesHack.Authorization.Users;
using SalesHack.Outreach.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Outreach
{
    [AbpAuthorize]
    public class OutreachAppService : SalesHackServiceBase, IOutreachAppService
    {
        private readonly IAbpSession _session;
        private readonly IRepository<User, long> _userRepository;
        public OutreachAppService(IAbpSession session, IRepository<User, long> userRepository)
        {
            _session = session;
            _userRepository = userRepository;
        }
        public bool SendEmail(EmailDto email)
        {
            var userId = _session.UserId;
            var user = _userRepository.FirstOrDefault(x => x.Id == userId);

            try
            {
                if (!email.isBulk)
                {
                    Emailer.Send(to: email.Recipient, subject: email.Subject, body: email.Message, fromEmail: user.EmailAddress, isBodyHtml: true);
                }
                else
                {
                    Parallel.ForEach(email.Recipients, recipient =>
                    {
                        Emailer.Send(to: recipient, subject: email.Subject, body: email.Message, fromEmail: user.EmailAddress, isBodyHtml: true);
                    });
                }

                return true;
            }
            catch (Exception)
            {
                return false;

            }

            //throw new NotImplementedException();
        }

        public bool SendSms(SmsDto sms)
        {
            try
            {
                if (!sms.isBulk)
                {
                    Smser.Send(to: sms.Recipient, messageBody: sms.Message);
                }
                else
                {
                    Parallel.ForEach(sms.Recipients, recipient =>
                    {
                        Smser.Send(to: recipient, messageBody: sms.Message);
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }
    }
}
