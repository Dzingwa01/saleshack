﻿using SalesHack.Outreach.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Outreach
{
    public interface IOutreachAppService
    {
        bool SendEmail(EmailDto email);

        bool SendSms(SmsDto sms);

    }
}
