﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace SalesHack
{
    static class Smser
    {
        //public static string Send(string to, string messageBody)
        //{
        //    // Find your Account Sid and Token at twilio.com/console
        //    const string accountSid = "ACe54e1741ee92846bb4e79e77ea61baa0";
        //    const string authToken = "4a3a7ad1d58ff54610314d9187448188";

        //    TwilioClient.Init(accountSid, authToken);

        //    var message = MessageResource.Create(body: messageBody,
        //        from: new Twilio.Types.PhoneNumber("+27793071968"),
        //        to: new Twilio.Types.PhoneNumber(to)
        //    );

        //    return message.Sid;
        //}

        public static string Send(string to, string messageBody)
        {
            var toNum = to.Replace(" ", String.Empty);
            if (to.StartsWith('0'))
            {
               to = "27"+ toNum.Substring(1, toNum.Length - 1);
            }


            String username = "747eeadf-a376-40e4-a19b-1dd35d3b6f81";
            String password = "7yKquAoQAldMsH4D0/Fb0pJi+x2jGqry";
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            //Get Auth Token
            var clientToken = new RestClient("https://rest.mymobileapi.com");
            var sendTokenRequest = new RestRequest("/v1/Authentication", Method.GET);
            sendTokenRequest.AddHeader("authorization", string.Format("basic {0}", encoded));
            var sendTokenResponse = clientToken.Execute(sendTokenRequest);

            var data = JsonConvert.DeserializeObject<Data>(sendTokenResponse.Content);


            var client = new RestClient("https://rest.smsportal.com");
            var authToken = data.token;
            client.Authenticator = new RestSharp.Authenticators.JwtAuthenticator(authToken);

            var sendRequest = new RestRequest("/v1/bulkmessages", Method.POST);
            sendRequest.AddJsonBody(new {
                Messages = new[] {
                         new { content = messageBody, destination = to } /* Format: 27749183210 */ 
                }
            });

            var sendResponse = client.Execute(sendRequest);

            return sendResponse.Content;

            //if (sendResponse.StatusCode == HttpStatusCode.OK)
            //{
            //    Console.WriteLine(sendResponse.Content);
            //}
            //else
            //{
            //    Console.WriteLine("error: " + sendResponse.ErrorMessage);
            //}
        }
    }

    public class Data
    {
        [JsonProperty("token")]
        public string token { get; set; }
    }
}
