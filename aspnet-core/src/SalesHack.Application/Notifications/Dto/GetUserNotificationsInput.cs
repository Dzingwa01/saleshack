﻿using Abp.Notifications;
using SalesHack.Dto;

namespace SalesHack.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}