﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;

namespace SalesHack.EntityFrameworkCore.Seed.Host
{
    public class DefaultSettingsCreator
    {
        private readonly SalesHackDbContext _context;

        public DefaultSettingsCreator(SalesHackDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            //Host = "Abp.Net.Mail.Smtp.Host";
            //Port = "Abp.Net.Mail.Smtp.Port";
            //UserName = "Abp.Net.Mail.Smtp.UserName";
            //Password = "Abp.Net.Mail.Smtp.Password";
            //Domain = "Abp.Net.Mail.Smtp.Domain";
            //EnableSsl = "Abp.Net.Mail.Smtp.EnableSsl";
            //UseDefaultCredentials = "Abp.Net.Mail.Smtp.UseDefaultCredentials";

            // Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "no-reply@saleshack.io");
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "Saleshack Mailer");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Host, "mail.kipson.co.za");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Port, "25");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UserName, "test@kipson.co.za");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Password, "Password@1");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Domain, "kipson.co.za");
            AddSettingIfNotExists(EmailSettingNames.Smtp.EnableSsl, "false");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UseDefaultCredentials, "false");

            // Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "en");
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.IgnoreQueryFilters().Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}
