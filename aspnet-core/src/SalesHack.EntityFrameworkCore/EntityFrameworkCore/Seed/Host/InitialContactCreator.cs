﻿using Abp.Runtime.Session;
using SalesHack.Companies;
using SalesHack.Deals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesHack.EntityFrameworkCore.Seed.Host
{
    public class InitialContactCreator
    {
        private readonly SalesHackDbContext _context;

        public InitialContactCreator(SalesHackDbContext context)
        {
            _context = context;
        }

        //public void Create()
        //{
        //    var douglas = _context.Contacts.FirstOrDefault(p => p.Email == "propella@fortytwo.com");
        //    if (douglas == null)
        //    {

        //        var contact = Contact.Create(1,"Propella", "engel.co.za", "large", "upper", "propella.co.za",
        //            "041 504 3564", "Incubator", "propella@engel.co.za", "lorem ipsum", "port elizabeth", "Eastern Cape", "6001", "lorem ipsum", "3rd Stage",
        //            "unknown", "Propella","gg", 1,"");
        //        _context.Contacts.Add(contact);

        //    }


        //    var ll = _context.Contacts.FirstOrDefault(p => p.Email == "propella2@fortytwo.com");
        //    if (ll == null)
        //    {

        //        var contact = Contact.Create(2,"Propella", "propella", "large", "upper", "propella.co.za",
        //            "041 504 3564", "Incubator", "propella2@engel.co.za", "lorem ipsum", "port elizabeth", "Eastern Cape", "6001", "lorem ipsum", "3rd Stage",
        //            "unknown", "Propella", "gg",1,"");
        //        _context.Contacts.Add(contact);

        //    }

        //}

        public void CreateStages()
        {
            var stages = _context.Stages.FirstOrDefault(p => p.Name == "New");
            if (stages == null)
            {
                var stage = Stage.Create("New", "New Description", 1);
                _context.Stages.Add(stage);
            }
            var stages2 = _context.Stages.FirstOrDefault(p => p.Name == "Awareness");
            if (stages2 == null)
            {
                var stage2 = Stage.Create("Awareness", "Awereness Description", 2);
                _context.Stages.Add(stage2);
            }
            var stages3 = _context.Stages.FirstOrDefault(p => p.Name == "Interest");
            if (stages3 == null)
            {
                var stage3 = Stage.Create("Interest", "Interest Description", 3);
                _context.Stages.Add(stage3);
            }
            var stages4 = _context.Stages.FirstOrDefault(p => p.Name == "Decision");
            if (stages4 == null)
            {
                var stage4 = Stage.Create("Decision", "Decision Description", 4);
                _context.Stages.Add(stage4);
            }
            var stages5 = _context.Stages.FirstOrDefault(p => p.Name == "Closed");
            if (stages5 == null)
            {
                var stage5 = Stage.Create("Closed", "Closed Description", 5);
                _context.Stages.Add(stage5);
            }
            var stages6 = _context.Stages.FirstOrDefault(p => p.Name == "Lost");
            if (stages6 == null)
            {
                var stage6 = Stage.Create("Lost", "Lost Description", 6);
                _context.Stages.Add(stage6);
            }

        }
        public void CreateDeals()
        {
            var deal = _context.Deals.FirstOrDefault(p => p.Name == "Web Development");
            if (deal == null)
            {
                var deal1 = Deal.Create(1, "Web Development", 1, Guid.Parse("ecd9572a-891f-40fb-c7f4-08d5a084fa45"), 2, 20000.0, 10000.0, null, null, Guid.Parse("1cdcb349-0fdc-41b8-1873-08d5a07bff35"), Guid.Parse("3880b0ed-6a15-4133-9902-08d678a3425d"));
                _context.Deals.Add(deal1);
            }
        }
    }
}
