﻿using SalesHack.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SalesHack.EntityFrameworkCore.Seed.Host
{
    public enum DataTableTypes
    {
        INDUSTRY,
        BEE_STATUS,
        DEPARTMENT,
        AGE_BRACKET,
        COMPANY_SIZE,
        CITY,
        SERVICE_OFFERING,
        EST_BRACKET,
        LEVEL_AT_COMPANY,
        JOB_TITLE,
        PROVINCE,
        EXPERIENCE,
        SOURCE
    }
    public class DataTableCreator
    {
        private readonly SalesHackDbContext _context;

        public DataTableCreator(SalesHackDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            CreateIndustries();
            CreateCompanySize();
            CreateBeeStatus();
            CreateDepartment();
            CreateJobTitle();
            CreateAgeBracket();
            CreateServiceOffering();
            CreateEstBracket();
            CreateLevelAtCompany();
            CreateProvince();
            CreateCity();
            CreateExperience();
            CreateSources();
        }



        public void CreateIndustries()
        {
            var industry = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.INDUSTRY.ToString()));
            if (industry == null)
            {
                string[] industries = { "Accommodation", "Accounting" };

                foreach (string str in industries)
                {
                    var industryValue = DataTable.Create(DataTableTypes.INDUSTRY.ToString(), str);
                    _context.DataTable.Add(industryValue);
                }

            }
        }

        public void CreateCompanySize()
        {
            var companySize = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.COMPANY_SIZE.ToString()));
            if (companySize == null)
            {
                string[] companySizes = { "1-9 (Micro)", "10-49 (Small)" };

                foreach (string str in companySizes)
                {
                    var companySizeValue = DataTable.Create(DataTableTypes.COMPANY_SIZE.ToString(), str);
                    _context.DataTable.Add(companySizeValue);
                }

            }
        }

        public void CreateBeeStatus()
        {
            var beeStatus = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.BEE_STATUS.ToString()));
            if (beeStatus == null)
            {
                string[] beeStatuses = { "Level 1 (135% procurement recognition)", "Level 1 (125% procurement recognition)" };

                foreach (string str in beeStatuses)
                {
                    var beeStatusValue = DataTable.Create(DataTableTypes.BEE_STATUS.ToString(), str);
                    _context.DataTable.Add(beeStatusValue);
                }

            }
        }
        public void CreateDepartment()
        {
            var department = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.DEPARTMENT.ToString()));
            if (department == null)
            {
                string[] departments = { "Marketing", "Sales" };

                foreach (string str in departments)
                {
                    var departmentValue = DataTable.Create(DataTableTypes.DEPARTMENT.ToString(), str);
                    _context.DataTable.Add(departmentValue);
                }

            }
        }
        public void CreateJobTitle()
        {
            var jobTitle = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.JOB_TITLE.ToString()));
            if (jobTitle == null)
            {
                string[] jobTitles = { "Account Executive", "Sales Manager" };

                foreach (string str in jobTitles)
                {
                    var jobTitleValue = DataTable.Create(DataTableTypes.JOB_TITLE.ToString(), str);
                    _context.DataTable.Add(jobTitleValue);
                }

            }
        }


        public void CreateAgeBracket()
        {
            var ageBracket = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.AGE_BRACKET.ToString()));
            if (ageBracket == null)
            {
                string[] ageBrackets = { "18-25", "26-35", "36-45", "46-55", "55+" };

                foreach (string str in ageBrackets)
                {
                    var ageBracketValue = DataTable.Create(DataTableTypes.AGE_BRACKET.ToString(), str);
                    _context.DataTable.Add(ageBracketValue);
                }

            }
        }

        public void CreateServiceOffering()
        {
            var serviceOffering = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.SERVICE_OFFERING.ToString()));
            if (serviceOffering == null)
            {
                string[] serviceOfferings = { "Web Development", "Software Development" };

                foreach (string str in serviceOfferings)
                {
                    var serviceValue = DataTable.Create(DataTableTypes.SERVICE_OFFERING.ToString(), str);
                    _context.DataTable.Add(serviceValue);
                }

            }
        }

        public void CreateEstBracket()
        {
            var estBracket = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.EST_BRACKET.ToString()));
            if (estBracket == null)
            {
                string[] estBrackets = { "50K-499K", "500K-999K" };

                foreach (string str in estBrackets)
                {
                    var estBracketValue = DataTable.Create(DataTableTypes.EST_BRACKET.ToString(), str);
                    _context.DataTable.Add(estBracketValue);
                }

            }
        }

        public void CreateLevelAtCompany()
        {
            var level = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.LEVEL_AT_COMPANY.ToString()));
            if (level == null)
            {
                string[] levels = { "Director", "Marketing Manager" };

                foreach (string str in levels)
                {
                    var levelValue = DataTable.Create(DataTableTypes.LEVEL_AT_COMPANY.ToString(), str);
                    _context.DataTable.Add(levelValue);
                }

            }
        }

        public void CreateProvince()
        {
            var province = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.PROVINCE.ToString()));
            if (province == null)
            {
                string[] provinces = { "Eastern Cape", "Gauteng", "Kwazulu-Natal", "Western Cape", "Northern Cape", "North West", "Limpopo", "Mpumalanga", "Free-State" };
                foreach (string str in provinces)
                {
                    var provinceValue = DataTable.Create(DataTableTypes.PROVINCE.ToString(), str);
                    _context.DataTable.Add(provinceValue);
                }
            }
        }

        public void CreateCity()
        {
            var city = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.CITY.ToString()));
            if (city == null)
            {
                string[] cities = { "Port Elizabeth", "Durban", "Cape Town", "Johannesburg" };
                foreach (string str in cities)
                {
                    var cityValue = DataTable.Create(DataTableTypes.CITY.ToString(), str);
                    _context.DataTable.Add(cityValue);
                }
            }
        }
        private void CreateExperience()
        {
            var experience = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.EXPERIENCE.ToString()));
            if (experience == null)
            {
                string[] yearsOfExperience = { "1-3", "4-5", "6-10", "10+" };
                foreach (string str in yearsOfExperience)
                {
                    var cityValue = DataTable.Create(DataTableTypes.EXPERIENCE.ToString(), str);
                    _context.DataTable.Add(cityValue);
                }
            }
        }

        private void CreateSources()
        {
            var source = _context.DataTable.FirstOrDefault(p => p.Type.Equals(DataTableTypes.SOURCE.ToString()));
            if (source == null)
            {
                string[] sources = { "Online Sources", "Website", "Blogging", "Premium Content", "Organic Search",
                    "Email Marketing", "Organic Search", "Digital Advertising", "Media Coverage", "Direct Marketing",
                "Traditional Advertising", "Affiliate/ Partner Programs", "Events/ Shows", "Inbound Phone Calls",
                    "Outbound Sales", "Referrals", "Speaking Engagements", "Traditional/ Offline Networking"};
                foreach (string str in sources)
                {
                    var sourceValue = DataTable.Create(DataTableTypes.SOURCE.ToString(), str);
                    _context.DataTable.Add(sourceValue);
                }
            }
        }
    }
}
