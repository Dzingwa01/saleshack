﻿namespace SalesHack.EntityFrameworkCore.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly SalesHackDbContext _context;

        public InitialHostDbBuilder(SalesHackDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            // new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            //new InitialCompanyCreator(_context).Create();
            new InitialContactCreator(_context).CreateStages();
            // new InitialContactCreator(_context).CreateDeals();
            //new InitialContactProspectCreator(_context).Create();
           // new DataTableCreator(_context).Seed();
            _context.SaveChanges();
        }
    }
}
