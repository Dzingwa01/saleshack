﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using SalesHack;
using SalesHack.EntityFrameworkCore.Seed;

namespace SalesHack.EntityFrameworkCore
{
    [DependsOn(
        typeof(SalesHackCoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class SalesHackEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                //Configuration.Modules.().DefaultWrapResultAttribute.WrapOnSuccess = false;

                Configuration.Modules.AbpEfCore().AddDbContext<SalesHackDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        SalesHackDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        SalesHackDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SalesHackEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
