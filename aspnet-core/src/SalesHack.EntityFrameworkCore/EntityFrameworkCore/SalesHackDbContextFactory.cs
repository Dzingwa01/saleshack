﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SalesHack.Configuration;
using SalesHack.Web;

namespace SalesHack.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SalesHackDbContextFactory : IDesignTimeDbContextFactory<SalesHackDbContext>
    {
        public SalesHackDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SalesHackDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
   
            SalesHackDbContextConfigurer.Configure(builder, configuration.GetConnectionString(SalesHackConsts.ConnectionStringName));

            return new SalesHackDbContext(builder.Options);
        }
    }
}
