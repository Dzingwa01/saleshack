using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace SalesHack.EntityFrameworkCore
{
    public static class SalesHackDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SalesHackDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SalesHackDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
