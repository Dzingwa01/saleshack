﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using SalesHack.Authorization.Roles;
using SalesHack.Authorization.Users;
using SalesHack.MultiTenancy;
using SalesHack.Companies;
using System.Collections.Generic;
using SalesHack.Prospects;
using SalesHack.General;
using SalesHack.Deals;
using SalesHack.MultiTenancy.Payments;
using SalesHack.MultiTenancy.Accounting;
using SalesHack.Storage;
using SalesHack.Editions;
using SalesHack.Authorization.Users.ProfilePhoto;

namespace SalesHack.EntityFrameworkCore
{
    public class SalesHackDbContext : AbpZeroDbContext<Tenant, Role, User, SalesHackDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactStage> ContactStages { get; set; }
        public virtual DbSet<CompanyProspect> CompanyProspects { get; set; }
        public virtual DbSet<ContactProspect> ContactProspects { get; set; }
        public virtual DbSet<Prospect> Prospects { get; set; }
        public virtual DbSet<ProspectList> ProspectLists { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Deal> Deals { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }
        public virtual DbSet<Stage> Stages { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Email> Emails { get; set; }
        public virtual DbSet<DataTable> DataTable { get; set; }
        public virtual DbSet<IdealClient> IdealClient { get; set; }
        //public virtual DbSet<SubscriptionPayment> SubscriptionPayment { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<BinaryObject> BinaryObject { get; set; }
        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }
        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<UserServiceOffering> UserServiceOfferings { get; set; }
        public virtual DbSet<UserTargetIndustry> UserTargetIndustry { get; set; }
        public virtual DbSet<ProfilePhoto> ProfilePhotos { get; set; }
        public SalesHackDbContext(DbContextOptions<SalesHackDbContext> options)
            : base(options)
        {

        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Entity<BinaryObject>(b =>
        //    {
        //        b.HasIndex(e => new { e.TenantId });
        //    });


        //    modelBuilder.Entity<Tenant>(b =>
        //    {
        //        b.HasIndex(e => new { e.SubscriptionEndDateUtc });
        //        b.HasIndex(e => new { e.CreationTime });
        //    });

        //    modelBuilder.Entity<SubscriptionPayment>(b =>
        //    {
        //        b.HasIndex(e => new { e.Status, e.CreationTime });
        //        b.HasIndex(e => new { e.PaymentId, e.Gateway });
        //    });

        //    //            
        //    //            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
        //    //                .Where(q => q.GetInterface(typeof(IEntityTypeConfiguration<>).FullName) != null);
        //    //
        //    //
        //    //            foreach (var type in typesToRegister)
        //    //            {
        //    //                dynamic configurationInstanc = Activator.CreateInstance(type);
        //    //
        //    //                modelBuilder.ApplyConfiguration(configurationInstanc);
        //    //            }
        //    //            

        //    //modelBuilder.ConfigurePersistedGrantEntity();
        //}
    }
}
