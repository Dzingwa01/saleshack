using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace SalesHack.Controllers
{
    public abstract class SalesHackControllerBase: AbpController
    {
        protected SalesHackControllerBase()
        {
            LocalizationSourceName = SalesHackConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
