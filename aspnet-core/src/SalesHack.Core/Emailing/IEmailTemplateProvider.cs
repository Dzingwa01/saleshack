﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate(int? tenantId);
    }
}
