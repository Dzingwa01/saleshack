﻿using Abp.Application.Services;
using Microsoft.Extensions.Configuration;

namespace SalesHack.Configuration
{
    public interface IAppConfigurationAccessor: IApplicationService
    {
        IConfigurationRoot Configuration { get; }
    }
}
