﻿namespace SalesHack
{
    public class SalesHackConsts
    {
        public const string LocalizationSourceName = "SalesHack";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
