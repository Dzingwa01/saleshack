﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Deals
{
    public interface IDealManager : IDomainService
    {
        Task<Deal> GetAsync(Guid id);
        Task CreateAsync(Deal deal);

    }
}
