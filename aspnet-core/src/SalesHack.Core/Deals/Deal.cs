﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Deals
{
    [Table("Deals")]
    public class Deal : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual string Name { get; set; }
        public virtual double? ExpectedAmount { get; set; }
        public virtual double? Amount { get; set; }
        public virtual DateTime? ExpectedCloseDate { get; set; }
        public int Type { get; set; }
        public long OwnerId { get; set; }
        [ForeignKey(nameof(OwnerId))]
        public virtual User Owner { get; set; }
        public virtual DateTime? CloseDate { get; set; }
        public Guid PipelineStageId { get; set; }
        [ForeignKey(nameof(PipelineStageId))]
        public Stage Stage { get; set; }
        public Guid CompanyId { get; set; }
        [ForeignKey(nameof(CompanyId))]
        public Company Company { get; set; }
        [ForeignKey(nameof(ContactId))]
        public Guid? ContactId { get; set; }
        public Contact Contact { get; set; }
        public int TenantId { get; set; }

        public static Deal Create(int TenantId, string Name, int Type, Guid ContactId, long OwnerId, double? ExpectedAmount, double? Amount, DateTime? ExpectedCloseDate, DateTime? CloseDate, Guid CompanyId, Guid PipelineStageId)
        {
            var deal = new Deal
            {
                OwnerId = OwnerId,
                TenantId = TenantId,
                Name = Name,
                Type = Type,
                ExpectedAmount = ExpectedAmount,
                Amount = Amount,
                ExpectedCloseDate = ExpectedCloseDate,
                CloseDate = CloseDate,
                PipelineStageId = PipelineStageId,
                CompanyId = CompanyId,
                ContactId = ContactId
            };
            return deal;
        }
    }
}
