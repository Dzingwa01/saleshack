﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Deals
{
    [Table("Stages")]
    public class Stage : FullAuditedEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Position { get; set; }
        public ICollection<Deal> Deals { get; set; }
        public static Stage Create(string Name, String Description, int Position)
        {
            var stage = new Stage
            {
                Name = Name,
                Description = Description,
                Position = Position
            };
            return stage;
        }

    }
}
