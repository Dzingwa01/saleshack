﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Deals
{
    public class DealManager : IDealManager
    {
        private readonly IRepository<Deal, Guid> _dealRepository;

        public DealManager(IRepository<Deal, Guid> dealRepository)
        {
            _dealRepository = dealRepository;
        }
        public async Task CreateAsync(Deal deal)
        {
            await _dealRepository.InsertAsync(deal);
        }

        public async Task<Deal> GetAsync(Guid id)
        {
            var deal = await _dealRepository.FirstOrDefaultAsync(id);
            if (deal == null)
            {
                throw new UserFriendlyException("Deal Not Found, Maybe it's deleted");
            }
            return deal;
        }
    }
}
