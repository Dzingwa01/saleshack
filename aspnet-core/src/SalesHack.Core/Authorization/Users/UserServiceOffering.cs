﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Authorization.Users
{
    [Table("UserServiceOffering")]
    public class UserServiceOffering : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        [ForeignKey(nameof(UserId))]
        public long UserId { get; set; }
        public User User { get; set; }
        public int TenantId { get; set; }
        [ForeignKey(nameof(ServiceId))]
        public int ServiceId { get; set; }
        public DataTable DataTable { get; set; }

        public static UserServiceOffering Create(int TenantId, long UserId, int ServiceId)
        {
            var userServiceOffering = new UserServiceOffering
            {
                TenantId = TenantId,
                UserId = UserId,
                ServiceId = ServiceId
            };
            return userServiceOffering;
        }
    }
}
