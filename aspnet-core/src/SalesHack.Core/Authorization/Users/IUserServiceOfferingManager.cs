﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users
{
    public interface IUserServiceOfferingManager : IDomainService
    {
        Task<UserServiceOffering> GetAsync(Guid id);
        Task CreateAsync(UserServiceOffering serviceOffering);
    }
}
