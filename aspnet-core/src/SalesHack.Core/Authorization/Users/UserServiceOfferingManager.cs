﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users
{
    public class UserServiceOfferingManager : IUserServiceOfferingManager
    {
        private readonly IRepository<UserServiceOffering, Guid> _userServiceOfferingRepository;

        public UserServiceOfferingManager(IRepository<UserServiceOffering, Guid> userServiceOfferingRepository)
        {
            _userServiceOfferingRepository = userServiceOfferingRepository;
        }
        public async Task CreateAsync(UserServiceOffering serviceOffering)
        {
            await _userServiceOfferingRepository.InsertAsync(serviceOffering);
        }

        public async Task<UserServiceOffering> GetAsync(Guid id)
        {
            var userServiceOffering = await _userServiceOfferingRepository.FirstOrDefaultAsync(id);
            if (userServiceOffering == null)
            {
                throw new UserFriendlyException("Service Offering Not Found, Maybe It's Deleted");
            }
            return userServiceOffering;
        }
    }
}
