﻿using System;
using System.Collections.Generic;
using Abp.Authorization.Users;
using Abp.Extensions;
using Abp.Timing;

namespace SalesHack.Authorization.Users
{
    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        //public static string CreateRandomPassword()
        //{
        //    return Guid.NewGuid().ToString("N").Truncate(16);
        //}

        //public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        //{
        //    var user = new User
        //    {
        //        TenantId = tenantId,
        //        UserName = AdminUserName,
        //        Name = AdminUserName,
        //        Surname = AdminUserName,
        //        EmailAddress = emailAddress
        //    };

        //    user.SetNormalizedNames();

        //    return user;
        //}

        public static User CreateTenantAdminUser(int tenantId, string emailAddress, string name, string lastName, string password, string cellphone)
        {
            var user = new User
            {
                TenantId = tenantId,
                //UserName = AdminUserName,
                //Name = AdminUserName,
                //Surname = AdminUserName,
                EmailAddress = emailAddress,
                UserName = emailAddress,
                Name = name,
                Surname = lastName,
                Password = password,
                PhoneNumber = cellphone,
                IsActive = false
            };

            user.SetNormalizedNames();

            return user;
        }

        // 

        //Small Deals
        public virtual double TargetedMonthlyTurnoverSD { get; set; }
        public virtual double AverageDealSizeSD { get; set; }
        public virtual int TurnAroundTimeSD { get; set; }

        //Medium Deals
        public virtual double TargetedMonthlyTurnoverMD { get; set; }
        public virtual double AverageDealSizeMD { get; set; }
        public virtual int TurnAroundTimeMD { get; set; }

        //Big Deals
        public virtual double TargetedMonthlyTurnoverBD { get; set; }
        public virtual double AverageDealSizeBD { get; set; }
        public virtual int TurnAroundTimeBD { get; set; }

        public virtual string TelNumber { get; set; }

        //EMAILS        

        public virtual string MailUsername { get; set; }
        public virtual string MailPassword { get; set; }

        public virtual string IncomingMailServer { get; set; }
        public virtual string IncomingPort { get; set; }
        public virtual bool IncomingMailUseSSL { get; set; }
        public virtual string IncomingMailAuthenticationType { get; set; }

        public virtual string OutgoingMailServer { get; set; }
        public virtual string OutgoingPort { get; set; }
        public virtual bool SMTPAuthentication { get; set; }
        public virtual string MailEncryption { get; set; }

        //SOCIAL MEDIA
        public virtual string FacebookLink { get; set; }
        public virtual string LinkedInLink { get; set; }
        public virtual string GooglePlusLink { get; set; }
        public virtual string InstagramLink { get; set; }
        public virtual string TweeterLink { get; set; }

        //Others
        public virtual string ProfilePicture { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string EmailSignature { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string Industry { get; set; }
        public virtual string Size { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string BeeStatus { get; set; }
        public virtual string Turnover { get; set; }
        public virtual string Department { get; set; }
        public virtual string YearsOfExperience { get; set; }
        public ICollection<UserServiceOffering> Services { get; set; }
        public ICollection<UserTargetIndustry> TargetIndustries { get; set; }
        // SMALL DEAL CALCULATION
        public int LeadsTargetSD { get; set; }
        public int EmailsTargetSD { get; set; }
        public int CallsTargetSD { get; set; }
        public int NeedsAnalysisTargetSD { get; set; }
        public int MeetingsTargetSD { get; set; }
        public int ProposalsTargetSD { get; set; }
        public int WinsTargetSD { get; set; }
        public int FollowupSD { get; set; }
        public double RevenueTargetSD { get; set; }
        public int ProjectedTurnAroundTimeSD { get; set; }

        // MEDIUM DEAL CALCULATION
        public int LeadsTargetMD { get; set; }
        public int EmailsTargetMD { get; set; }
        public int CallsTargetMD { get; set; }
        public int NeedsAnalysisTargetMD { get; set; }
        public int MeetingsTargetMD { get; set; }
        public int ProposalsTargetMD { get; set; }
        public int WinsTargetMD { get; set; }
        public int FollowupMD { get; set; }
        public double RevenueTargetMD { get; set; }
        public int ProjectedTurnAroundTimeMD { get; set; }

        // BIG DEAL CALCULATION
        public int LeadsTargetBD { get; set; }
        public int EmailsTargetBD { get; set; }
        public int CallsTargetBD { get; set; }
        public int NeedsAnalysisTargetBD { get; set; }
        public int MeetingsTargetBD { get; set; }
        public int ProposalsTargetBD { get; set; }
        public int WinsTargetBD { get; set; }
        public int FollowupBD { get; set; }
        public double RevenueTargetBD { get; set; }
        public int ProjectedTurnAroundTimeBD { get; set; }
      
        public virtual Guid? ProfilePictureId { get; set; }

        public virtual bool ShouldChangePasswordOnNextLogin { get; set; }

        public DateTime? SignInTokenExpireTimeUtc { get; set; }

        public string SignInToken { get; set; }

        public string GoogleAuthenticatorKey { get; set; }       

        public User()
        {
            IsLockoutEnabled = true;
            IsTwoFactorEnabled = true;
        }

        /// <summary>
        /// Creates admin <see cref="User"/> for a tenant.
        /// </summary>
        /// <param name="tenantId">Tenant Id</param>
        /// <param name="emailAddress">Email address</param>
        /// <returns>Created <see cref="User"/> object</returns>
        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress
            };

            user.SetNormalizedNames();

            return user;
        }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public override void SetNewPasswordResetCode()
        {
            /* This reset code is intentionally kept short.
             * It should be short and easy to enter in a mobile application, where user can not click a link.
             */
            PasswordResetCode = Guid.NewGuid().ToString("N").Truncate(10).ToUpperInvariant();
        }

        public void Unlock()
        {
            AccessFailedCount = 0;
            LockoutEndDateUtc = null;
        }

        public void SetSignInToken()
        {
            SignInToken = Guid.NewGuid().ToString();
            SignInTokenExpireTimeUtc = Clock.Now.AddMinutes(1).ToUniversalTime();
        }
    }


}
