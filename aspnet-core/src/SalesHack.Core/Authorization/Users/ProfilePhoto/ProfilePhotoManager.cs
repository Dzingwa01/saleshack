﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users.ProfilePhoto
{
    public class ProfilePhotoManager : IProfilePhotoManager
    {
        private readonly IRepository<ProfilePhoto, long> _photoRepository;
        public ProfilePhotoManager(IRepository<ProfilePhoto, long> photoRepository)
        {
            _photoRepository = photoRepository;
        }
        public async Task CreateAsync(ProfilePhoto photo)
        {
            await _photoRepository.InsertAsync(photo);
        }
    }
}
