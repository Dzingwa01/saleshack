﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Authorization.Users.ProfilePhoto
{
    [Table("ProfilePhotos")]
    public class ProfilePhoto : FullAuditedEntity<long>, IMustHaveTenant
    {
        public string Url { get; set; }
        public string Description { get; set; }
        public string PublicId { get; set; }
        [ForeignKey(nameof(UserId))]
        public long UserId { get; set; }
        public User User { get; set; }
        public int TenantId { get; set; }

        public static ProfilePhoto Create(int tenantId, string url, string description, string publicId, long userId)
        {
            var profilePhoto = new ProfilePhoto
            {
                TenantId = tenantId,
                Url = url,
                Description = description,
                PublicId = publicId,
                UserId = userId
            };
            return profilePhoto;
        }
    }
}
