﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users.ProfilePhoto
{
    public interface IProfilePhotoManager : IDomainService
    {
        Task CreateAsync(ProfilePhoto photo);
    }
}
