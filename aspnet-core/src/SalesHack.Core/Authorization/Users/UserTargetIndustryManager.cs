﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users
{
    public class UserTargetIndustryManager : IUserTargetIndustryManager
    {
        private readonly IRepository<UserTargetIndustry, Guid> _userTargetIndustryRepository;

        public UserTargetIndustryManager(IRepository<UserTargetIndustry, Guid> userTargetIndustryRepository)
        {
            _userTargetIndustryRepository = userTargetIndustryRepository;
        }
        public async Task CreateAsync(UserTargetIndustry targetIndustry)
        {
            await _userTargetIndustryRepository.InsertAsync(targetIndustry);
        }

        public async Task<UserTargetIndustry> GetAsync(Guid id)
        {
            var userTargetIndustry = await _userTargetIndustryRepository.FirstOrDefaultAsync(id);
            if (userTargetIndustry == null)
            {
                throw new UserFriendlyException("Target Industry Not Found, Maybe It's Deleted");
            }
            return userTargetIndustry;
        }
        
    }
}
