﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Authorization.Users
{
    public interface IUserTargetIndustryManager : IDomainService
    {
        Task<UserTargetIndustry> GetAsync(Guid id);
        Task CreateAsync(UserTargetIndustry targetIndustry );
    }
}
