﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Authorization.Users
{
    [Table("UserTargetIndustryOffering")]
    public class UserTargetIndustry : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        [ForeignKey(nameof(UserId))]
        public long UserId { get; set; }
        public User User { get; set; }
        public int TenantId { get; set; }
        [ForeignKey(nameof(IndustryId))]
        public int IndustryId { get; set; }
        public DataTable DataTable { get; set; }

        public static UserTargetIndustry Create(int TenantId, long UserId, int IndustryId)
        {
            var userTargetIndustry = new UserTargetIndustry
            {
                TenantId = TenantId,
                UserId = UserId,
                IndustryId = IndustryId
            };
            return userTargetIndustry;
        }
    }
}
