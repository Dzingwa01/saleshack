﻿using Abp.Authorization;
using SalesHack.Authorization.Roles;
using SalesHack.Authorization.Users;

namespace SalesHack.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }        
    }
}
