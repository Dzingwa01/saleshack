﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies
{
    [Table("Companies")]
    public class Company : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        [Required]
        [MaxLength(256)]
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        //[Required]
        //[MaxLength(256)]

        [ForeignKey(nameof(AssignedId))]
        public virtual User AssingedUser { get; set; }
        public long AssignedId { get; set; }
        public virtual string Size { get; set; }
        public virtual string Phase { get; set; }
        public virtual decimal Turnover { get; set; }
        public virtual int YearInBusiness { get; set; }
        public virtual string WebUrl { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Industry { get; set; }
        public virtual string BusinessType { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string BeeStatus { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string Source { get; set; }
        public virtual string Stage { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }

        //[ForeignKey(nameof(ContactId))]
        public virtual ICollection<Contact> Contacts { get; set; }
        //public virtual Guid? ContactId { get; set; }
        public Company()
        {
            Contacts = new Collection<Contact>();
        }
        public static Company Create(int TenantId, string Name, string email, long AssignedId, string Size, string Phase, decimal Turnover
            , int YearInBusiness, string WebUrl, string Telephone, string Industry, string BusinessType, string Address, string City, string Province, string Country, string BeeStatus
            , string PostalCode, string Source, string Stage, string SpecialEvents, string Facebook, string LinkedIn, string GooglePlus, string Instagram, string Twitter)
        {
            var company = new Company
            {
                TenantId = TenantId,
                Name = Name,
                Email = email,
                AssignedId = AssignedId,
                Size = Size,
                Phase = Phase,
                Turnover = Turnover,
                YearInBusiness = YearInBusiness,
                WebUrl = WebUrl,
                Telephone = Telephone,
                Industry = Industry,
                BusinessType = BusinessType,
                Address = Address,
                City = City,
                Province = Province,
                Country = Country,
                PostalCode = PostalCode,
                Source = Source,
                BeeStatus = BeeStatus,
                Stage = Stage,
                SpecialEvents = SpecialEvents,
                Facebook = Facebook,
                LinkedIn = LinkedIn,
                GooglePlus = GooglePlus,
                Instagram = Instagram,
                Twitter = Twitter
            };

            company.Contacts = new Collection<Contact>();

            return company;

        }
    }
}
