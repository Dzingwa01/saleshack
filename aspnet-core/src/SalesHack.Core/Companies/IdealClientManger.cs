﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public class IdealClientManger : IIdealClientManager
    {
        private readonly IRepository<IdealClient, Guid> _idealClientRepository;

        public IdealClientManger(IRepository<IdealClient, Guid> idealClientRepository)
        {
            _idealClientRepository = idealClientRepository;
        }
        public async Task CreateAsync(IdealClient idealClient)
        {
            await _idealClientRepository.InsertAsync(idealClient);
        }

        public async Task<IdealClient> GetAsync(Guid id)
        {
            var idealClient = await _idealClientRepository.FirstOrDefaultAsync(id);
            if (idealClient == null)
            {
                throw new UserFriendlyException("Ideal Client Not Found, Maybe it's Deleted");
            }

            return idealClient;
        }
    }
}
