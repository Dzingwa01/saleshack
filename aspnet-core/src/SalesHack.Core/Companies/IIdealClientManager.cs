﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public interface IIdealClientManager : IDomainService
    {
        Task<IdealClient> GetAsync(Guid id);
        Task CreateAsync(IdealClient idealClient);

    }
}
