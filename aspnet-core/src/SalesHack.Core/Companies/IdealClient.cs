﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies
{
    [Table("IdealClient")]
    public class IdealClient : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        [ForeignKey(nameof(DataTableId))]
        public int DataTableId { get; set; }
        public DataTable DataTable { get; set; }
        public int TenantId { get; set; }

        public static IdealClient Create(int TenantId, int DataTableId)
        {
            var idealClient = new IdealClient
            {
                TenantId = TenantId,
                DataTableId = DataTableId
            };

            return idealClient;
        }
    }
}
