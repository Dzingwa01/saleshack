﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies
{
    [Table("ContactStages")]
    public class ContactStage : FullAuditedEntity<int>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        //fields go in here  
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}
