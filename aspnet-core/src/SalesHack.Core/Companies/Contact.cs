﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using SalesHack.Prospects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Companies
{
    [Table("Contacts")]
    public class Contact : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        [MaxLength(256)]
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        [ForeignKey(nameof(AssignedId))]
        public virtual User AssingedUser { get; set; }
        public long? AssignedId { get; set; }

        public virtual string Source { get; set; }

        public virtual string JobTitle { get; set; }
        public virtual string Department { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }

        public string Industry { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public virtual string Instagram { get; set; }
        public virtual string Twitter { get; set; }

        [ForeignKey(nameof(ContactStageId))]
        public virtual ContactStage ContactStage { get; set; }
        public virtual int? ContactStageId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company ContactCompany { get; set; }
        public Guid? CompanyId { get; set; }

        public Contact()
        {
        }

        public static Contact Create(Guid? companyId, int id, string name, string surname, string source, string industry,
            string jobTitle, string department, string telephone, string cellphone, string email, string address,
            string city, string province, string specialEvents, string facebook, string linkedIn, string googlePlus, string instagram, string twitter, long assignedId, string status)
        {
            var contact = new Contact
            {
                TenantId = id,
                AssignedId = assignedId,
                Name = name,
                Surname = surname,
                Source = source,
                Industry = industry,
                JobTitle = jobTitle,
                Department = department,
                Telephone = telephone,
                Cellphone = cellphone,
                Email = email,
                Address = address,
                City = city,
                Province = province,
                SpecialEvents = specialEvents,
                Facebook = facebook,
                LinkedIn = linkedIn,
                GooglePlus = googlePlus,
                Instagram = instagram,
                Twitter = twitter,
                Status = status,
                CompanyId = companyId
                //CompanyId = companyId

            };

            return contact;
        }
    }
}
