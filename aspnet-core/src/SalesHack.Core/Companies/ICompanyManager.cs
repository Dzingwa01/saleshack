﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public interface ICompanyManager : IDomainService
    {
        Task<Company> GetAsync(Guid id);

        Task CreateAsync(Company company);

        Task<Contact> AddContactAsync(Contact contact, Company company);

        Task<IReadOnlyList<Contact>> GetCompanyContactsAsync(Company company);
    }
}
