﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public interface IContactManager : IDomainService
    {
        Task<Contact> GetAsync(Guid id);
        Task CreateAsync(Contact company);
      
    }
}
