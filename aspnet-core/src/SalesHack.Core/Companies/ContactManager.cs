﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public class ContactManager : IContactManager
    {
        private readonly IRepository<Contact, Guid> _contactRepository;
        public ContactManager(IRepository<Contact, Guid> contactRepository)
        {
            _contactRepository = contactRepository;
        }
        public async Task CreateAsync(Contact contact)
        {
            await _contactRepository.InsertAsync(contact);
        }

        public async Task<Contact> GetAsync(Guid id)
        {
            var contact = await _contactRepository.FirstOrDefaultAsync(id);
            if (contact == null)
            {
                throw new UserFriendlyException("Could not find the company, maybe it's deleted!");
            }

            return contact;
        }
    }
}
