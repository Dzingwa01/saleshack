﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Companies
{
    public class CompanyManager : ICompanyManager
    {
        private readonly IRepository<Company, Guid> _companyRepository;
        private readonly IRepository<Contact, Guid> _contactRepository;

        public CompanyManager(IRepository<Company, Guid> companyRepository, IRepository<Contact, Guid> contactRepository)
        {
            _companyRepository = companyRepository;
            _contactRepository = contactRepository;
        }

        public async Task<Contact> AddContactAsync(Contact contact, Company company)
        {
           return await _contactRepository.InsertAsync(contact);
        }

        public async Task CreateAsync(Company company)
        {
            await _companyRepository.InsertAsync(company);
        }

        public async Task<Company> GetAsync(Guid id)
        {
            var company = await _companyRepository.FirstOrDefaultAsync(id);
            if (company == null)
            {
                throw new UserFriendlyException("Could not find the company, maybe it's deleted!");
            }

            return company;
        }

        public Task<IReadOnlyList<Contact>> GetCompanyContactsAsync(Company company)
        {
            throw new NotImplementedException();
        }
    }
}
