﻿using Abp.Domain.Services;
using SalesHack;

namespace SalesHack
{
    public abstract class SalesHackDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected SalesHackDomainServiceBase()
        {
            LocalizationSourceName = SalesHackConsts.LocalizationSourceName;
        }
    }
}
