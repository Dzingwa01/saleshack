﻿using Abp;

namespace SalesHack
{

    public abstract class SalesHackServiceBase : AbpServiceBase
    {
        protected SalesHackServiceBase()
        {
            LocalizationSourceName = SalesHackConsts.LocalizationSourceName;
        }
    }
}
