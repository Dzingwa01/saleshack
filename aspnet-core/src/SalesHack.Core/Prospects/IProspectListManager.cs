﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public interface IProspectListManager:IDomainService
    {
        Task<ProspectList> GetAsync(Guid id);
        Task CreateAsync(ProspectList prospect);
    }
}
