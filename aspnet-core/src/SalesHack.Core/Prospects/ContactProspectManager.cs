﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public class ContactProspectManager : IContactProspectManager
    {
        private readonly IRepository<ContactProspect, Guid> _prospectRepository;
        public ContactProspectManager(IRepository<ContactProspect, Guid> prospectRepository)
        {
            _prospectRepository = prospectRepository;
        }
        public async Task CreateAsync(ContactProspect prospect)
        {
            await _prospectRepository.InsertAsync(prospect);

        }

        public async Task<ContactProspect> GetAsync(Guid id)
        {
            var prospect = await _prospectRepository.FirstOrDefaultAsync(id);
            if (prospect == null)
            {
                throw new UserFriendlyException("Could not find the prospect, maybe it's deleted!");
            }
            return prospect;
        }
    }
}
