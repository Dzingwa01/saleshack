﻿using Abp.Domain.Entities.Auditing;
using SalesHack.Companies;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Prospects
{
    [Table("Prospects")]
    public class Prospect : FullAuditedEntity<Guid>
    {
        [Required]
        [MaxLength(256)]
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Size { get; set; }
        public virtual string Phase { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual decimal Turnover { get; set; }
        public virtual int YearInBusiness { get; set; }
        public virtual string WebUrl { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Industry { get; set; }
        public virtual string BusinessType { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Source { get; set; }
        public virtual string Stage { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }


        public static Prospect Create(string Name, string email, string Size, string Phase, string JobTitle, decimal Turnover, int YearInBusiness, string WebUrl, string Telephone, string Industry, string BusinessType, string Address, string City, string Province
            , string PostalCode, string Source, string Stage, string SpecialEvents, string Facebook, string LinkedIn, string GooglePlus, string Instagram, string Twitter)
        {
            var prospect = new Prospect
            {

                Name = Name,
                Email = email,
                Size = Size,
                Phase = Phase,
                JobTitle = JobTitle,
                Turnover = Turnover,
                YearInBusiness = YearInBusiness,
                WebUrl = WebUrl,
                Telephone = Telephone,
                Industry = Industry,
                BusinessType = BusinessType,
                Address = Address,
                City = City,
                Province = Province,
                PostalCode = PostalCode,
                Source = Source,
                Stage = Stage,
                SpecialEvents = SpecialEvents,
                Facebook = Facebook,
                LinkedIn = LinkedIn,
                GooglePlus = GooglePlus,
                Instagram = Instagram,
                Twitter = Twitter

            };
            return prospect;
        }
    }
}
