﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.Prospects
{
    public class CompanyProspect : FullAuditedEntity<Guid>
    {
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Size { get; set; }
        public virtual string Phase { get; set; }
        public virtual decimal Turnover { get; set; }
        public virtual int YearInBusiness { get; set; }
        public virtual string WebUrl { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Industry { get; set; }
        public virtual string BusinessType { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string BeeStatus { get; set; }
        public virtual string Source { get; set; }
        public virtual string Stage { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public virtual ICollection<ContactProspect> Contacts { get; set; }
        public CompanyProspect()
        {

        }

        public static CompanyProspect Create(string Name, string email, long AssignedId, string Size, string Phase, decimal Turnover
           , int YearInBusiness, string WebUrl, string Telephone, string Industry, string BusinessType, string Address, string City, string Province, string Country, string BeeStatus
           , string PostalCode, string Source, string Stage, string SpecialEvents, string Facebook, string LinkedIn, string GooglePlus, string Instagram, string Twitter)
        {
            var companyProspect = new CompanyProspect
            {
                Name = Name,
                Email = email,
                Size = Size,
                Phase = Phase,
                Turnover = Turnover,
                YearInBusiness = YearInBusiness,
                WebUrl = WebUrl,
                Telephone = Telephone,
                Industry = Industry,
                BusinessType = BusinessType,
                Address = Address,
                City = City,
                Province = Province,
                Country = Country,
                PostalCode = PostalCode,
                Source = Source,
                BeeStatus = BeeStatus,
                Stage = Stage,
                SpecialEvents = SpecialEvents,
                Facebook = Facebook,
                LinkedIn = LinkedIn,
                GooglePlus = GooglePlus,
                Instagram = Instagram,
                Twitter = Twitter
            };

            return companyProspect;
        }
    }

}
