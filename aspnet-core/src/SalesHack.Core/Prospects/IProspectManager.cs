﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
   public interface IProspectManager:IDomainService
    {
        Task<Prospect> GetAsync(Guid id);
        Task CreateAsync(Prospect prospect);

    }
}
