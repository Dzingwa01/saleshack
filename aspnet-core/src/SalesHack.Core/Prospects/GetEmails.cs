﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public static class GetEmails
    {

        public static List<string> From(string webUrl)
        {
            var links = GetNewLinks(webUrl);
            List<string> emails = new List<string>();

            //foreach (var link in links)
            //{
            //    RetrieveEmails(link, ref emails);
            //}

            Parallel.ForEach(links, link =>
            {
                RetrieveEmails(link, ref emails);
            });

            return emails;
        }
        //public method called from your application 
        private static void RetrieveEmails(string webPage, ref List<string> emails)
        {
            //regular expression 
            string pattern = @"(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})";

            //Set up regex object 
            Regex RegExpr = new Regex(pattern, RegexOptions.IgnoreCase);

            //get the first match 
            Match match = RegExpr.Match(webPage);

            if (match.Success)
            {
                GetAllEmails(RetrieveContent(webPage), ref emails);
            }
        }

        //get the content of the web page passed in 
        private static string RetrieveContent(string webPage)
        {
            //HttpWebResponse response = null;//used to get response 
            //StreamReader respStream = null;//used to read response into string 
            string content;
            try
            {
                //create a request object using the url passed in
                if (!webPage.StartsWith("http"))
                    webPage = "http://" + webPage;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webPage);
                request.Timeout = 100000;
                request.Method = "GET";
                request.Proxy = WebRequest.DefaultWebProxy;

                //go get a response from the page 
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())// ;
                {
                    //create a streamreader object from the response 
                    using (StreamReader respStream = new StreamReader(response.GetResponseStream()))
                    {
                        content = respStream.ReadToEnd();
                    }
                }
                    //get the contents of the page as a string and return it 
                    return content;
                }
            catch (Exception ex)
            {
                throw ex;
            }
            //finally
            //{
            //    response.Close();
            //    respStream.Close();
            //}
        }


        //using a regular expression, find all of the href or urls in the content of the page 
        private static void GetAllEmails(string content, ref List<string> emails)
        {
            //regular expression 
            string pattern = @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@" + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\." + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";

            //Set up regex object 
            Regex RegExpr = new Regex(pattern, RegexOptions.IgnoreCase);

            //get the first match 
            Match match = RegExpr.Match(content);

            //loop through matches 
            while (match.Success)
            {

                //output the match info to a file named matchlog.txt in D drive..

                if (!emails.Contains(match.Groups[0].Value))
                {
                    emails.Add(match.Groups[0].Value);
                    //Console.WriteLine("href match: " + match.Groups[0].Value);
                    //WriteToLog("C:matchlog.txt", "Email match: " + match.Groups[0].Value + Environment.NewLine);
                }

                //get next match 
                match = match.NextMatch();
            }
        }

        //Write to a log file 
        //private void WriteToLog(string file, string message)
        //{
        //    using (StreamWriter w = File.AppendText(file))
        //    {
        //        w.WriteLine(DateTime.Now.ToString() + ": " + message); w.Close();
        //    }
        //}

        private static List<string> GetNewLinks(string webPage)
        {
            string content = RetrieveContent(webPage);
            Regex regexLink = new Regex("(?<=<a\\s*?href=(?:'|\"))[^'\"]*?(?=(?:'|\"))");

            List<string> newLinks = new List<string>();

            Parallel.ForEach(regexLink.Matches(content), match =>
            {
                if (!newLinks.Contains(match.ToString()))
                    newLinks.Add(match.ToString());
            });

            //foreach (var match in regexLink.Matches(content))
            //{
            //    if (!newLinks.Contains(match.ToString()))
            //        newLinks.Add(match.ToString());
            //}

            return newLinks;
        }
    }
}
