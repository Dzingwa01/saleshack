﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public class ProspectManager : IProspectManager
    {
        private readonly IRepository<Prospect, Guid> _prospectRepository;
        public ProspectManager(IRepository<Prospect, Guid> prospectRepository)
        {
            _prospectRepository = prospectRepository;
        }
        public async Task CreateAsync(Prospect prospect)
        {
            await _prospectRepository.InsertAsync(prospect);

        }

        public async Task<Prospect> GetAsync(Guid id)
        {
            var prospect = await _prospectRepository.FirstOrDefaultAsync(id);
            if (prospect == null)
            {
                throw new UserFriendlyException("Could not find the prospect, maybe it's deleted!");
            }
            return prospect;
        }
    }
}
