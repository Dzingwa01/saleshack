﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Prospects
{
    [Table("ProspectLists")]
    public class ProspectList : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }
        [Required]
        public Guid ContactProspectId { get; set; }

        [ForeignKey(nameof(ContactProspectId))]
        public ContactProspect ContactProspect { get; set; }

        public bool IsQualified { get; set; }

        public static ProspectList Add(Guid ContactId)
        {
            try
            {
                var prospectList = new ProspectList
                {
                    ContactProspectId = ContactId
                };
                return prospectList;
            }
            
            catch(Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
