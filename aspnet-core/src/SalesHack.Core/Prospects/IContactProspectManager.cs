﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
   public interface IContactProspectManager : IDomainService
    {
        Task<ContactProspect> GetAsync(Guid id);
        Task CreateAsync(ContactProspect prospect);

    }
}
