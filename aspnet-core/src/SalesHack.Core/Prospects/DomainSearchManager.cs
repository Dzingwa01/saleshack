﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public class DomainSearchManager : IDomainSearchManager
    {
        //private readonly IRepository<Prospect, Guid> _prospectRepository;
        //public DomainSearchManager(IRepository<Prospect, Guid> prospectRepository)
        //{
        //    //_prospectRepository = prospectRepository;
        //}

        public DomainSearchManager()
        {
        }

        public  List<string> GetWebEmails(string domain)
        {
            return GetEmails.From(domain);
        }
    }
}
