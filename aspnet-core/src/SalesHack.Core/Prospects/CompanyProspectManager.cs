﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public class CompanyProspectManager : ICompanyProspectManager
    {
        private readonly IRepository<CompanyProspect, Guid> _prospectRepository;
        public CompanyProspectManager(IRepository<CompanyProspect, Guid> prospectRepository)
        {
            _prospectRepository = prospectRepository;
        }
        public async Task CreateAsync(CompanyProspect prospect)
        {
            await _prospectRepository.InsertAsync(prospect);

        }

        public async Task<CompanyProspect> GetAsync(Guid id)
        {
            var prospect = await _prospectRepository.FirstOrDefaultAsync(id);
            if (prospect == null)
            {
                throw new UserFriendlyException("Could not find the prospect, maybe it's deleted!");
            }
            return prospect;
        }
    }
}
