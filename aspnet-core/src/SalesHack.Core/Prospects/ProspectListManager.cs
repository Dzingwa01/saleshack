﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
    public class ProspectListManager : IProspectListManager
    {
        private readonly IRepository<ProspectList, Guid> _prospectListRepository;


        public ProspectListManager(IRepository<ProspectList,Guid> prospectListRepository)
        {
            _prospectListRepository = prospectListRepository;
        }
        public async Task CreateAsync(ProspectList prospect)
        {
            await _prospectListRepository.InsertAsync(prospect);
        }

        public async Task<ProspectList> GetAsync(Guid id)
        {
            var prospectList = await _prospectListRepository.FirstOrDefaultAsync(id);
            if (prospectList == null)
            {
                throw new UserFriendlyException("Could not find the List, maybe it's deleted!");
            }
            return prospectList;
        }
    }
}
