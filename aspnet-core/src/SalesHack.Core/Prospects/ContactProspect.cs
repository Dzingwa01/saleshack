﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.Prospects
{
    public class ContactProspect : FullAuditedEntity<Guid>
    {

        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Source { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string Department { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Industry { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public virtual string SpecialEvents { get; set; }
        public virtual string Facebook { get; set; }
        public virtual string LinkedIn { get; set; }
        public virtual string GooglePlus { get; set; }
        public virtual string Instagram { get; set; }
        public virtual string Twitter { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual CompanyProspect ContactCompany { get; set; }
        public Guid? CompanyId { get; set; }

        public ContactProspect()
        {

        }

        public static ContactProspect Create(Guid? companyId, string name, string surname, string source,
            string jobTitle, string department, string telephone, string cellphone, string email, string industry, string address,
            string city, string province, string specialEvents, string facebook, string linkedIn, string googlePlus, string instagram, string twitter)
        {
            var contactProspect = new ContactProspect
            {

                Name = name,
                Surname = surname,
                Source = source,
                JobTitle = jobTitle,
                Department = department,
                Telephone = telephone,
                Cellphone = cellphone,
                Email = email,
                Industry = industry,
                Address = address,
                City = city,
                Province = province,
                SpecialEvents = specialEvents,
                Facebook = facebook,
                LinkedIn = linkedIn,
                GooglePlus = googlePlus,
                Instagram = instagram,
                Twitter = twitter

            };

            return contactProspect;
        }
    }
}
