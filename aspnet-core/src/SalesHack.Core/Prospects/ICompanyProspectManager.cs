﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
   public interface ICompanyProspectManager : IDomainService
    {
        Task<CompanyProspect> GetAsync(Guid id);
        Task CreateAsync(CompanyProspect prospect);

    }
}
