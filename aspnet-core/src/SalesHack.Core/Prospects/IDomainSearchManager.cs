﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.Prospects
{
   public interface IDomainSearchManager:IDomainService
    {
        List<string> GetWebEmails(string domain);
    }
}
