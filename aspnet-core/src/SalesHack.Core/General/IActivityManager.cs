﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IActivityManager : IDomainService
    {
        Task<Activity> GetAsync(Guid id);
        Task CreateAsync(Activity activity);
    }
}
