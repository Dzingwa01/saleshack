﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface ITasksManager: IDomainService
    {
        Task<Tasks> GetAsync(Guid id);
        Task CreateAsync(Tasks task);
    }
} 
