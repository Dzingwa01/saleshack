﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class DataTableManager : IDataTableManager
    {
        private readonly IRepository<DataTable, int> _repository;

        public DataTableManager(IRepository<DataTable, int> repository)
        {
            _repository = repository;
        }
        public async Task CreateAsync(DataTable dataTable)
        {
            await _repository.InsertAsync(dataTable);
        }

        public async Task<DataTable> GetAsync(int id)
        {
            var datatable = await _repository.FirstOrDefaultAsync(id);
            if (datatable == null)
            {
                throw new UserFriendlyException("Data Not Found, Maybe it's Deleted");
            }

            return datatable;
        }
    }
}
