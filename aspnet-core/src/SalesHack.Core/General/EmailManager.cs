﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class EmailManager : IEmailManager
    {
        private readonly IRepository<Email, Guid> _emailRepository;

        public EmailManager(IRepository<Email, Guid> emailRepository)
        {
            _emailRepository = emailRepository;
        }
        public async Task CreateAsync(Email activity)
        {
            await _emailRepository.InsertAsync(activity);
        }

        public async Task<Email> GetAsync(Guid id)
        {
            var email = await _emailRepository.FirstOrDefaultAsync(id);
            if (email == null)
            {
                throw new UserFriendlyException("Email Not Found Maybe it's Deleted");
            }
            return email;
        }
    }
}
