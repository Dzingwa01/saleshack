﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.General
{

    [Table("Notes")]
    public class Note : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Description { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }

        public static Note Create(int TenantId, string Description, Guid TargetId, string TargetType)
        {
            var note = new Note
            {
                TenantId = TenantId,
                Description = Description,
                TargetId = TargetId,
                TargetType = TargetType
            };
            return note;
        }
    }
}
