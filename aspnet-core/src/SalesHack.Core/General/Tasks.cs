﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.General
{
    [Table("Tasks")]
    public class Tasks : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public string Description { get; set; }
        public int TaskStatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }


        public static Tasks Create(int TenantId, string Description, int TaskStatusId, DateTime? DueDate, DateTime? CompletedDate, Guid TargetId, string TargetType)
        {
            var tasks = new Tasks
            {
                TenantId = TenantId,
                Description = Description,
                TaskStatusId = TaskStatusId,
                DueDate = DueDate,
                CompletedDate = CompletedDate,
                TargetId = TargetId,
                TargetType = TargetType
            };
            return tasks;
        }
    }
}
