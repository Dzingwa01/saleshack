﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using SalesHack.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.General
{
    [Table("Emails")]
    public class Email : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual string From { get; set; }
        public virtual string Recipient { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Message { get; set; }
        [ForeignKey(nameof(UserId))]
        public virtual long UserId { get; set; }
        public virtual User User { get; set; }
        public int TenantId { get; set; }
        public DateTime? FollowUpDate { get; set; }

        public static Email Create(int TenantId, string From, string Recipient, string Subject, string Message, long UserId, DateTime? FollowUpDate)
        {
            var email = new Email
            {
                TenantId = TenantId,
                From = From,
                Recipient = Recipient,
                Subject = Subject,
                Message = Message,
                UserId = UserId,
                FollowUpDate = FollowUpDate
            };

            return email;
        }
    }
}
