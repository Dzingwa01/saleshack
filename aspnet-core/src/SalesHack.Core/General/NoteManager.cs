﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class NoteManager : INoteManager
    {
        private readonly IRepository<Note, Guid> _noteRepository;

        public NoteManager(IRepository<Note, Guid> noteRepository)
        {
            _noteRepository = noteRepository;
        }
        public async Task CreateAsync(Note note)
        {
            await _noteRepository.InsertAsync(note);
        }

        public async Task<Note> GetAsync(Guid id)
        {
            var note = await _noteRepository.FirstOrDefaultAsync(id);
            if (note == null)
            {
                throw new UserFriendlyException("Note Not Found, Maybe it's Deleted.");
            }
            return note;
        }
    }
}
