﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.General
{
    public class DataTable : FullAuditedEntity<int>
    {
        public string Type { get; set; }
        public string Value { get; set; }

        public static DataTable Create(string Type, string Value)
        {
            var data = new DataTable
            {
                Type = Type,
                Value = Value
            };

            return data;
        }

    }
}
