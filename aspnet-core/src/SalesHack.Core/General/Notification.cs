﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.General
{
    [Table("Notifications")]
    public class Notification : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Description { get; set; }

        public Guid TargetId { get; set; }
        public string TargetType { get; set; }

        public bool isRead { get; set; }
    }
}
