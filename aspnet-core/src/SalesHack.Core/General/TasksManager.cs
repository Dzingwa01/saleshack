﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class TasksManager : ITasksManager
    {
        private readonly IRepository<Tasks, Guid> _tasksRepository;

        public TasksManager(IRepository<Tasks, Guid> tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }
        public async Task CreateAsync(Tasks task)
        {
            await _tasksRepository.InsertAsync(task);
        }

        public async Task<Tasks> GetAsync(Guid id)
        {
            var tasks = await _tasksRepository.FirstOrDefaultAsync(id);
            if (tasks == null)
            {
                throw new UserFriendlyException("Tasks Not Found, Maybe They're Deleted");
            }
            return tasks;
        }
    }
}
