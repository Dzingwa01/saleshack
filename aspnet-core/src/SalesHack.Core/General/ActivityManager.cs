﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public class ActivityManager : IActivityManager
    {
        private readonly IRepository<Activity, Guid> _activityRepository;
        public ActivityManager(IRepository<Activity, Guid> activityRepository)
        {
            _activityRepository = activityRepository;
        }
        public async Task CreateAsync(Activity activity)
        {
            await _activityRepository.InsertAsync(activity);
        }

        public async Task<Activity> GetAsync(Guid id)
        {
            var activity = await _activityRepository.FirstOrDefaultAsync(id);
            if (activity == null)
            {
                throw new UserFriendlyException("Activity Not Found Maybe it's Deleted");
            }
            return activity;
        }
    }
}
