﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesHack.General
{
    [Table("Activities")]
    public class Activity : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual string Description { get; set; }
        public virtual string RouteUrl { get; set; }
        public int TenantId { get; set; }
        public Guid TargetId { get; set; }
        public string TargetType { get; set; }
        public static Activity Create(int TenantId, string Description, string RouteUrl, Guid TargetId, string TargetType)
        {
            var activity = new Activity
            {
                TenantId = TenantId,
                Description = Description,
                RouteUrl = RouteUrl,
                TargetId = TargetId,
                TargetType = TargetType
            };

            return activity;
        }
    }
}
