﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface INoteManager: IDomainService
    {
        Task<Note> GetAsync(Guid id); 
        Task CreateAsync(Note note);
    }
}
