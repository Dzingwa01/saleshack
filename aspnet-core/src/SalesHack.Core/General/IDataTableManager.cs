﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IDataTableManager : IDomainService
    {
        Task<DataTable> GetAsync(int id);
        Task CreateAsync(DataTable dataTable); 
    }
}
