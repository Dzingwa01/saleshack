﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesHack.General
{
    public interface IEmailManager : IDomainService
    {
        Task<Email> GetAsync(Guid id);
        Task CreateAsync(Email activity);
    }
}
