﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy.Payments
{
    public enum PaymentPeriodType
    {
        Monthly = 30,
        Annual = 365
    }
}
