﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy.Payments
{
    public enum SubscriptionPaymentStatus
    {
        Processing = 1,
        Completed = 2,
        Failed = 3,
        Cancelled = 4
    }
}
