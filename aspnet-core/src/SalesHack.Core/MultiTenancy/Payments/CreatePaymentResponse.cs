﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy.Payments
{
    public abstract class CreatePaymentResponse
    {
        public abstract string GetId();
    }
}
