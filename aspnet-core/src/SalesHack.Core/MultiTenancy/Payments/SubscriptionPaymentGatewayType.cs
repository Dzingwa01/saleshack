﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy.Payments
{
    public enum SubscriptionPaymentGatewayType
    {
        Payfast = 1,
        Paypal = 2
    }
}
