﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy.Payments
{
    public abstract class ExecutePaymentResponse
    {
        public abstract string GetId();
    }
}
