﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesHack.MultiTenancy
{
    public enum EndSubscriptionResult
    {
        TenantSetInActive,
        AssignedToAnotherEdition
    }
}
